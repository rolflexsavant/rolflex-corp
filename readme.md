# Rolflex Corperate website

## Inleiding

Dit document beschrijft hoe de Wordpress Multisite precies is ingezet als platform voor de meertalige website. Welke plugins belangrijk zijn en hoe je het project kan
starten op je ontwikkelmachine.

Op details over de werking van Wordpress zelf wordt niet ingegaan. Hierover bestaat al [hele goede documentatie](https://codex.wordpress.org/) op de Wordpress website zelf. De versie van Wordpress
die gebruikt is voor de initiele versie van Wordpress is Wordpress 5.5.1

Wordpress is een vrij eenvoudig systeem. Wanneer je nieuw bent in Wordpress zijn er eigenlijk slechts 3 dingen die je goed door moet hebben om ermee aan de slag te kunnen gaan.

1. Goed begrip van de [Wordpress Loop](https://developer.wordpress.org/themes/basics/the-loop/) en daarmee het standaard gedrag van Wordpress.
2. Het gebruik van Wordpress [hooks](https://developer.wordpress.org/plugins/hooks/) om standaard gedrag te beinvloeden.
3. De Wordpress [template hierarchy](https://developer.wordpress.org/themes/basics/template-hierarchy/) om te brijpen wanneer welke informatie geladen en getoond wordt.

## Technologie stack in het kort

Voor de ongeduldige lezer hier alvast een korte overview van alle onderdelen in het project die van belang zijn.

De corperate website voor Rolflex is gemaakt op basis een [Wordpress multi-site](https://wordpress.org/support/article/create-a-network/) installatie, een maatwerk thema en framework bovenop Wordpress en slechts 1 plugin dependency. [Lees meer over Wordpress themes]((https://wordpress.org/support/article/using-themes/)).

Iedere taal is een sub-site. Op de hoofdsite wordt een thema geselecteerd die de taal detecteert en de bezoeker doorstuurt naar de juiste sub-site.

De `rolflex-network-front` theme is de hoofd site. Dit is de theme die je kiest voor site die op de root-url terecht komt. Hier zit niets aan styling e.d. in. Op de root-url is geen taal gekozen. De rolflex network front-theme handelt de taaldetectie correct af en stuurt de bezoeker door naar de juiste taal.

De `rolflex-compact` maatwerk wordpress theme bevat alle datamodellen, javascript en files die nodig zijn voor de presentatie van de website en de juiste admin schermen. Iedere taal is een "instance" (sub-site) van de theme. Door de theme te kiezen voor je site krijg je als content beheerder dus alle tools om de Rolflex website te maken.

De theme heeft zijn eigen [gulp.js](https://gulpjs.com/) file die de theme build. In de gulp file zelf is te zien hoe de build werkt. Voor styling wordt gebruik gemaakt van [SCSS](https://sass-lang.com/) en voor Javascript wordt de [ECMA 2016](https://www.ecma-international.org/ecma-262/7.0/) standaard gebruikt. Via [babel](https://babeljs.io/) wordt de javascript gecompiled naar cross-browser compatible javascript. Met de ECMA 2016 standaard i.c.m. babel is er **geen** noodzaak libraries als jQuery te gebruiken. Dit komt de performance ten goede.

Als CSS framework wordt er gebruik gemaakt van [bootstrap 4.x](https://getbootstrap.com/docs/4.0/getting-started/introduction/). Dit is een node package en er wordt enkel ingeladen wat gebruikt wordt. Zo houden we het aantal KB's zo klein mogelijk.

Alle Javascript en CSS is verder maatwerk. Bronbestanden voor de developer bevinden zich in de `/src` directory in de theme directory. Wanneer het project geinstalleerd is worden via een `$ npm run build:theme` de source files gecompiled en komen terecht in de `/assets` directory binnen de theme.


## Plugins & Thema

Er zijn drie betaalde plugins die gebruikt worden binnen het project. Met slechts 1 van die drie plugins is er een dependency, en zelfs dit is slechts een "zachte dependency". M.a.w. er hoeft niet betaalt te worden om de website in de lucht te houden zoals die op moment van oplevering is. De betaalde plugins worden hier kort uitgelicht. Verder in het project wordt ingegaan op de technische werking van de aanbevolen plugins in het project. Plugins die noch betaald noch aanbevolen zijn worden buiten beschouwing gelaten.

[Advanced Custom Fields Pro](https://www.advancedcustomfields.com/pro/) is de enige plugin waar een dependency mee is. Dit is eveneens een betaalde plugin. De licentie kost $49,- per jaar. Voor dit jaar zijn de licentie kosten al betaald. Wanneer de licentie verloopt blijft de plugin gewoon werken, het is dan alleen niet meer mogelijk om te upgraden. Mochten de nieuwere versies van Wordpress incompatible raken met de plugin, dan zou er betaald moeten worden om de laatste versie weer te kunnen downloaden.

[WP Migrate DB Pro](https://deliciousbrains.com/wp-migrate-db-pro/) is een andere betaalde plugin die wordt gebruikt. Deze kost (zoals die binnen de site gebruikt wordt) $199,- per jaar. Deze plugin is niet nodig voor een juiste werking van de website. De plugin maakt eveneens gebruik van de Media-AddOn en dit maakt het mogelijk content en data te pushen en pullen tussen verschillende servers. Zo kun je makkelijk je ontwikkelmachine synchroniseren met een live of (andere) testomgeving.

[WP Optimize Premium](https://getwpo.com/) Is de laatste betaalde plugin die gebruikt wordt en kost 39 EUR per jaar. Ook deze plugin is geen dependency voor een juiste werking van de website. De plugin optimaliseert content en gerenderde code. Zonder de plugin werkt alles gewoon naar behoren, zonder de plugin is de website technisch al zeer goed geoptimaliseerd. Met de plugin nog net iets beter en wordt de content ook geoptimaliseerd. Er is ook een gratis versie, maar die is niet vanaf de command-line te gebruiken.

# Getting started

## Eisen vooraf

Van start gaan met het project gaat het makkelijkste met een Linux server. Zelf gebruik ik hiervoor vaak [vagrant](https://www.vagrantup.com/). Hiermee kun je vanaf de CLI van je eigen OS (Mac, Linux of Windows) een Linux VM inrichten zodat al je web-technologie binnen een eigen omgeving draait zoals die dat op productie ook zou doen.

Om het makkelijk te maken bestaan er al voorafingestelde vagrant machines die te downloaden zijn. Zelf vind ik het altijd makkelijk om [Homestead](https://laravel.com/docs/8.x/homestead) te gebruiken. Hier kun je eenvoudig via YAML files databases, hosts, directory mappings tussen je VM en Host en dat soort zaken instellen. [Node, PHP, MySQL, apache, NGINX, MariaDB e.d.](https://laravel.com/docs/8.x/homestead#included-software) zijn allemaal inbegrepen en eenvoudig in te stellen. Er kan ook eenvoudig tussen versies van node en php gewisseld worden.

Voor de installatie van [Homestead](https://laravel.com/docs/8.x/homestead) kun je het beste terecht bij [hun eigen documentatie](https://laravel.com/docs/8.x/homestead#introduction). Ook leggen zij nog eens uit wat je met vagrant kunt en hoe je dit kan installeren. Erg handig allemaal.

Wil je de Rolflex Corperate site op een andere manier draaien, dan kan dat ook, maar dat is niet inbegrepen in deze documentatie. Wat je in ieder geval nodig hebt zijn de volgende packages. Het zijn de versies waarmee ontwikkeld is, nieuwer is over het algemeen altijd beter.

- Bash
- PHP 7.4.x
- MySQL 5.7.x of een compatible MariaDB variant
- Apache/2.4.x
- Node 10.14.x
- NPM 6.13.x

Verder worden alle dependencies geinstalleerd met een `npm install` commando. Een package die niet via de node package.json wordt geinstalleerd maar veelal global op de ontwikkelmachine geinstalleerd moet worden is [node-sass](https://www.npmjs.com/package/node-sass). Node sass is compatible met alle SASS features die gebruikt worden, maar is deprecated. Het kan zijn dat wanneer je het project wilt installeren deze inmiddels werkt met [dart-sass](https://sass-lang.com/dart-sass).


## Installatie

Er zijn twee manieren hoe het project geinstalleerd kan worden.

1. Een verse Wordpress Multisite installatie
2. Een installatie met dummy content (de content zoals vlak voor livegang).

### Installatie verse Wordpress Multisite insallatie

Voor de schone installatie van een nieuw netwerk [kunnen deze stappen worden gevolgd](https://wordpress.org/support/article/create-a-network/). Een kleine sidenote daarbij is dat er al configuratie aanwezig is in `wp-config-project.php` die **niet** aanwezig moet zijn tot nadat de installatie voltooid is. Daarom dien je deze regels dan even in commentaar te zetten totdat de installatie voltooid is en op het scherm wordt getoond dat ze aan de configuratie toegevoegd moeten worden:

    define('MULTISITE', true);
    define('SUBDOMAIN_INSTALL', false);
    define('PATH_CURRENT_SITE', '/');
    define('SITE_ID_CURRENT_SITE', 1);
    define('BLOG_ID_CURRENT_SITE', 1);

Na een schone verse installatie kun je dan eventueel met `wp migrate db pro` een [pull](https://deliciousbrains.com/wp-migrate-db-pro/) doen van een reeeds bestaande site en op die manier alle data en media binnenhalen. Let daarbij op dat je de URL middels de **find & replace** functionaliteit omzet naar de URL waarop je de installatie hebt geplaatst.

Via een `npm install` commando in de project root directory worden alle dependencies geinstalleerd. Vanaf dat moment kun je de commando's gebruiken die zijn opgesteld in `package.json`. Het belangrijkste commando om te ontwikkelen in `npm run develop:theme`. Dit triggert de gulp file in de theme en rebuild het project na veranderingen in files. Bestudeer de `package.json` voor meerdere commando's (build, deploy etc...).


# Details

### Wordpress Themes
Binnen de Wordpress multisite is de rolflex theme het onderdeel waarin alle website logica, datamodellen, styling, javascript e.d. zit. Iedere taal is een site binnen het Wordpress Multisite network. Hier is voor gekozen zodat iedere taal optimale flexibile site biedt. Het ontwikkelde theme is dus eigenlijk de tool waarmee rolflex websites in iedere taal gebouwd kunnen worden door de content beheerder. Changes aan de theme rollen code-technisch uit over alle talen in 1 keer. Ze beschikken echter een-ieder over hun eigen database tables. Via de `options.php` file in de theme kunnen [wordpress options](https://codex.wordpress.org/Database_Description#Table:_wp_options) in de database over alle talen in 1 keer worden uitgerold en via de deloyment pipeline worden gepushed.