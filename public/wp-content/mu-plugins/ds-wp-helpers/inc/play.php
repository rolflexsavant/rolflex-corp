<?php

require_once dirname(__FILE__).'/array.php';

/**
 * File to play with php, not loaded in the plugin.
 */

$one = [
    'lorem' => 'Ipsum',
    'dolor' => "Est",
    'object' => 'Another',
    'text' => 'Nunc hendrerit aliquam quam in mollis. Phasellus eget sapien hendrerit, pharetra felis et, venenatis magna. Suspendisse hendrerit nisl vitae nulla sodales cursus.',
    'count' => 90890,
    'float' => 1.232
];

$two = explode(' ', 'Nunc hendrerit aliquam quam in mollis. Phasellus eget sapien hendrerit, pharetra felis et, venenatis magna. Suspendisse hendrerit nisl vitae nulla sodales cursus.');

$three = explode('  ', 'Morbi efficitur sapien quis lacus dignissim faucibus.  Sed at fringilla nunc.  Sed posuere, ex ut suscipit euismod, augue elit interdum libero, sed efficitur nulla leo a purus.
     Integer massa ligula, pretium id commodo quis, ornare sit amet nisl.  Proin mattis ut leo eget tempor. ');

$four = ['one' => 1, 'two' => 2, 'three' => 3, 'four' => ''];

$five = array_keys($one);

$six = array_merge($one, ['six' => $one, 'seven' => $four ]);

$array = [
    'one' => [
        'test' => [
            'something' => true
        ],
        $one,
        $two,
        $three,
        $four,
        $five,
    ],
    'two' => $six,
    'three' => $three,
    'four' => $four,
    'five' => $five,
    'six' => $six,
    'seven' => 'my value',
    'eight' => 8,
    'nine' => $one,
];



function play_array_dot( $array , $include_objects = false,  $top_key = null ) {
    $dot_values = [];

    foreach($array as $key => $value ) {
        if( $include_objects && is_object($value) ) {
            $value = (array) $value;
        }

        if( is_array($value) ) {
            $dot_values = array_merge($dot_values, play_array_dot($value, $include_objects, $key ) );

        } else {
            $key = !is_null($top_key) ? $key : implode('.', [$top_key, $key]);
            $dot_values[$key] = $value;
        }
    }

    return $dot_values;
}



dd( array_get($array, 'one.test.something') );

dd( $array['one']['test']['something'] );

dd( play_array_dot($array) , true );



exit;
