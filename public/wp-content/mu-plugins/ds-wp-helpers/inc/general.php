<?php
/**
 * Get the version object
 */
function ds_version() {
    $json_file = dirname(ABSPATH).'/version.json';
    $json = json_file($json_file);

    return $json;
}


/**
 * Get json data from file
 */
function json_file( $file_path, $assoc_array = false ) {
    if( file_exists($file_path) ) {
        $json_string = file_get_contents($file_path);
        return json_decode( $json_string, $assoc_array );
    }

    return null;
}



add_action('init', 'helper_admin_color_scheme_warning' );
function helper_admin_color_scheme_warning() {
    if( is_user_logged_in() ) {
        if( constant('RFLEX_ENV') == 'production' ) {
            $users          = ['admin', 'root'];
            $current_user   = wp_get_current_user();

            if( in_array($current_user->data->user_login, $users) ) {
                // Force the red sunrise color scheme
                add_filter( 'get_user_option_admin_color', function() {
                    return 'sunrise';
                }, 9000 );
            }
        }
    }
}


function helper_admin_color_schema_force( $remove_color_picker = true ) {
    if( $remove_color_picker ) {
    // remove the color scheme picker completely
    remove_action( 'admin_color_scheme_picker', 'admin_color_scheme_picker' );
    }
    // force all users to use this color scheme
    add_filter( 'get_user_option_admin_color', function() {
        return 'light';
    });
}


function helper_current_request() {
    return (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
}


function helper_is_login() {
    $ABSPATH_MY = str_replace(array('\\','/'), DIRECTORY_SEPARATOR, ABSPATH);
    return ((in_array($ABSPATH_MY.'wp-login.php', get_included_files()) || in_array($ABSPATH_MY.'wp-register.php', get_included_files()) ) || (isset($_GLOBALS['pagenow']) && $GLOBALS['pagenow'] === 'wp-login.php') || $_SERVER['PHP_SELF']== '/wp-login.php');
}

function helper_get_all_transients() {
    global $wpdb;

    return $wpdb->get_results(
        "SELECT option_name AS name, option_value AS value FROM $wpdb->options
        WHERE option_name LIKE '_transient_%'"
    );
}

