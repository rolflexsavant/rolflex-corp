<?php
/**
 * Simple debug function that prints all the arguments it takes and formats the text nicely
 * by dumping it in a <pre> tag and then exits the scripts.
 *
 * @param mixed
 */
function dd( $value , $pretty = false) {
    $style = 'padding: 20px; border: 1px solid #ccc; background: #eee';
    print '<pre style="'.$style.'">';
    if( $pretty ) {
        print_r($value);
    } else {
        var_dump($value);
    }
    print '</pre>';
}

function dkeys($arr) {
    foreach($arr as $k => $v ) {
        dd($k);
    }
}

/**
 * Same, but quits the function
 */
 function d( $value, $pretty = false ) {
     dd($value, $pretty );
     exit;
 }


 /**
  * Stops the script and prints filename
  */
function f( $f = __FILE__ ) {
    print $f; exit;
}


function doptions() {
    d(wp_load_alloptions());
}


function dump_icons() {
    $icons = [
    'menu'          => 'bars',
    'calendar'      => 'calendar-alt',
    'search'        => 'search1',
    'phone'         => 'phone',
    'phone-alt'     => 'phone-alt',
    'close'         => 'close',
    'popular'       => 'star',
    'delivery'      => 'shipping-fast',
    'route'         => 'route',
    'download'      => 'cloud-download-alt',
    'check-in-circle' => 'check-circle',
    'check'         => 'check',
    'office'        => 'building',
    'topic'         => 'readme',
    'gears'         => 'cogs',
    'home'          => 'home',
    'globe'         => 'globe',
    'location'      => 'map-marker-alt',
    'location-map'  => 'map-marked-alt',
    'thumbs-up'     => 'thumbs-up',
    'thumbs-down'   => 'thumbs-down',
    'door'          => 'warehouse',
    'quote-left'    => 'quote-left',
    'quote-right'   => 'quote-right',
    'design-door'   => 'pencil-ruler',
    'plus'          => 'plus-square',
    'topic-default' => 'star',
    'email'         => 'envelope',


    'related'       => 'project-diagram',
    //'related'       => 'mendeley',
    'arrow-up'      => 'caret-up',
    'arrow-right'   => 'caret-right',
    'arrow-down'    => 'caret-down',
    'arrow-left'    => 'caret-left',

    'down'          => 'caret-down',
    // 'chevron-right' => 'chevron-right',
    // 'chevron-left'  => 'chevron-left',
    // 'chevron-up'    => 'chevron-up',
    // 'chevron-down'  => 'chevron-down',
    // Social media
    'linkedin'      => 'linkedin',
    'facebook'      => 'facebook',
    'twitter'       => 'twitter',
    'youtube'       => 'youtube',
    'pinterest'     => 'pinterest',
    'instagram'     => 'instagram',

    // Icon picker
    'wind' => 'wind',
    'external-link-alt' => 'external-link-alt',
    'door-closed' => 'door-closed',
    'eye' => 'eye',
    'low-vision' => 'low-vision',
    'layer-group' => 'layer-group',
    'align-justify' => 'align-justify',
    'camera' => 'camera-retro',
    'paint-roller' => 'paint-roller',
    'car-wash' => 'warehouse',
    'store' => 'store',
    'car-mechanic' => 'wrench',
    'spray-can' => 'spray-can',
    'upload' => 'upload',
    'caret-square-up' => 'caret-square-up',
    'campground' => 'campground',
    'truck-moving' => 'truck-moving',
    'bomb' => 'bomb',
    'divide' => 'divide',
    'ambulance' => 'ambulance',
    'stream' => 'stream',
    'ruler-vertical' => 'ruler-vertical',
    'tools' => 'tools',
    'store-alt' => 'store-alt',
    'truck-loading' => 'truck-loading',
    'hand-holding' => 'hand-holding',
    'users-cog' => 'users-cog',
    'clock' => 'clock',
    'magic' => 'magic',
    'umbrella' => 'umbrella',
    'border-all' => 'border-all',
    'check-circle' => 'check-circle',
    'cogs' => 'cogs',
    'fax' => 'fax'];

    $x = 0;
    $sass_map = [];

    print '<div class="container-lg">';

    print '<div class="row">';
    foreach( $icons as $icon_key => $icon ) { ++$x;
        $sass_map[] = "'$icon_key' : '$icon',\n";
        print '<div class="col-2" style="text-align: center; padding: 10px;"><i class="icon-'.$icon.'" style="font-size: 2em;"></i><br/><code>rflex-icon-'.$icon_key.'</code></div>';
    }
    print '</div><div class="row"><div class="col-12 text-center"><p>'.$x.' icons</p></div></div>';


    print '<div class="row">';
    print '<div class="col-12 text-center"><h2>SASS</h2></div>';
    print '<div class="col-12"><pre>'.implode('', $sass_map ).'</pre></div>';

    print '</div>';

    print '</div>';
}