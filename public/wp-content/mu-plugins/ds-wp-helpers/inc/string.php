<?php


/**
 * Checks wether a string is empty or contains characters
 *
 * @param string The string to test
 * @return bool TRUE if it contains characters other than spaces, FALSE if the string is empty or contains spaces only
 */
function str_is_empty( $string ) {
    $string = trim($string);
    return !(strlen($string) > 0);
}


/**
 * Make a string kebab case. Now only simple and deals with spaces, dots and underscores. Should
 * become more advanced
 *
 * @see https://medium.com/better-programming/string-case-styles-camel-pascal-snake-and-kebab-case-981407998841
 */
function str_kebab_case( $string , $lowercase = true ) {
    if( $lowercase )
        $string = strtolower($string);

    $string = str_replace(['_', '.', ' '], '-', trim($string) );
    return $string;
}


/**
 * Make a string snake case. Now only simple and deals with spaces, dots and dashes. Should
 * become more advanced
 *
 * @see https://medium.com/better-programming/string-case-styles-camel-pascal-snake-and-kebab-case-981407998841
 */
function str_snake_case( $string ) {
    $string = str_replace(['-', '.', ' ', '  '], '_', trim($string) );
    return $string;
}



function str_readable_bytes($size, $precision = 2) {
    $base = log($size, 1024);
    $suffixes = array('', 'K', 'M', 'G', 'T');

    return round(pow(1024, $base - floor($base)), $precision) .' '. $suffixes[floor($base)];
}


function str_wordcut_char( $string, $max_chars , $appendix = '...' ) {
    /**
     * Check if the string is longer than the maximim allowed characters and
     * the appendix and a space added to it. To make sure we don't replace the
     * last few characters with just the appendix.
     */
    if( strlen($string) > ($max_chars + strlen($appendix) + 1 ) ) {

        // Subtract the string, trim the las spaces
        $string = rtrim( substr($string, 0, $max_chars ) );
        $words = explode(' ', $string ); // put the words in an array

        array_pop($words); // remove the last word
        array_push($words, $appendix ); // replace it with the appendix

        $words = implode(' ', $words ); // And glue it back together

        return $words;
    }
    return $string;
}