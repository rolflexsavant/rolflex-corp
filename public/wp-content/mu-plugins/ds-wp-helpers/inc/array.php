<?php

/**
 * Array defaults, anything other than an array is set to an empty array
 */
function array_default( $must_be_array ) {
    if( !is_array($must_be_array) ) {
        return [];
    }
    return $must_be_array;
}


/**
 * We need to do something to increase performance of this function. It can potentially be
 * a peformance hit.
 */
function array_get( $array, $key, $default = null , $include_objects = false ) {
    if( !is_array($array) ) {
        return $default;
    }

    $keys           = explode('.', $key);
    $current_value  = $array;

    foreach($keys as $k) {
       // var_dump($current_value);
        if( is_array($current_value) && isset($current_value[$k]) ) {
            $current_value = $current_value[$k];
        } elseif( $include_objects && is_object($current_value) && isset($current_value->{$k}) ) {
            $current_value = $current_value->{$k};
        } else {
            $current_value = $default;
            break;
        }
    }

    return $current_value;
}


/**
 * Array flatten
 */
function array_flatten( $array ) {
    $return = [];

    foreach ($array as $key => $value) {
        if (is_array($value)){
            $return = array_merge($return, array_flatten($value));
        } else {
            $return[$key] = $value;
        }
    }

    return $return;
}

/**
 * Checks if an array contains a list of keys
 */
function array_has_keys( array $array, array $required_keys ) {
    if (count(array_intersect_key(array_flip($required_keys), $array)) === count($required_keys)) {
        return true;
    }
    return false;
}


/**
 * Checks wether an array has at least one of the values. Set
 * strict to TRUE if the array must contain all values
 *
 * @param Array The needles to look for in the haystack
 * @param Array The haystack to search in
 * @param Boolean Set to true if array must have all needles (optional, default = false)
 * @return Boolean Yes when the needles have been found
 */
function array_includes( array $needles, array $haystack, $strict = false ) {
    $results = [];

    foreach($needles as $needle ) {
        $result = in_array($needle, $haystack);

        if( $result ) {
            if( $strict ) {
                array_push($results, $result );
            } else {
                return true;
            }
        }
    }
    return (count($results) === count($needles) );
}




// /**
//  * We need to do something to increase performance of this function. It can potentially be
//  * a peformance hit.
//  */
// function array_get( $array, $key, $default = null , $include_objects = false ) {
//     if( !is_array($array) ) {
//         return $default;
//     }

//     $keys           = explode('.', $key);
//     $current_value  = $array;

//     foreach($keys as $k) {
//        // var_dump($current_value);
//         if( is_array($current_value) && isset($current_value[$k]) ) {
//             $current_value = $current_value[$k];
//         } elseif( $include_objects && is_object($current_value) && isset($current_value->{$k}) ) {
//             $current_value = $current_value->{$k};
//         } else {
//             $current_value = $default;
//             break;
//         }
//     }
//     return $current_value;
// }



// /**
//  * Create an array with notation as keys
//  */
// function array_dot( $array , $include_objects = false,  $top_key = null ) {
//     $dot_values = [];

//     foreach($array as $key => $value ) {
//         if( $include_objects && is_object($value) ) {
//             $value = (array) $value;
//         }

//         if( is_array($value) ) {
//             $dot_values = array_merge($dot_values, array_dot($value, $include_objects, $key ) );

//         } else {
//             $key = is_null($top_key) ? $key : implode('.', [$top_key, $key]);
//             $dot_values[$key] = $value;
//         }
//     }

//     return $dot_values;
// }