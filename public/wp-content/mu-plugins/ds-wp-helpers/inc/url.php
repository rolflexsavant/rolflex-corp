<?php

/**
 * Short hand to easily grab a value from the querystring in
 * an url. It works with any url or url path and returns
 * FALSE on failure.
 *
 * @param string The name of the querystring parameter to grab
 * @param string A (part of a) URL to grab the parameter from
 * @return mixed The value of the parameter as a string or FALSE on failure
 */
function url_query_value($param_name, $url ) {
    $query = parse_url($url, PHP_URL_QUERY );
    parse_str( $query , $query_parts );

    if( isset($query_parts[$param_name]) ) {
        return $query_parts[$param_name];
    }

    return false;
}


