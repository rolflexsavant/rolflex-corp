<?php
/*
Plugin Name: Digital Savant Helpers
Plugin URI: https://www.digitalsavant.nl
Description: Small collection of helper functions related which are just handy to use in PHP or Wordpress. Stuff that you always want to have loaded and ready to use and don't cost performance when they aren't used.
Version: 0.1
Author: Digital Savant / Emiel Klein Ovink
Author URI: https://emiel.cc
License: GPLv2 or later
*/

if( !defined('DS_LOAD_DEBUG') ) {
    define('DS_LOAD_DEBUG', defined('WP_DEBUG') ? WP_DEBUG : false );
}

/* The rootpath */
define('DS_ROOTPATH' , dirname(__FILE__) );

/* The include path */
define('DS_INC', dirname(__FILE__).'/inc' );


/**
 * Load the files
 */
if( DS_LOAD_DEBUG ) {
    require_once DS_INC.'/debug.php';
    // By putting it here we prevent it to be loaded by accident when its not commented out
   // require_once DS_INC.'/play.php';
}

require_once DS_INC.'/array.php';
require_once DS_INC.'/string.php';
require_once DS_INC.'/url.php';
require_once DS_INC.'/general.php';

