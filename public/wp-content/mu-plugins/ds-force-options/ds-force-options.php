<?php
/*
Plugin Name: Digital Savant Force Options
Plugin URI: https://www.digitalsavant.nl
Description: Little plugin that allows you to force options in a website. A little bit tweaked for rolflex so it works nicely with WP-network sites and this specific use case.
Version: 0.1
Author: Digital Savant / Emiel Klein Ovink
Author URI: https://emiel.cc
License: GPLv2 or later
*/

/**
 * Wordpress has many options that define certain behaviour. All these options can
 * be managed in the admin. However, since we deal with a multilingual website
 * with multiple instances we want to force certain option settings.
 * The forced option settings are defined here. This will later on
 * probably move outside the theme and be placed somehwere else.
 */

define('DS_THEME_OPTIONS_FILENAME', 'options.php');

if( !defined('DS_FORCE_OPTIONS_ALWAYS') ) {
    define('DS_FORCE_OPTIONS_ALWAYS', false );
}


add_action('muplugins_loaded', 'ds_update_forced_options');


function ds_update_forced_options() {
    $forced_options = ds_get_forced_options();

    if( ds_force_updated_required($forced_options) ) {
        foreach($forced_options as $option => $value ) {
            update_option($option, $value );
        }

        if( constant('WP_DEBUG') == TRUE ) {
            add_action('wp_footer', 'ds_debug_force_options');
        }
    }
}


function ds_debug_force_options() {
    print "<!--\n";
    print_r(wp_load_alloptions());
    print "\n-->";
}


function ds_get_forced_options() {
    $forced_options = [];
    $search_paths = [ dirname(__FILE__), get_template_directory() ];

    foreach( $search_paths as $path ) {
        $options_file = rtrim($path, '/').'/'.DS_THEME_OPTIONS_FILENAME;

        if( file_exists($options_file) ) {
            $options        = include $options_file;
            $forced_options = array_merge($forced_options, $options );
        }
    }

    if( function_exists('ds_version') ) {
        $v = ds_version();
        $forced_options['ds_build'] = isset($v->build) ? (string) $v->build : '0' ;
    }

    return $forced_options;
}



function ds_force_updated_required($forced_options)  {
    $last_build_update     = (int) get_option('ds_build', 0 );
    $current_build_update  = (int) $forced_options['ds_build'];

    if( $current_build_update > $last_build_update ) {
        return true;
    }
    return DS_FORCE_OPTIONS_ALWAYS;
}






