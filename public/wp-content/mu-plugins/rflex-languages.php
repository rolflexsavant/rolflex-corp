<?php
/*
Plugin Name: Rolflex Languages
Plugin URI: https://www.rolflex.com
Description: Lightweight mu-plugin that handles the language detection and redirection for Rolflex. Contains the language options as well (hard coded).
Version: 0.1
Author: Rolflex / Emiel Klein Ovink
Author URI: https://emiel.cc
License: GPLv2 or later
*/

require_once dirname(__FILE__).'/rflex-languages/rflex-languages.php';