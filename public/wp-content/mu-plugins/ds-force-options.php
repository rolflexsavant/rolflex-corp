<?php
/*
Plugin Name: Digital Savant Force Options
Plugin URI: https://www.digitalsavant.nl
Description: Little plugin that allows you to force options in a website. A little bit tweaked for rolflex so it works nicely with WP-network sites and this specific use case.
Version: 0.1
Author: Digital Savant / Emiel Klein Ovink
Author URI: https://emiel.cc
License: GPLv2 or later
*/

require_once dirname(__FILE__).'/ds-force-options/ds-force-options.php';