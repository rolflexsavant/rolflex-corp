<?php
/*
Plugin Name: Rolflex Languages
Plugin URI: https://www.rolflex.com
Description: Lightweight mu-plugin that handles the language detection and redirection for Rolflex. Contains the language options as well (hard coded).
Version: 0.1
Author: Rolflex / Emiel Klein Ovink
Author URI: https://emiel.cc
License: GPLv2 or later
*/


define('RFLEX_LANGUAGE_COOKIE_KEY', 'rflex_user_lang' );

function rflex_current_user_language() {
    $user_language = null;

    if( isset($_COOKIE[RFLEX_LANGUAGE_COOKIE_KEY]) ) {
        $user_language = $_COOKIE[RFLEX_LANGUAGE_COOKIE_KEY];
    }

    if( is_null($user_language) ) {
        $browser_languages = array_map( function($language) {
            return strtok( strtok($language, '-') , ';');
        }, explode(',', $_SERVER['HTTP_ACCEPT_LANGUAGE']) );

        foreach($browser_languages as $browser_language ) {
            $available_language = rflex_get_available_language($browser_language);

            if( !is_null($available_language) ) {
                $user_language = $available_language['code'];
                break;
            }
        }
    }

    return $user_language;
}



function rflex_get_available_language( $language_code ) {
    foreach( rflex_get_language_options() as $option ) {
        if( $option['code'] === $language_code ) {
            return $option;
        }
    }
    return null;
}


function rflex_set_current_site_language() {
    $language = rflex_current_site_language();
    setcookie(RFLEX_LANGUAGE_COOKIE_KEY, $language['code'], time()+31556926, '/');
}


function rflex_current_site_language() {
    $request_uri    = explode('/', $_SERVER['REQUEST_URI'] );
    $language_code  = $request_uri[1];

    return rflex_get_available_language($language_code);
}


function rflex_decorate_language_option( $option ) {
    $option['url'] = '/'.$option['code'];
    return $option;
}


function rflex_get_language_options() {
    $options = [
        [
            'code' => 'nl',
            'label' => 'Nederlands',
        ],
        [
            'code' => 'en',
            'label' => 'English',
        ],
        [
            'code' => 'de',
            'label' => 'Deutsch',
        ],
        [
            'code' => 'fr',
            'label' => 'Français',
        ],
        // [
        //     'code' => 'es',
        //     'label' => 'Español',
        // ],
        [
            'code' => 'it',
            'label' => 'Italiano',
        ]
    ];

    foreach($options as $k => $option ) {
        $decorated_options[$k] = rflex_decorate_language_option($option);
    }

    return $decorated_options;
}