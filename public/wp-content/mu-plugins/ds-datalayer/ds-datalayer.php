<?php
/*
Plugin Name: Digital Savant DataLayer
Plugin URI: https://www.digitalsavant.nl
Description: Simple mu-plugin that interfaces with the Google Tag Manager datalayer.
Version: 0.1
Author: Digital Savant / Emiel Klein Ovink
Author URI: https://emiel.cc
License: GPLv2 or later
*/


/**
 * Simple PHP interface to use in Wordpress as a representation of the Google Tag Manager datalayer.
 */

class DsDataLayer {

    /**
     * Storing our singleton
     */
    protected static $_instance = null;

    /**
     * The array in which we store the data
     */
    protected $_data = [];

    /**
     * Constructor not publicly accessable, pass optional starting data as an array
     */
    protected function __construct( array $_data = [] ) {
        $this->_data = $_data;
        add_action('wp', [$this, 'init_defaults'] );
    }


    /**
     * Set default WP related values
     */
    public function init_defaults() {

        $this->push('contentType' , $this->get_content_type() );
        $this->push('pageType' , $this->get_page_type() );
    }


    /**
     * Get the content type we're dealing with
     */
    protected function get_content_type() {
        $content_type = get_post_type();

        if( $content_type == 'page' ) {
            $content_type = 'default';

            if( is_404() ) {
                $content_type = 'error';
            }
            if( is_front_page() ) {
                $content_type = 'front';
            }

            if( !empty( get_page_template_slug()) ) {
                $content_type = pathinfo(get_page_template_slug(), PATHINFO_FILENAME);
            }
       } else {
            $content_type = rtrim($content_type, 's'); // Remove s because of plural namings...
       }

       return ucfirst($content_type);
    }


    /**
     * Get the page type we're dealing with
     */
    protected function get_page_type() {
        if( is_page() ) {
            return 'Page';
        }
        if( is_singular() || is_single() ) {
            return 'Singular';
        }
        if( is_tax() || is_category() ) {
            return 'Taxonomy';
        }
        if( is_archive() || is_home() ) {
            return 'Archive';
        }
        if( is_search() ) {
            return 'Search';
        }
        if( is_404() ) {
            return '404';
        }
        return 'Misc';
    }

    /**
     * Get the Datalayer object
     */
    public static function getInstance( array $_data = [] ) {
        if( is_null(self::$_instance) ) {
          self::$_instance = new self( $_data );
        }

        return self::$_instance;
    }

    /**
     * Push a value into the datalayer
     */
    public function push( $name, $value ) {
        $this->_data[$name] = $value;
    }

    /**
     * Get a datalayer value
     */
    public function get($name) {
        if( isset($this->_data[$name]) ) {
            return $this->_data[$name];
        }
        return null;
    }

    /**
     * Remove something from the datalayer
     */
    public function remove($name) {
        if( isset($this->_data[$name]) ) {
            unset($this->_data[$name]);
        }
    }

    /**
     * Get the datalayer data as a Json string ready to print
     */
    public function toJson() {
        return json_encode($this->_data);
    }
}


/**
 * Shorthand to get the datalayer, functionally, Wordpress style...
 */
function ds_datalayer( array $_data = [] ) {
    return DsDataLayer::getInstance( $_data );
}