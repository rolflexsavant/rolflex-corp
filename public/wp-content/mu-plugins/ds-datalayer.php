<?php
/*
Plugin Name: Digital Savant DataLayer
Plugin URI: https://www.digitalsavant.nl
Description: Simple mu-plugin that interfaces with the Google Tag Manager datalayer.
Version: 0.1
Author: Digital Savant / Emiel Klein Ovink
Author URI: https://emiel.cc
License: GPLv2 or later
*/

require_once dirname(__FILE__).'/ds-datalayer/ds-datalayer.php';
