<?php
/*
Plugin Name: Digital Savant Helpers
Plugin URI: https://www.digitalsavant.nl
Description: Small collection of helper functions related which are just handy to use in PHP or Wordpress. Stuff that you always want to have loaded and ready to use and don't cost performance when they aren't used.
Version: 0.1
Author: Digital Savant / Emiel Klein Ovink
Author URI: https://emiel.cc
License: GPLv2 or later
*/

require_once dirname(__FILE__).'/ds-wp-helpers/ds-wp-helpers.php';
