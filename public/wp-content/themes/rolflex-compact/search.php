<?php
//d(get_post_type());

if( get_post_type() === 'articles' ) { // Get the articles hero if we're in the articles post type, to match the other page.
    $args['hero']   = get_hero_fields( get_archive_field('hero') );
    $args['title']  = get_archive_field('title');
    $post_type = get_post_type();
} else {
    $args['hero']   = [
        'style' => 'overlay_light',
        'image_large' => get_stylesheet_directory_uri().'/assets/img/hero.jpg'
    ];
    $args['title']  = __('Search the website', 'rctd');
    $post_type = '';
}

yield_header(); did_yield_breadcrumbs(true) ?>
<section <?php post_class('rflex-content rflex-modular'); ?>>
    <?php yield_part('section-hero', [
        'title' => $args['title'],
        'hero' => $args['hero'],
        'component' => [
            'view' => 'part-search-form',
            'args' => [
                'post_type' => $post_type,
            ],
        ]
    ]) ?>
    <div class="rflex-section">
        <div class="container-lg">
            <div class="row justify-content-center">
                <div class="col-12 col-md-10 col-lg-8">
                    <h2><?= sprintf( __('Showing search results for "%1s"', 'rctd'), get_search_query() ) ?></h2>
                    <hr />
                    <p class="rflex-search-meta"><?= rflex_paged_text() ?></p>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-12 col-md-10 col-lg-8">
                    <?php while( have_posts()): the_post(); ?>
                        <ul class="rflex-search-results">
                            <li class="rflex-search-results-item">
                                <h4 class="h4"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h4>
                                <p><?php the_result_description() ?></p>
                            </li>
                        </ul>
                    <?php endwhile; ?>

                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-12 col-md-10 col-lg-8">
                    <hr/>
                    <p class="rflex-search-meta"><?= rflex_paged_text() ?></p>
                </div>
            </div>
            <?php yield_part('part-archive-nav') ?>
        </div>
    </div>
</section>
<?php yield_footer(); ?>