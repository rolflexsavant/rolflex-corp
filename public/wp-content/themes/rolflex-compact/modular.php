<?php
/**
 * Template Name: Modular Template
 */

$fields = get_fields();

$args['hide_hero']      = !isset($fields['hide_hero']) ? false : $fields['hide_hero'];
$args['hide_content']   = !isset($fields['hide_content']) ? false : $fields['hide_content'];
$args['is_wide']        = !isset($fields['wide_content']) ? true : $fields['wide_content'];

if( !$args['hide_hero'] ) {
    $args['hero'] = get_hero_fields($fields['hero']);
}
?>
<?php yield_header(); ?>
<section <?php post_class('rflex-content rflex-modular'); ?>>
    <?php if( is_front_page() ): ?>
        <?php
            yield_part('section-hero-animated', [
                'title' => empty($args['title']) ? get_the_title() : $args['title'],
            // 'description' => $args['description'], @deprecated... should not return and caused confusion. If it causes bugs that this is commented, fix it...
                'hero' => $args['hero']
            ]);
        ?>
    <?php else: ?>
    <?php yield_part('section-content', $args ); ?>
    <?php endif; ?>
    <?php load_sections($fields['sections']); ?>

</section>
<?php yield_footer(); ?>