<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>

    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="HandheldFriendly" content="True">

    <?php wp_head(); ?>

</head>
<body <?php body_class(); ?>>

<div id="rflex-layout" class="rflex-layout">
    <header id="rflex-header-container" class="rflex-header">
        <div class="rflex-brand-bar">
            <div class="container-lg">
                <div class="row">
                    <div class="col">
                        <div class="rflex-logo-container">
                            <?php yield_part('logo'); ?>
                        </div>
                    </div>
                    <nav class="col-7 text-right d-lg-none">
                        <ul class="rflex-nav-icons rflex-list-inline">
                            <li><a href="tel://"><i class="rflex-icon rflex-icon-phone"></i><span class="rflex-icon-label"><?php _e('Call', 'rctd'); ?></span></a></li>
                            <li><a class="rflex-toggle-search" href="#"><i class="rflex-icon rflex-icon-search"></i><span class="rflex-icon-label"><?php _e('Search', 'rctd'); ?></span></a></li>
                            <li><a href="#" class="rflex-toggle-mob-nav"><i class="rflex-icon rflex-icon-menu rflex-anchor-prevent-default"></i><span class="rflex-icon-label"><?php _e('Menu', 'rctd'); ?></span></a></li>
                        </ul>
                    </nav>
                    <div class="d-none d-lg-block col text-right">
                        <nav class="rflex-nav-top">
                            <ul class="rflex-list-inline">
                                <?php if( !empty(get_info('special_pages.contact')) ): ?>
                                <li class="menu-item menu-item-type-post_type_archive menu-item-object-projects menu-item-492">
                                    <a href="<?php info('special_pages.contact') ?>"><?php _e('Contact &amp; Service' , 'rctd' ) ?></a>
                                </li>
                                <?php endif; ?>
                                <li class="menu-item menu-item-type-post_type_archive menu-item-object-projects menu-item-492">
                                    <?= yield_part('part-language-selector') ?>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <?php yield_part('part-desktop-navbar', ['classes' => 'd-none d-lg-block'] ); ?>
    </header>

	<?php if( isset($args['main_classes'] ) ): ?>
		<main id="rflex-main" class="rflex-main <?= $args['main_classes'] ?>">
	<?php else: ?>
    	<main id="rflex-main" class="rflex-main">
	<?php endif; ?>