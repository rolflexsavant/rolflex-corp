<?php
/**
 * For the posts overview page we also want to use the logic from "archive".
 */

$ID     = get_option( 'page_for_posts' );
$fields = get_fields($ID);
$hero   = get_hero_fields($fields['archive_post']['hero']);
$title  = $fields['archive_post']['title'];

yield_template('archive', [
    'title' => $title,
    'hero' => $hero,
    'super' => get_the_title($ID),
])
?>