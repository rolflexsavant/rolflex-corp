��    o      �  �         `	     a	     i	     y	     �	     �	     �	     �	     �	     �	     �	     
     
      
     -
  
   >
     I
     U
     b
     o
     w
     �
     �
     �
     �
  
   �
     �
     �
     �
     �
     �
     �
     �
     �
        	             +     <     I     V     h     u  
   �     �     �     �     �  	   �     �     �     �     �  
   �  
   �                    $     4     @     M     h     t     �  	   �     �  
   �     �     �     �     �     �     �               )     8     J     R  	   [     e     m     t     �     �     �     �     �     �     �     �               *  {   7     �     �     �     �     �     �                    0     =     F     `     e     r  �  w          +     C     ]     y     �     �     �     �     �     �               $     6  
   G     R     `     o  	   w     �     �     �     �     �     �     �     �     �     �     �     �     �       	        !     -     A     R     d     y     �     �  
   �  	   �     �     �     �     �     �  
   �     
       	   /  
   9     D     R     W     k     y     �     �     �     �     �     �     �          &     .     :     G     \     p     }     �     �     �  	   �     �     �     �     �     �               "     6     N     `     t     �     �     �  y   �  	   1     ;     G     d     x     �     �     �     �     �  
   �     �                     \   #   9      o   4               .      X   l   g      >         C           c      <                     Y   !          k   W       _   6   5       1   /   B   
           b   V                         ^      '       G   [   7   a   +   i             J           $   `   d      R   2          (   ?   *      m   ]          A   L   K      I   ;   ,   F                 3         T   Q   h       M   0   H   O          U       S       N       )         E   e                 :   f   	   -   %   j   P       @   Z           D       "       &   n   8       =        Add New Add New Article Add New Download Add New Feature Add New Organization Add New Project Add New Testimonial Address information All Applications All Articles All Downloads All Features All Projects All Testimonials All Topics Application Applications Archive page Article Articles BTW Back to overview Blog archive Call Categories City Client Company name Contact Cookies Country Download Download Categories Download Category Downloads E-mail address Edit Application Edit Article Edit Feature Edit Organization Edit Project Edit Testimonial Edit Topic Feature Features Filename Footer Menu Full name Get in touch with us IBAN Image Image Gallery Image grid Impression Included Knowledge center Menu New Application New Article New Download New Download Category Name New Feature New Project New Testimonial New Topic New Topic Name Next image No Testimonials Found Oops... Organization Organizations Page not found Parent Feature Parent Projects Photo gallery Previous image Privacy statement Project Projects Read more Related Search Search Application Search Articles Search Downloads Search Features Search Projects Search Testimonials Site settings Submit form Telephone number Terms &amp; conditions Testimonial Testimonials The page you were looking for could not be found. It might have been removed, renamed, or did not exist in the first place. Topic Topics Update Download Category Update Topic Video player View Articles View Downloads View Projects View Testimonials View project Visit us Was this article helpful? Year applications more Plural-Forms: nplurals=2; plural=(n != 1);
Project-Id-Version: Rolflex Compact Door
PO-Revision-Date: 2020-10-01 12:40+0200
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.4.1
X-Poedit-Basepath: ..
X-Poedit-Flags-xgettext: --add-comments=translators:
X-Poedit-WPHeader: style.css
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
Last-Translator: 
Language: nl
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.min.js
 Nieuwe toevoegen Nieuw artikel toevoegen Nieuwe Download Toevoegen Nieuwe Eigenschap Toevoegen Organisatie toevoegen Nieuw Project toevoegen Nieuwe recensie toevoegen Adresinformatie Alle Sollicitaties Alle artikelen Alle Downloads Alle eigenschappen Alle Projecten Alle testimonials Alle onderwerpen Applicatie Sollicitaties Archief pagina Artikel Artikelen BTW Terug naar overzicht Blog archief Bel Categorieën Stad Klant Bedrijfsnaam Contact Cookies Land Download Download categorieën Download categorie Downloads E-mailadres Bewerk sollicitatie Artikel bewerken Bewerk Eigenschap Organisatie bewerken Project Bewerken Bewerk referentie Bewerk Onderwerp Eigenschap Kenmerken Bestandsnaam Footer Menu Volledige naam Neem contact op IBAN Afbeelding Afbeeldingsgalerij Afbeeldingsraster Impressie Inbegrepen Kenniscentrum Menu Nieuwe sollicitatie Nieuw artikel Nieuwe Download Nieuwe Download Categorie Naam Nieuw kenmerk Nieuw project Nieuwe referentie Nieuw Onderwerp Nieuwe onderwerp naam Volgende afbeelding geen reviews gevonden Oeps... Organisatie Organisaties Pagina niet gevonden Hoofd Eigenschappen Testimonials Foto galerij Vorige afbeelding Privacy verklaring Project Projecten Lees verder Gerelateerd Zoeken Zoek sollicitatie Zoek artikelen Downloads Zoeken Zoek Eigenschap Zoeken in projecten Testimonials doorzoeken Site instellingen Formulier verzenden Telefoonnummer Algemene &amp voorwaarden Testimonial Beoordelingen De pagina die u zocht kon niet worden gevonden. Het is mogelijk verwijderd, hernoemd of bestond in de eerste plaats niet. Onderwerp Onderwerpen Download Categorie Bijwerken Onderwerp Bijwerken Videospeler Artikelen bekijken Downloads bekijken Bekijk projecten Bekijk referenties Bekijk project Bezoek ons Was dit artikel nuttig? Jaar sollicitaties meer 