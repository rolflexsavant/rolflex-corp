��    e      D  �   l      �     �     �     �     �     �     �     �     �     	     $	     2	     ?	  
   P	     [	     g	     t	     �	     �	     �	     �	     �	  
   �	     �	     �	     �	     �	     �	     �	     �	     �	     
     
  	   -
     7
     F
     S
     a
     x
     �
     �
  
   �
     �
     �
     �
     �
     �
  	   �
     �
                 
     
   %     0  
   9     D     I     U     b     }     �     �  	   �  
   �     �     �     �     �     �               #     +  	   4     >     F     M     ]     n     ~     �     �     �     �     �     �  {   �     h     n     u     �     �     �     �     �     �     �     
            �  !  
   �               8     K     _     s     �  '   �     �     �     �       	     	        (     8  	   A     K     a     p  	   y     �     �     �     �     �  
   �     �     �     �     �     �                  %   4     Z     n     }     �  	   �     �     �     �     �     �     �                         -     <     E     Z     _     m  )   �     �     �     �  
   �     �     �                    4     D  %   X     ~     �     �  
   �     �     �     �     �     �     �               0  
   D  
   O  y   Z     �     �  )   �               -     =     L     [     v     �  	   �     �     4          2      ;             :   `       I       Q   C   E   D   X              !       "   c   (   &       b   9       J       +   ]   @       d              $       e       S          =           N   O   T             7   K          8      ?      Z           0   W   3            R          1   Y   [         U   %          L       5   ^           ,                  F   V   B   A       G       \   
      -   '              6   >   H         P             <   	   _      /   .       a      M       #   )   *    Add New Add New Article Add New Download Add New Feature Add New Project Add New Testimonial All Applications All Articles All Download Categories All Downloads All Projects All Testimonials All Topics Application Applications Archive page Article Articles Back to overview Blog archive Call Categories City Client Close search Company name Contact Cookies Country Download Download Categories Download Category Downloads E-mail address Edit Article Edit Download Edit Download Category Edit Organization Edit Project Edit Testimonial Edit Topic Feature Features Filename Filesize Footer Menu Full name Get in touch with us IBAN Image Image Gallery Image grid Impression Included Learn more Menu New Article New Download New Download Category Name New Feature New Project New Testimonial New Topic Next image No Testimonials Found Oops... Organization Page not found Photo gallery Previous image Privacy statement Project Projects Read more Related Search Search Articles Search Downloads Search Features Search Projects Search Testimonials Site settings Submit form Terms &amp; conditions Testimonial Testimonials The page you were looking for could not be found. It might have been removed, renamed, or did not exist in the first place. Topic Topics Update Download Category Video player View Articles View Projects View Testimonials View project Was this article helpful? What are you looking for? Year applications more Project-Id-Version: Rolflex Compact Door
PO-Revision-Date: 2020-10-21 15:06+0200
Last-Translator: 
Language-Team: 
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : 2);
X-Generator: Poedit 2.4.1
X-Poedit-Basepath: ..
X-Poedit-Flags-xgettext: --add-comments=translators:
X-Poedit-WPHeader: style.css
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.min.js
 Dodaj nowy Dodaj nowy artykuł Dodaj nowy plik do pobrania Dodaj nową cechę Dodaj nowe zlecenie Dodaj nową opinię Wszystkie aplikacje Wszystkie Artykuły Wszystkie kategorie plików do pobrania Wszystkie pliki do pobrania Wszystkie projekty Wszystkie opinie Wszystkie tematy Aplikacja Aplikacje Strona Archiwum Artykuł Artykuły Powrót do przeglądu Archiwum bloga Zadzwoń Kategorie Miasto Klient Zamknij wyszukiwanie Nazwa firmy Kontakt Ciasteczka Kraj Pobierz Kategorie downloadu Kategoria downloadu Pliki Adres e-mail Edytuj atykuł Edytuj plik do pobrania Edytuj kategorię plików do pobrania Edytuj organizację Edytuj projekt Edytuj opinię Edytuj temat Wyróznij Funkcje Nazwa pliku Rozmiar pliku Menu w stopce Imię i nazwisko Pozostań z nami w kontakcie IBAN Obraz Galeria zdjęć Siatka obrazków Wycisk cyfrowy Wliczono Dowiedz się więcej Menu Nowy artykuł Nowy plik do pobrania Nazwa nowej kategorii plików do pobrania Nowa Funkcja Nowe zlecenie Nowa opinia Nowy temat Następne zdjecie Nie znaleziono referencje Ups... Organizacja Nie znaleziono strony Galeria zdjęć Poprzednie zdjęcie Oświadczenie o ochronie prywatności Projekt Projekty Czytaj więcej Powiązane Szukaj Szukaj artykułów Szukaj plików do pobrania Szukaj cech Szukaj projektu Szukaj opinii Właściwości strony Wyślij formularz Regulamin i warunki Referencja Referencje Nie można odnaleźć strony, której szukasz. Mogła zostać usunięta, zmieniono jej nazwę lub w ogóle nie istniała. Temat Tematy Aktualizuj kategorię plików do pobrania Odtwarzacz wideo Zobacz artykuły Zobacz zlecenia Pokaż opinię Pokaż projekt Czy artykuł był pomocny? Czego szukasz? Rok aplikacje więcej 