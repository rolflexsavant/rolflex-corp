��    y      �  �   �      8
     9
     A
     Q
     b
     r
     �
     �
     �
     �
     �
     �
     �
     �
  
                  )     6     >     G     K     \     i  
   n     y     �     �     �     �     �     �     �     �     �  	   �                      -     ;     R     _     l  
   }     �     �     �     �     �     �  	   �     �     �     �     �  
   �  
                       +     8     S     _     k  	   {     �  
   �     �     �     �     �     �          0     8     E     S     b     q     �     �     �     �     �     �  	   �     �     �     �     �               ,     @  (   M     v     �     �     �     �     �     �  {   �     i     o     v     �     �     �     �     �     �     �     �               (     -     :  �  ?     �     �          #     =     W     u     �     �     �     �     �     �     �  	         
          "     *     2     7     N     Z  
   `  5   k     �     �  
   �     �     �     �     �     �     �  	   �               +     >     R     o     �     �     �     �     �     �  	   �     �     �               ,     1     6     D     X     d     p     v     �     �     �     �     �     �     �     �       &        E  %   ]     �  )   �     �     �     �     �               )     7     G     ]     e     n     |     �     �     �     �     �     �     �     �  '        /     J     `     v     �     �  
   �  y   �     1     7     >     V     v     �     �     �     �     �     �               1     6     B     6           k   B   Y   &   j      <   G          n          x   ?   9                   2   v   *       '       
      H   E      3       /   f                           >   p   X          w   M       l                 4       (       ,   \   W   D   +   e   %   c      !       )   S   g   L   t   r   O   8       K   R       d               _   o      V   N       P          F   ;          :   #   -                              0   I   ^       ]   u   T   q   y      m   5   C      b       	          a   s       J       7   Z   =   [   $      `       h   1   @   U   "                     .          Q   i   A        Add New Add New Article Add New Download Add New Feature Add New Project Add New Testimonial All Applications All Articles All Download Categories All Downloads All Features All Projects All Testimonials All Topics Application Applications Archive page Article Articles BTW Back to overview Blog archive Call Categories Choose from most used Topics City Client Company name Contact Cookies Country Download Download Categories Download Category Downloads E-mail address Edit Application Edit Article Edit Download Edit Download Category Edit Feature Edit Project Edit Testimonial Edit Topic Embed video Feature Features Filename Filesize Footer Menu Full name Get in touch with us IBAN Image Image Gallery Image grid Impression Included Menu New Article New Download New Download Category Name New Feature New Project New Testimonial New Topic New Topic Name Next image No Downloads Found No Downloads Found in Trash No Projects Found No Projects Found in Trash No Testimonials Found No Testimonials Found in Trash Oops... Organization Organizations Page not found Parent Feature Parent Projects Photo gallery Previous image Privacy statement Project Projects Projects archive Read more Related Search Search Articles Search Downloads Search Features Search Projects Search Testimonials Search Topic Separate Download Categories with commas Separate Topics with commas Site settings Submit form Telephone number Terms &amp; conditions Testimonial Testimonials The page you were looking for could not be found. It might have been removed, renamed, or did not exist in the first place. Topic Topics Update Application Update Download Category Update Topic Video player View Articles View Features View Projects View Testimonials View project Visit us Was this article helpful? Year applications more Plural-Forms: nplurals=2; plural=(n != 1);
Project-Id-Version: Rolflex Compact Door
PO-Revision-Date: 2020-10-01 12:39+0200
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.4.1
X-Poedit-Basepath: ..
X-Poedit-Flags-xgettext: --add-comments=translators:
X-Poedit-WPHeader: style.css
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
Last-Translator: 
Language: de
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.min.js
 Neu hinzufügen Neuen Artikel hinzufügen Neuen Download anlegen Neue Funktion hinzufügen Neues Projekt hinzufügen Neues Testimonial hinzufügen Alle Bewerbungen Alle Artikel Alle Download Kategorien Alle Downloads Alle Eigenschaften Alle Projekte Alle Erfahrungsberichte Alle Themen Bewerbung Anwendungen Archivseite Artikel Artikel MwSt Zurück zur Übersicht Blog-Archiv Anruf Kategorien Wählen Sie aus den am häufigsten verwendeten Themen Stadt Kunde Firmenname Kontakt Cookies Land Download Download Kategorien Download Kategorie Downloads E-Mail-Adresse Anwendung bearbeiten Artikel bearbeiten Download bearbeiten Bearbeite Download Kategorie Funktion bearbeiten Projekt bearbeiten Testimonial bearbeiten Thema bearbeiten Video einbetten Feature Features Dateiname Dateigröße Footer Menü Vollständiger Name Kontaktieren Sie uns IBAN Bild Bildergalerie Galerie Grid Format Einblendung Inbegriffen Menü Neuer Artikel Neuer Download Neue Download Kategorie Name Neues Feature Neues Projekt Neues Testimonial Neues Thema Neues Thema Nächstes Bild Keine Downloads gefunden Keine Downloads im Papierkorb gefunden Keine Projekte gefunden Keine Projekte im Papierkorb gefunden Keine Testimonials gefunden Keine Testimonials gefunden im Papierkorb Ups... Organisation Organisationen Seite nicht gefunden Übergeordnetes Merkmal Parent Projekte Bildergalerie Vorheriges Bild Datenschutzerklärung Projekt Projekte Projektarchiv Weiterlesen Ähnlich Suche Artikel durchsuchen Downloads suchen Eigenschaften suchen Projekte suchen Testimonials durchsuchen Thema suchen Download Kategorien mit Kommata trennen Themen durch Komma trennen Website-Einstellungen Formular übermitteln Telefonnummer Bedingungen und Konditionen Testimonial Referenzen Die von Ihnen gesuchte Seite wurde nicht gefunden. Möglicherweise wurde sie gelöscht, umbenannt oder es gibt sie nicht. Thema Themen Anwendung aktualisieren Aktualisiere Download Kategorie Thema aktualisieren Videospiele Hilfebeiträge anzeigen Eigenschaften anzeigen Projekte anzeigen Erfahrungsberichte ansehen Projekt ansehen Besuchen Sie uns War dieser Beitrag hilfreich? Jahr Bewerbungen mehr 