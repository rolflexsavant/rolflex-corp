��    S      �  q   L                     )     9     I     ]     j     w     �  
   �     �     �     �     �     �     �     �  
   �     �     �                     "     *     2     ;  	   M     W     f     w     �     �     �     �  
   �     �     �     �     �     �  	   �     	     	     	     !	  
   /	     :	  
   C	     N	     S	     _	     l	     x	     �	  
   �	     �	     �	     �	     �	     �	     �	  	   �	     �	     �	     
     
     "
     2
     F
     T
     `
  {   m
     �
     �
     �
                    5     :     G  �  L     �     �          (     ?     R     `     p     �  
   �     �  	   �  
   �     �     �  
   �     �  
   �     �     �     �  	                  +     0     9     M     Z     h     v     �     �     �     �     �     �  
   �     �  
   �       
             )     .     4     A  	   P     Z     c     h     t     �     �  	   �     �     �     �     �     �     �  
   �     �           	               ,     >     M     `     g  p   o     �     �     �     �  
   
          2     6     D     @   O   *              H          %              =   )   	   4      3                   <   7                  D      2      I      1   >           ;       C       $   ,       :         P         ?   B      -   S   "          G   E   .   J   '          K      #      8   (          0           6   
   !   A   M              &   +   N   5       9          L           Q   F             /   R                              Add New Add New Article Add New Feature Add New Project Add New Testimonial All Articles All Features All Projects All Testimonials All Topics Application Applications Archive page Article Articles Blog archive Call Categories City Client Close search Company name Contact Cookies Country Download Download Category Downloads E-mail address Edit Application Edit Article Edit Download Edit Feature Edit Project Edit Testimonial Edit Topic Feature Features Filename Filesize Footer Menu Full name Get in touch with us IBAN Image Image Gallery Image grid Included Learn more Menu New Article New Download New Feature New Project New Testimonial Next image Organization Page not found Photo gallery Previous image Project Projects Read more Related Search Search Articles Search Features Search Projects Search Testimonials Site settings Testimonial Testimonials The page you were looking for could not be found. It might have been removed, renamed, or did not exist in the first place. Topic Topics Update Topic View Downloads Visit us What are you looking for? Year applications more Project-Id-Version: Rolflex Compact Door
PO-Revision-Date: 2020-10-21 15:07+0200
Last-Translator: 
Language-Team: 
Language: nb_NO
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.4.1
X-Poedit-Basepath: ..
X-Poedit-Flags-xgettext: --add-comments=translators:
X-Poedit-WPHeader: style.css
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.min.js
 Legg til ny Legg til ny artikkel Legg til ny egenskap Legg til nytt prosjekt Legg til ny omtale Alle Artikler Alle egenskaper Alle prosjekter Alle anbefalinger All temaer Søknad Søknader Arkiv side Artikkel Artikler Bloggarkiv Ring Kategorier Poststed Kunde Lukk søkefelt Firmanavn Kontakt Informasjonskapsler Land Last ned Nedlastingskategori Nedlastinger E-postadresse Endre søknad Rediger Artikkel Rediger nedlastning Rediger egenskaper Rediger prosjekt Rediger omtale Rediger Emne Egenskap Funksjoner Filnavn Størrelse Bunnmeny Fullt navn Ta kontakt med oss IBAN Bilde Bildegalleri Bilde-rutenett Inkludert Lær mer Meny Ny artikkel Ny nedlastning Ny Funksjon Nytt prosjekt Ny omtale Neste bilde Organisasjon Side ikke funnet Fotogalleri førre Prosjekt Prosjekter Les mer Lignende Søk Søk Artikler Søkefunksjoner Søk i prosjekter Søk i omtaler Site-innstillinger Omtale Omtaler Siden du lette etter ble ikke funnet. Den kan ha blitt fjernet, omdøpt, eller eksisterte ikke i første omgang. Tema Emner Oppdater emne Vis nedlastinger Besøk oss Behandlingsform eller tittel År applikasjoner Les mer &raquo; 