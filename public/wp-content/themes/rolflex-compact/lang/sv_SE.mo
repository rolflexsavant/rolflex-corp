��    [      �     �      �     �     �     �     �               *     >     O     \     j     w     �  
   �     �     �     �     �     �     �     �     �  
   �     	     
	     	     	     &	     .	     6	  	   ?	     I	     X	     i	     v	     �	     �	  
   �	     �	     �	     �	     �	     �	  	   �	     �	     �	     �	     
  
   
     
  
   '
     2
     7
     G
     S
     _
     k
  	   {
  
   �
     �
     �
     �
     �
     �
     �
     �
  	   �
     �
     �
               &     6     F     Z     k     w  {   �                           -     ;     I     [     u     �     �     �  �  �     I     W     m     �     �     �     �     �     �               "     /     =  
   I     T  	   a     k     s     |  
   �     �  
   �     �     �     �     �     �     �  	   �     �     �               "     6     G     X     f  
   o     z  
   �     �     �     �     �     �     �     �  
   �     �     �     �  
                   $  
   1     <     H     b     j     w     �     �     �     �  	   �     �     �     �     �     �     �          "     *  e   3     �     �     �     �     �     �     �  $   �          -     1     >            K   B      R      (       $      2                 C                       7   S   6                &   @   *       3           Y   )   1   D   L           U      [       P       ?   J   /   :       4   0               ,   !   I   -   G           9              A       8   =       M       #   F   N         +                  V      X       E       <   ;   '   5   "           %      Q   W   Z                  
   H   >             T   .                   	      O    Add New Add New Article Add New Feature Add New Organization Add New Project Add New Testimonial Address information All Applications All Articles All Downloads All Features All Projects All Testimonials All Topics Application Applications Archive page Article Articles Back to overview Blog archive Call Categories City Client Company name Contact Cookies Country Download Downloads E-mail address Edit Application Edit Article Edit Feature Edit Project Edit Testimonial Edit Topic Feature Features Filename Filesize Footer Menu Full name Get in touch with us IBAN Image Image Gallery Image grid Included Learn more Menu New Application New Article New Feature New Project New Testimonial New Topic Next image No Testimonials Found Oops... Organization Page not found Previous image Project Projects Read more Related Search Search Application Search Articles Search Features Search Projects Search Testimonials Telephone number Testimonial Testimonials The page you were looking for could not be found. It might have been removed, renamed, or did not exist in the first place. Topic Topics Update Application Update Topic View Articles View Projects View all projects Was this article helpful? What are you looking for? Year applications more Project-Id-Version: Rolflex Compact Door
PO-Revision-Date: 2020-10-21 15:06+0200
Last-Translator: 
Language-Team: 
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.4.1
X-Poedit-Basepath: ..
X-Poedit-Flags-xgettext: --add-comments=translators:
X-Poedit-WPHeader: style.css
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.min.js
 Lägg till ny Lägg Till Ny Artikel Lägg till ny funktion Lägg till ny organisation Lägg till nytt projekt Lägg till nytt omdöme Adressinformation Alla ansökningar Alla artiklar Alla nedladdningar Alla features Alla projekt Alla omdömen Alla Ämnen Ansökning Ansökningar Arkivsida Artikel Artiklar Tillbaka till översikt Bloggarkiv Ring Kategorier Stad Kund Företagsnamn Kontakt Cookies Land Ladda ner Nedladdningar E-postadress Ändra ansökan Redigera Artikel Redigera funktionen Redigera projekt Redigera omdöme Redigera Sida Funktion Funktioner Filnamn Filstorlek Sidfotsmeny Fullständigt namn Kontakta oss IBAN Bild Bildgalleri Bildrutnät Inkluderad Läs mer Meny Ny ansökan Ny artikel Ny funktion Nytt projekt Nytt omdöme Nytt ämne Nästa bild Ingen Referenser hittades Oops… Organisation Sidan hittades inte Föregående bild Projekt Projekt Läs mer Relaterat Sök Sök ansökan Sök artiklar Sök Funktioner Sök projekt Sök rekommendationer Telefonnummer Omdöme Omdömen Sidan du letade efter kunde inte hittas. Den kan ha tagits bort, bytt namn, eller har aldrig funnits. Ämne Ämnen Uppdatera ansökan Uppdatera ämne Visa artiklar Visa projekt Se alla projekt Fick du hjälp av den här artikeln? Vad letar du efter? År ansökningar mer 