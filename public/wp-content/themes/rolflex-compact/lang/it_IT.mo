��    j      l  �   �      	     	     	     )	     :	     J	     Z	     n	     �	     �	     �	     �	     �	     �	  
   �	     �	     �	     �	     

     
     
     
     0
     =
  
   B
     M
     R
     Y
     f
     n
     
     �
     �
     �
     �
     �
  	   �
     �
     �
                    *     <     I  
   Z     e     q     y     �     �  	   �     �     �     �     �  
   �     �     �     �     �                    &  	   6     @  
   O     Z     l     �     �     �     �     �     �     �     �            	             &     -     @     P     a     q     �     �     �     �     �  {   �     C     I     P     ]     j     x     �     �     �     �     �     �  �  �     h     w     �     �     �     �     �          !     4     E     ^     o     �     �     �     �     �     �     �     �     �     �  	                       *     3     E     M     U     j     s     �  	   �     �     �     �     �     �     �          +     B     U     g     v     �     �     �     �     �     �     �     �                    #     2     A     P     _     s     �     �     �     �  &   �     
          !     4     N     \     p     �     �     �  	   �     �     �     �     �     �               &     8     J     V  t   d  	   �  	   �     �        
        '     =     V     d     m     r     ~         $          j   7               1      X              Z         C           e      =                     Y   "          g   W       f   9   8       /   2   B   L           `   V                         \   
   (       I   [       _   -             E   K           %   ^   a      R   5   T      *   !   ,      +   ?          A   ]          J   <   .   H                 6         4   Q   d       M   0       O       	   U       G       N       )         F   b       3         ;   c         &       P       @   S   h       D       #       '   i   :       >        Add New Add New Article Add New Download Add New Feature Add New Project Add New Testimonial Address information All Applications All Articles All Downloads All Features All Projects All Testimonials All Topics Application Applications Archive page Article Articles BTW Back to overview Blog archive Call Categories City Client Company name Contact Contact Template Cookies Country Do you want to know more? Download Download Categories Download Category Downloads E-mail address Edit Application Edit Article Edit Download Edit Feature Edit Organization Edit Project Edit Testimonial Edit Topic Embed video Feature Features Filename Filesize Full name Get in touch with us IBAN Image Image Gallery Image grid Included Menu New Application New Article New Download New Feature New Project New Testimonial New Topic New Topic Name Next image No Projects Found No Testimonials Found No Testimonials Found in Trash Oops... Organization Page not found Parent Feature Photo gallery Previous image Privacy statement Project Projects Read more Related Search Search Application Search Articles Search Downloads Search Features Search Projects Search Testimonials Search Topic Submit form Testimonial Testimonials The page you were looking for could not be found. It might have been removed, renamed, or did not exist in the first place. Topic Topics Update Topic Video player View Articles View Projects View Testimonials View project Visit us Year applications more Plural-Forms: nplurals=2; plural=(n != 1);
Project-Id-Version: Rolflex Compact Door
PO-Revision-Date: 2020-10-01 12:42+0200
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.4.1
X-Poedit-Basepath: ..
X-Poedit-Flags-xgettext: --add-comments=translators:
X-Poedit-WPHeader: style.css
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
Last-Translator: 
Language: it
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.min.js
 Aggiungi Nuovo Aggiungi nuovo articolo Aggiungi nuovo download Aggiungi Nuova Caratteristica Aggiungi nuovo progetto Aggiungi Nuovo Testimonial Riepilogo indirizzo Tutte le candidature Tutti gli articoli Tutti i Download Tutte le caratteristiche Tutti i progetti Tutte le testimonianze Tutte le discussioni Applicazione Candidature Pagina archivio Articolo Articoli IVA Torna alla panoramica Blog Archive Chiama Categorie Città Cliente Nome Azienda Contatto Modello  contatti Cookies Nazione Vuoi sapere di più? Download Categorie Download Categoria Download Downloads Indirizzo email Modifica applicazione Modifica Articolo Modifica Download Modifica Progetto Modificare Organizzazione Modifica Progetto Modifica Testimonianza Modifica Argomento Video incorporato Caratteristica Caratteristiche Nome del file Dimensione del file: Nome completo Mettiti in contatto con noi IBAN Immagine Galleria Immagini Griglia immagini Incluso Menu Nuova Candidatura Nuovo articolo Nuovo download Nuovo Progetto Nuovo progetto Nuova Testimonianza Nuovo Argomento Nome del nuovo Argomento Immagine successiva Nessun progetto trovato Nessun Testimonial Trovato Nessun Testimonial Trovato nel cestino Oops… Organizzazione Pagina non trovata Caratteristica principale Galleria foto Immagine precedente Informativa sulla privacy Progetto Progetti Leggi di più Correlati Cerca Cerca applicazione Cerca articoli Cercare download Cerca caratteristiche Cerca Progetti Cerca testimonianze Cerca Discussione Inviare il modulo Testimonial Testimonianze La pagina che stai cercando non è stata trovata. Potrebbe essere stata eliminata, rinominata o non è mai esistita. Argomento Argomenti Aggiorna Argomento Lettore di videoGenericName Visualizza Visualizza Iniziative Visualizza testimonianza Apri progetto Visitaci Anno candidature più 