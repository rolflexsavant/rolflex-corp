��    _                      	          !     2     B     R     f     s     �     �     �  
   �     �     �     �     �     �     �  
   	     	     	     	     %	     -	     >	     F	     N	     W	     k	  	   }	     �	     �	     �	     �	     �	     �	     �	     �	  
   
     
     
      
     )
  	   5
     ?
     D
     J
  
   X
     c
  
   l
     w
     |
     �
     �
     �
     �
  	   �
  
   �
     �
     �
               (     7     F     U     ]  	   f     p     x          �     �     �     �     �     �     �             {        �     �     �     �     �     �     �     �                ,     F     K  �  P  
   �     �          &     9     M     b     p     �     �     �  
   �  
   �     �     �     �     �        
   	            	   "     ,     4     D     L     Q     Z     n  	   �     �     �     �     �     �     �     �               $  
   ,     7     ?  
   L     W     \     d     r  
   �  	   �     �  
   �     �  
   �     �     �     �     �        $        <     I     X     o     �     �  	   �  	   �  
   �     �     �     �     �     �       	             0  	   <  
   F  j   Q     �     �     �     �     �     �     
          %     9     S     d     h        R   L   ?       6      )       %      1              =   D          Y   U      	   5   V   9                '       +       2   W      T   *   0   E   O       $   Z      _      S       >   K   .   X       3   /       ^       ,       J   -   N       I   8              B   C   7   H      P   M      A   Q          G                  [      ]       <           :   (   4   !           &      #   \                        F                      @      
      "      ;          Add New Add New Article Add New Download Add New Feature Add New Project Add New Testimonial All Articles All Download Categories All Features All Projects All Testimonials All Topics Application Applications Article Articles Back to overview Call Categories City Client Company name Contact Contact Template Cookies Country Download Download Categories Download Category Downloads E-mail address Edit Application Edit Article Edit Download Edit Download Category Edit Feature Edit Project Edit Testimonial Edit Topic Feature Features Filename Footer Menu Full name IBAN Image Image Gallery Image grid Included Learn more Menu New Article New Download Category Name New Feature New Project New Testimonial New Topic Next image No Projects Found No Projects Found in Trash Organization Organizations Page not found Parent Feature Previous image Project Projects Read more Related Search Search Articles Search Downloads Search Features Search Projects Search Testimonials Search Topic Site settings Telephone number Testimonial Testimonials The page you were looking for could not be found. It might have been removed, renamed, or did not exist in the first place. Topic Topics Update Download Category Update Topic Video player View Articles View Downloads View Projects View all projects Was this article helpful? What are you looking for? Year more Project-Id-Version: Rolflex Compact Door
PO-Revision-Date: 2020-10-21 15:09+0200
Last-Translator: 
Language-Team: 
Language: da
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.4.1
X-Poedit-Basepath: ..
X-Poedit-Flags-xgettext: --add-comments=translators:
X-Poedit-WPHeader: style.css
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.min.js
 Tilføj ny Tilføj ny artikel Tilføj nyt download Tilføj ny feature Tilføj Nyt Projekt Tilføj ny udtalelse Alle artikler Alle download kategorier Alle Forhold Alle projekter Alle Udtalelser Alle emner Ansøgning Ansøgninger Artikel Artikler Tilbage til oversigten Ring til Kategorier Kommune Kunde Firmanavn Kontakt Kontaktskabelon Cookies Land Download Download kategorier Download kategori Downloads E-mail adresse Rediger ansøgning Rediger artikel Rediger download Rediger download kategori Rediger Idé Redigér Projekt Rediger udtalelse Rediger emne Feature Funktioner Filnavn Sidefod Menu Fulde navn IBAN Billede Billedgalleri Billede gitter Inkluderet Lær mere Menu Ny artikel Navn på ny download kategori Ny feature Nyt Projekt Ny udtalelse Nyt emne Næste billede Ingen projekter fundet Ingen projekter fundet i papirkurven Organisation Organisationer Siden blev ikke fundet Overordnet Egenskaber Forrige billede Projekt Projekter Læs mere Relaterede Søg Søg artikler Søg efter downloads Søgefunktioner Søg Projekter Søg udtalelser Søg emne Site indstillinger Telefon nr. Udtalelse Udtalelser Den side, du ledte efter, blev ikke fundet. Den kan være blevet fjernet, omdøbt, eller findes slet ikke. Emne Emner Opdater download kategori Opdater tema Videoafspiller Vis artikler Se downloads Vis projekter Vis alle referencer Var denne artikel nyttig? Hvad søger du ? År mere 