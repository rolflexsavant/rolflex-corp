<?php return [
    //'blogname' => 'Rolflex',
    'posts_per_page' => 12,
    'projects_per_page' => 12,
    'searchresults_per_page' => 10,
    'downloads_per_page' => 1000, // High number, we don't really use archive navigation here
    'rflex_gtm_id' => '',
];