<?php

$buttons = get_field('buttons');
$lead  = get_field('lead');

if( !is_array($buttons) ) {
    return;
}

$buttons    = array_column($buttons, 'btn');
$btns       = array_map(function($button) {
    $btn['label']   = is_null($button['btn_label']) ? $button['btn_link']->post_title : $button['btn_label'];

    //dd($button);

    if( $button['link_outside_website'] == true ) {
        $btn['link'] = $button['btn_link_ext'];
    } elseif( isset($button['btn_link']->ID) )  {
        $btn['link']    = get_post_permalink($button['btn_link']->ID);
    } elseif( is_string($button['btn_link']) && !empty($button['btn_link']) ) {
        $btn['link'] = $button['btn_link'];
    }

    $btn['classes'] = $button['btn_type'] == 'cta' ? ['btn btn-cta'] : ['btn'];

    if( $button['btn_color']  == 'secondary') {
        $btn['classes'][] = 'btn-sec';
    }

    if( $button['btn_color'] == 'primary' ) {
        $btn['classes'][] = 'btn-prim';
    }

    if( !is_null($button['icon_name']) && $button['icon_name'] != 'none') {
        $btn['icon'] = 'rflex-icon-append rflex-icon-'.$button['icon_name'];
    }

    $btn['classes'] = implode(' ', $btn['classes'] );

    return $btn;

}, $buttons);


yield_part('part-button-list', [
    'lead' => $lead,
    'btns' => $btns
]);