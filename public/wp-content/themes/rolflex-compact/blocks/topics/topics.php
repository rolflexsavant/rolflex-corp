<?php

$topics = get_field('topics');

if( !is_array($topics) ) {
    return;
}

$topics = array_column($topics,'topic');
$topics = filter_add_term_fields($topics);

yield_part('articles-overview-grid', [
    'topics' => $topics
]);