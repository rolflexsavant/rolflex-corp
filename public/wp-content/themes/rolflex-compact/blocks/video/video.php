<?php
/**
 * Initialize the block
 */
$fields             = get_fields();
$fields['platform'] = 'youtube'; // Default platform for now.
?>
<div class="rflex-block rflex-block-video text-center">
    <?php yield_part('part-video-embed', [
        'platform' => $fields['platform'],
        'url' => $fields['source'],
        'title' => $fields['title'],
        'text' => $fields['text']
    ]); ?>
</div>