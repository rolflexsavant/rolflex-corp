<?php

$grid = get_fields();

// Set defaults
$grid['images'] = array_default($grid['images']);
$grid['limit']  = array_get($grid, 'limit', 4 );



?>
<div class="rflex-block rflex-block-image-grid">
    <?php yield_part('part-image-grid', $grid ); ?>
</div>
