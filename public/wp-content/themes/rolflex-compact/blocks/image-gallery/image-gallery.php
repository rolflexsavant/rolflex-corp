<?php

$args = get_field('gallery');
$args['gallery'] = $args;

?>
<div class="rflex-block rflex-block-gallery">
<?php yield_part('part-gallery', $args ); ?>
</div>