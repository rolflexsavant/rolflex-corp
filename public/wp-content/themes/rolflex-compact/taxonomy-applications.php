
<?php
/**
 * If the args are empty, this is the point of entry, otherwise the archive is
 * loaded through "home.php".
 */
if( !have_posts() ) { // If taxonomy is empty, redirect to projects
    rflex_redirect(get_post_type_archive_link('projects'));
}

$term = get_queried_object();
$fields = get_fields($term);

$args['content']        = term_description($term->ID);
$args['hide_hero']      = !isset($fields['hide_hero']) ? false : $fields['hide_hero'];
$args['hide_content']   = !isset($fields['hide_content']) ? false : $fields['hide_content'];
$args['is_wide']        = !isset($fields['wide_content']) ? true : $fields['wide_content'];
$args['title']          = get_the_term_title();

if( !$args['hide_hero'] ) {
    $args['hero'] = get_hero_fields($fields['hero']);
}

yield_header(); ?>
<section <?php post_class('rflex-content'); ?>>
    <?php yield_part('section-content', $args ); ?>
    <?php yield_archive( 'projects', [
        'title' => __('Projects', 'rctd'),
        'shade' => true
    ]) ?>
    <?php load_sections($fields['sections']); ?>
</section>
<?php yield_footer(); ?>