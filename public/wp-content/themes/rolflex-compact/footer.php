
</main>

<footer class="rflex-footer">
    <?php if( !empty(get_info('footer.buttons')) ): ?>
    <div class="rflex-footer-pre">
        <div class="container-lg text-center">
            <div class="row">
                <div class="col-12 rflex-footer-pre-links">
                    <h3 class="h2"><?php info('footer.headline', __('Do you need help getting a question answered?', 'rctd') ) ?></h3>
                    <ul class="rflex-footer-pre-link-list">
                        <?php foreach( get_info('footer.buttons') as $button ): ?>
                        <li><a class="btn btn-cta btn-out" href="<?= $button['link'] ?>"><span class="btn-label rflex-icon-append rflex-icon-arrow-right"><?= $button['label'] ?></span></a></li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <?php yield_part('part-polygon' , ['color' => 'primary', 'position' => 'bottom-left'] ); ?>
    <?php endif; ?>
    <div class="rflex-footer-body">
        <div class="container-lg rflex-footer-logo">
            <div class="row justify-content-end">
                <div class="col-6 col-sm-4 col-md-3 col-lg-2">
                    <?php yield_part('logo') ?>
                </div>
            </div>
        </div>
        <?php if ( has_nav_menu( 'footer' ) ) : $footer_menu_options = rflex_footer_menu_options() ?>
            <?php wp_nav_menu([
                // Custom walker properties
                'add_top_li_classes' => 'col-12 col-sm-6 '.$footer_menu_options['col_class'],
                'href_hashtag_add_li_classes' => 'rflex-hashtag-class',
                'href_hashtag_remove' => true,
                'href_hashtag_classes' => 'rflex-submenu-title',
                // Default walker properties
                'depth'           => 2,
                'items_wrap'      => '<ul id="%1$s" class="rflex-nav-extend rflex-footer-nav-menu row %2$s justify-content-'.$footer_menu_options['align'].'">%3$s</ul>',
                'theme_location'  => 'footer',
                'container'       => 'nav',
                'container_class' => 'container-lg',
                'container_id'    => '',
                'menu_class'      => 'menu',
                'menu_id'         => '',
                'echo'            => true,
            ]); ?>
        <?php endif; ?>
    </div>
    <div class="rflex-footer-bottom">
        <div class="container-lg">
            <hr class="hr d-none d-sm-block">
            <div class="row">
                <div class="col-12 col-md-8 col-lg-8">
                    <?php yield_part('footer-text'); ?>
                </div>
                <div class="rflex-footer-social col-12 col-md-4 col-lg-4 text-md-right">
                    <?php yield_part('social-list'); ?>
                </div>
            </div>
        </div>
    </div>
</footer>
</div> <!-- close .rflex-layout -->
<?php
/**
 * The search console shows up when the search menu item is clicked.
 * We place it as low as possible in the HTML document. Sometimes
 * search engines don't scan a full document. We don't want things
 * like a mobile menu or search console to be scanned at the
 * cost of content.
 */
yield_part('part-search-console')
?>
<div class="rflex-mob-nav-container d-lg-none">
    <div class="rflex-mob-nav-bg"></div>
    <?php if ( has_nav_menu( 'main-mobile' ) ) : ?>
        <?php wp_nav_menu([
            // Custom walker properties
            'href_hashtag_add_li_classes' => 'rflex-hashtag-class',
            'href_hashtag_remove' => true,
            'href_hashtag_classes' => 'rflex-submenu-title',
            // Default walker properties
            'depth'             => 2,
            'items_wrap'        => '<ul id="%1$s" class="rflex-nav-extend %2$s">%3$s</ul>',
            'theme_location'    => 'main-mobile',
            'include_language'  => true,
            'container'         => 'nav',
            'container_class'   => 'rflex-mob-nav',
            'container_id'      => '',
            'menu_class'        => 'menu',
            'menu_id'           => '',
            'echo'              => true,
        ]); ?>
    <?php endif; ?>
</div>
<?php yield_part('part-desktop-navbar', ['id' => 'rflex-nav-bar'] ); ?>
<?php wp_footer(); ?>
</body>
</html>