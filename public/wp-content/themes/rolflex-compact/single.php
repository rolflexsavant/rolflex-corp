<?php

global $post;

$args = [];
$fields = get_fields();
$post_classes = [
    'rflex-gutenberg',
    'rflex-content',
    'rflex-post',
];


$hero = get_hero_fields($fields['hero']);

?>
<?php yield_header(); ?>
<article <?php post_class($post_classes); ?>>
    <?php yield_part('section-content', [
        'hero_inline' => $hero,
        'superscript' => rflex_terms_list(get_the_category()),
        'is_wide' => false,
    ] ) ?>
</article>
<?php yield_footer(); ?>