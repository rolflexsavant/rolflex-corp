<?php

global $post;

$args = [];
$fields = get_fields();
$post_classes = [
    'rflex-gutenberg',
    'rflex-content',
    'rflex-post',
];

if( get_post_type() == 'articles' ) {
    $topics         = get_the_terms($post->ID, 'topics');
    $topics_list    = $topics ? array_column($topics, 'name') : [];
    $topic_ids      = $topics ? array_column($topics, 'term_id') : [];

    $post_classes[] = 'rflex-article';
    $args['superscript'] = implode(', ', $topics_list);

    $related_articles = [];

    $query  = new WP_Query([
        'post_type' => 'articles',
        'posts_per_page' => 10,
        'post__not_in' => [get_the_ID()],
        'tax_query' => [
            [
                'taxonomy' => 'topics',
                'field' => 'term_id',
                'terms' => $topic_ids,
                'operator' => 'IN',
                'include_children' => true,
            ]
        ]
    ]);

    if( $query->have_posts() ) {
        while( $query->have_posts() ) { $query->the_post();
            $related_articles[] = [
                'title' => get_the_title(),
                'link' => get_the_permalink()
            ];
        } wp_reset_postdata();
    }


    $args['aside'] = [
        'tile-posts-list' => [
            'title'         => __('Related', 'rctd'),
            'icon'          => 'related',
            'posts'      => $related_articles
        ]
    ];
    $args['footer'] = [
        'helpful' => []
    ];
}

?>
<?php yield_header(); ?>
<article <?php post_class($post_classes); ?>>
    <?php yield_part('section-content', $args ) ?>
</article>
<?php yield_footer(); ?>