<?php
/**
 * This file and its contents probably isn't used.
 */

$post = get_post();
$download = get_download_fields( get_post() );

set_download_headers($download);

if( $download['open_in_window'] ) {
   return;
}

$content = '<div class="alert alert-warning rflex-m-top"><div class="spinner-border spinner-border-sm" role="status"></div>&nbsp;'.__('De download begint over enkele seconden...', 'rctd').'</div>';
$content = get_the_excerpt().$content;

?>
<?php yield_header(); ?>
<section <?php post_class('rflex-content') ?>>
    <?php yield_part('section-content', [
        //'superscript' => 'Superscript',
        'is_wide' => false,
        'content' => $content,
        'aside' => [
            'aside-meta' => [
                'meta' => [
                    [
                        'label' => __('Filename', 'rctd'),
                        'value' => $download['file']['filename'],
                    ],[
                        'label' => __('Filesize', 'rctd'),
                        'value' => $download['file']['download_size']
                    ]
                ]
            ]
        ],
    ] ) ?>
</section>

<?php yield_footer(); ?>