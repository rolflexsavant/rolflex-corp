<?php yield_header(); ?>
<section <?php post_class('rflex-content'); ?>>
    <?php yield_part('section-content', ['is_wide' => false ]); ?>
</section>
<?php yield_footer(); ?>