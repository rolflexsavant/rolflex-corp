<?php

$args = isset($args) ? $args : [];
//$args = array_merge($args, get_fields_project($post, true ) );

?>
<?php yield_header(); ?>
<section <?php post_class('rflex-content rflex-post rflex-project'); ?>>
    <?php yield_part('section-content', [
        'hero' => $args['hero'],
        'description' => $args['description'],
        // 'aside' => [
        //     'project-meta' => [
        //         'logo' => $args['logo'],
        //         'meta' => $args['meta']
        //     ],
        // ]
    ]) ?>

</section>
<?php yield_footer(); ?>
