<?php
/**
 * If this is found at a later stage because of a forgotten clean-up. It has been just experimental. It can be removed
 */
$test_urls = [
    'http://rflex-corp-nl.test/kennis-centrum/full-light-panelen/',
    'http://rflex-corp-nl.test/projecten/bmw-test-project/',
    'http://rflex-corp-nl.test/contact/',
];

$results = [];

foreach( $test_urls as $k => $url ) {
    $id = url_to_postid($url);
    $p = get_post($id);

    $results[] = [
        'url' => $url,
        'id' => $id,
        'slug' => $p->slug,
        'title' => $p->post_title
    ];
}

//d($results);

/************************************************
 * ERROR 404 Page
 */
?>
<?php get_header(); ?>
        <section class="rflex-main-content">
            <div class="rflex-section">
                <div class="container-lg">
                    <div class="row justify-content-center">
                        <div class="col-12 col-sm-10 col-md-8 col-lg-6 text-center">
                            <p class="rflex-title-super">Error 404</p>
                            <h1 class="h1"><?php _e('Page not found') ?></h1>
                            <p><?php _e( 'The page you were looking for could not be found. It might have been removed, renamed, or did not exist in the first place.' ); ?></p>
                        </div>
                    </div>
                </div>
            </div>
            <?php for($i = 0; $i < 10; $i++ ): ?>
                <div class="rflex-section">
                <div class="container-lg">
                    <div class="row justify-content-center">
                        <div class="col-12 col-sm-10 col-md-8 col-lg-6 text-center">
                            <p class="rflex-title-super">Error 404</p>
                            <h1 class="h1"><?php _e('Page not found') ?></h1>
                            <p><?php _e( 'The page you were looking for could not be found. It might have been removed, renamed, or did not exist in the first place.' ); ?></p>
                        </div>
                    </div>
                </div>
            </div>
            <?php endfor; ?>


            <?php if( constant('RFLEX_ENV') == 'development' ): ?>
                <?php yield_part('part-search-form'); ?>
                <div class="rflex-section">
                <div class="container-lg">
                    <div class="row justify-content-center">
                        <div class="col-12">
                            <div id="react-component">

                            </div>
                        </div>
                    </div>
                </div>
                <?php yield_part('section-compact-door') ?>
            </div>



            </div>
            <?php endif; ?>
        </section>

<?php get_footer(); ?>