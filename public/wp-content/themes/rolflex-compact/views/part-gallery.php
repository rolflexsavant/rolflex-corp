<?php
    if( !empty($args['gallery']['title']) ) {
        $args['title'] = $args['gallery']['title'];
    }
    if( !empty($args['gallery']['description']) ) {
        $args['description'] = $args['gallery']['description'];
    }
?>
<div class="row">
    <?php if( !empty($args['gallery']['images']) ): $gallery = $args['gallery']; $first_image = array_shift($gallery['images']); ?>
    <div class="col-12 rflex-image-gallery">
        <?php if( !empty($args['superscript']) ): ?>
        <p class="rflex-title-super"><?= $args['superscript'] ?></p>
        <?php endif; ?>

        <?php if( !empty($args['title']) ): ?>
        <h3 class="h3"><?= $args['title'] ?></h3>
        <?php endif; ?>

        <?php if( !empty($args['description']) ): ?>
        <?= $args['description']; ?>
        <?php endif; ?>

        <div class="row rflex-image-gallery-images justify-content-center">
            <div class="col-12 col-md-8 text-center rflex-gallery-list-item">
                <a href="" class="rflex-gallery-img-container rflex-img-aspect">
                    <img class="rflex-gallery-img rflex-img rflex-img-center rflex-img-cover" title="<?= $first_image['image']['title'] ?>" alt="<?= $first_image['image']['alt'] ?>" src="<?= $first_image['image']['sizes']['gallery-highlight'] ?>">
                </a>
            </div>
            <?php if( count($gallery['images']) > 0 ): ?>
            <div class="col-12 col-md-3">
                <ul class="rflex-gallery-list row">
                <?php $i = 0; $limit = 3; foreach($gallery['images'] as $image): if(++$i > $limit) { break; }?>

                    <li class="rflex-gallery-list-item col-4 col-md-12">
                        <a href="#" class="rflex-gallery-img-container rflex-img-aspect">
                            <?php if( $i == $limit && count($gallery['images']) > $limit ): $more_count = count($gallery['images']) - $limit; ?>
                            <span class="rflex-gallery-list-item-label"><?= $more_count; ?> <?php _e('more', 'rctd') ?></span>
                            <?php endif; ?>
                            <img class="rflex-gallery-img rflex-img rflex-img-center rflex-img-cover" title="<?= $image['image']['title'] ?>" alt="<?= $image['image']['alt'] ?>" src="<?= $image['image']['sizes']['gallery-thumb'] ?>">
                        </a>
                    </li>
                <?php endforeach; ?>
                </ul>
            </div>
            <?php endif; ?>
        </div>

        <div class="rflex-modal rflex-gallery-modal justify-content-center">
            <div class="rflex-modal-top">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-8 text-left">
                                <h4 class="h4"><?= $args['title'] ?></h4>
                            </div>
                            <div class="col-4 text-right">
                                <a href="#" class="rflex-gallery-controls-close rflex-link-highlight"><i class="rflex-icon-prepend rflex-icon-close"></i><?php _e('Close gallery', 'rctd') ?></a>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="rflex-modal-content container-fluid ">
                <div class="row justify-content-center d-flex align-items-center">
                    <?php $c = 0; foreach( $args['gallery']['images'] as $image ): $total = count($args['gallery']['images']); ++$c; ?>

                    <div class="rflex-gallery-modal-img col-10 col-md-10 col-lg-9 col-xl-8 align-self-center is-toggled">
                        <div class="rflex-gallery-img-container">
                            <img id="rflex-image-<?= $image['image']['ID'] ?>" class="rflex-gallery-img" title="<?= $image['image']['title'] ?>" alt="<?= $image['image']['alt'] ?>" src="<?= $image['image']['sizes']['gallery-full'] ?>">
                        </div>
                        <div class="rflex-gallery-img-meta">
                            <small class="text-color-light"><?php _e('Image', 'rctd') ?> <?= $c ?>/<?= $total ?></small>
                        </div>
                        <?php if( $image['show_caption'] || !str_is_empty($image['image']['caption']) ): ?>
                        <div class="rlfex-gallery-img-caption">
                            <?= $image['image']['caption']; ?>
                        </div>
                        <?php endif; ?>
                    </div>

                    <?php endforeach; ?>
                </div>
                <?php if( count($args['gallery']['images']) > 1 ): ?>
                <div class="rflex-modal-bottom rflex-gallery-controls">
                    <div class="row justify-content-center">
                        <ul class="col-10 text-center rflex-list-inline">
                            <li class="rflex-gallery-controls-prev"><a href="#" class="btn btn-small rflex-icon-prepend rflex-icon-arrow-left"><span class="btn-label"><?php _e('Previous image', 'rctd') ?></span></a></li>
                            <li class="rflex-gallery-controls-next"><a href="#" class="btn btn-small rflex-icon-append rflex-icon-arrow-right"><span class="btn-label"><?php _e('Next image', 'rctd') ?></span></a></li>
                        </ul>
                    </div>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <?php else: // If images is empty ?>
    <div class="col-12">
        <p><?php _e('No images have been found in the gallery...', 'rctd'); ?></p>
    </div>
    <?php endif; ?>
</div>