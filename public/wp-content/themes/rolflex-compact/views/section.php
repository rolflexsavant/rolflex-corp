<section class="rflex-section text-left <?= $args['classes'] ?>">
    <div class="container-lg">
        <?php yield_part($part, $args) ?>
    </div>
</section>