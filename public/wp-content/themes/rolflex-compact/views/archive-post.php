<div class="rflex-section rflex-section-shaded rflex-section-overview rflex-section-blog-overview">
    <div class="container-lg">
        <?php if( have_posts() ): ?>
        <div class="row">
            <div class="col-12 text-center">
                <?php // Controls will go here ?>
            </div>
        </div>
        <div class="row justify-content-center">
            <?php while( have_posts() ) : the_post(); ?>
                <?php yield_part('tile-post-type-full', [
                    'hero' => get_hero_fields( get_field('hero') ),
                    'title' => get_the_title(),
                    'content' => get_the_excerpt(),
                    'link' => get_the_permalink(),
                    'superscript_icon' => 'calendar',
                    'col_limit' => 3,
                    'superscript' => sprintf( __('%1s in %2s', 'rctd') , get_the_date(), rflex_terms_list(get_the_category()) ),
                ] ); ?>
            <?php endwhile; ?>
        </div>
        <?php yield_part('part-archive-nav') ?>

        <?php endif; ?>
    </div>
</div>
