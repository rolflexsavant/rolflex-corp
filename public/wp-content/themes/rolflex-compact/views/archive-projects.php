<?php if( $args['shade'] ): ?>
<?php yield_part('part-polygon' , ['color' => 'shade', 'position' => 'top-right'] ); ?>
<?php endif; ?>
<div class="rflex-section <?= $args['shade'] ? 'rflex-section-shaded' : '' ?> rflex-section-overview">
    <div class="container-lg">
        <?php if( !empty($args['title']) || !empty($args['description']) ): ?>
        <div class="row">
                <div class="col-12 rflex-section-head text-center">
                    <?php if( !empty($args['title']) ): ?>
                    <h3 class="h3"><?= $args['title']  ?></h3>
                    <?php endif; ?>
                    <?php if(!empty($args['description'])): ?>
                        <?= $args['description'] ?>
                    <?php endif; ?>
                </div>
        </div>
        <?php endif ?>
        <div class="row d-none d-md-block rflex-m-bottom-bigger">
            <div class="col-auto">
                <h4><?= __('Checkout projects related to: ', 'rctd') ?></h4>
            </div>
            <div class="col-auto">
                <ul class="rflex-list-labels">
                    <?php foreach(get_all_applications() as $application): ?>
                    <li><a class="rflex-list-labels-item" href="<?= $application['link'] ?>"><?= $application['name'] ?></a></li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
        <div class="row justify-content-center">
            <?php while( have_posts() ) : the_post(); $project = get_fields_project(get_post() ); ?>
                <?php yield_part('tile-post-type-full', [
                    'hero' => $project['hero'],
                    'link' => $project['permalink'],
                    'title' => $project['title'],
                    'col_limit' => 2,
                    'content' => $project['description'],
                    'superscript' => sprintf('%1$s | %2$s', $project['country'], $project['sectors'] ),
                    'read_more' => __('View project'),
                ] ); ?>
            <?php endwhile; ?>
        </div>
        <?php yield_part('part-archive-nav') ?>
    </div>
</div>
<?php if( $args['shade'] ): ?>
<?php yield_part('part-polygon' , ['color' => 'shade', 'position' => 'bottom-left'] ); ?>
<?php endif; ?>


<?php //if( !is_tax() ): ?>
    <?php section_load_applications_overview(['shade' => !$args['shade'] ]) ?>
<?php //endif; ?>