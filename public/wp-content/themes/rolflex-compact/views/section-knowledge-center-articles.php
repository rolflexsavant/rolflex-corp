<?php $args = array_merge([
    'title' => __('Populair in our knowledge center'),
    'articles' => [],
    'classes' => '',
], $args );

?>
<section class="rflex-section text-left <?= $args['classes'] ?>">
    <div class="container-lg">
        <div class="row">
            <div class="col-12">
                <h3 class="h3"><?= $args['title'] ?></h3>
            </div>
        </div>
        <ul class="row rflex-row-list rflex-list-link-blocks">
            <?php foreach( $args['articles'] as $article ) : ?>
                <li class="col-12 col-md-6"><a class="rflex-icon-prepend rflex-icon-arrow-right" href="<?= $article['link'] ?>"><?= $article['title'] ?></a></li>
            <?php endforeach; ?>
        </ul>
    </div>
</section>