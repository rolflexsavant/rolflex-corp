<?php
$args = array_merge([
    'superscript' => __('Top quality','rctd'),
    'title' => __('Included features', 'rctd'),
], $args );

if( isset($args['features']) && count($args['features']) > 0 ):  ?>
    <section class="rflex-section">
        <div class="container-lg">
            <div class="row">
                <div class="col-12 rflex-section-head rflex-feature-overview text-center">
                    <p class="rflex-title-super"><?= $args['superscript'] ?></p>
                    <h3 class="h3"><?= $args['title'] ?></h3>
                </div>
            </div>
            <div class="row justify-content-center">
                <?php foreach($args['features'] as $feature ): ?>
                <?php yield_part('tile-post-type-lite', [
                    'title' => $feature['feature']['title'],
                    'link' => $feature['feature']['link'],
                    'description' => $feature['description'],
                    'icon' => $feature['feature']['icon'],
                    'read_more' => __('Checkout feature', 'rctd'),
                ]) ?>
                <?php endforeach; ?>
            </div>
        </div>
    </section>
    <?php endif; ?>