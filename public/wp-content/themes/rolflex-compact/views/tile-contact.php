<div class="rflex-tile rflex-tile-contact rflex-content-tile">
    <div class="rflex-inner">
        <i class="rflex-tile-contact-icon rflex-icon rflex-icon-<?= $args['icon'] ?>"></i>
        <h4 class="h4"><?= $args['name'] ?></h4>
        <p>
            <?= $args['description'] ?>
        </p>
        <?php if( !empty($args['phone_number']) || !empty($args['email']) ): ?>
        <ul class="rflex-list-clean">
            <?php if( !empty($args['phone_number']) ): ?>
            <li><a href="" class="rflex-link-highlight rflex-icon-prepend rflex-icon-phone"><?= $args['phone_number'] ?></a></li>
            <?php endif; ?>
            <?php if( !empty($args['email']) ): ?>
            <li><a href="" class="rflex-link-highlight rflex-icon-prepend rflex-icon-email"><?= $args['email'] ?></a></li>
            <?php endif; ?>
        </ul>
        <?php endif; ?>
    </div>
</div>