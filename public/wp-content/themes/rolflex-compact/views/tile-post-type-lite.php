<?php $args = array_merge([
    'icon' => false,
    'read_more' => __('Read more', 'rctd'),
    'col_limit' => 4,
], $args ); ?>
<div class="<?= classes_col_limit($args['col_limit']) ?> d-flex align-items-stretch">
    <div class="rflex-tile rflex-tile-lite">
        <div class="rflex-tile-bottom-link">
            <a href="<?= $args['link'] ?>" class="rflex-icon-prepend rflex-icon-arrow-right"><?= $args['read_more'] ?></a>
        </div>
        <div class="rflex-inner">
            <?php if($args['icon'] ): ?>
            <i class="rflex-icon rflex-icon-<?= $args['icon'] ?>"></i>
            <?php endif; ?>
            <h4 class="h4"><?= $args['title'] ?></h4>
            <?= $args['description']; ?>
        </div>
    </div>
</div>