<?php
if(  $args['override_globals'] != true ) {
    $args = array_merge([
        'pitch_text' => __('The door always comes with:', 'rctd' ),
        'superscript' => __('Features', 'rctd') ,
    ], $args );

    $args = array_merge( $args , get_info('selling_points') );
}
?>
<section class="rflex-section rflex-section-diapositive">
    <div class="container-lg">
        <div class="row">
            <div class="col-12 rflex-section-head rflex-feature-overview text-center">
                <p class="rflex-title-super"><?= $args['superscript'] ?></p>
                <h3 class="h3"><?= $args['pitch_text'] ?></h3>
            </div>
        </div>
        <div class="row justify-content-center">
            <?php foreach($args['items'] as $point): ?>
                <?php yield_part('tile-selling-point', [
                    'icon' => $point['icon'],
                    'title' => $point['label'],
                    'link' => $point['link'],
                ]); ?>
            <?php endforeach; ?>
        </div>
    </div>
</section>