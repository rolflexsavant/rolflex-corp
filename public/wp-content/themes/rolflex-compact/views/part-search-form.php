<?php $args = array_merge([
    'label' => sprintf( __('Search for: %1s', 'rctd') , ''),
    'button' => __('Search', 'rctd' ),
    'placeholder' => __('Type your search query' , 'rctd' ),
    'search_value' => get_search_query(),
    'post_type' => '',
], $args ); ?>
<div class="rflex-search-form row">
    <div class="col-12">
            <form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
                <div class="form-group">
                    <label class="d-none" for="s">
                        <?= $args['label'] ?>
                    </label>
                    <div class="rflex-search-control">
                            <input type="search" class="rflex-search-control-input" placeholder="<?= $args['placeholder'] ?>" value="<?= $args['search_value'] ?>" name="s" />
                            <?php if( !empty($args['post_type'])  ) :?>
                                <input type="hidden" name="post_type" value="<?= $args['post_type'] ?>" />
                            <?php endif; ?>

                            <button type="submit" class="btn btn-prim rflex-search-control-btn"><i class="rflex-icon rflex-icon-search"></i><span class="btn-label">&nbsp;<?= $args['button'] ?></span></button>
                    </div>
                </div>
            </form>
    </div>
</div>
