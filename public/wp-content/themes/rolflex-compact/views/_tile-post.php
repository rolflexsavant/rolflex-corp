<?php
    $args = array_merge([
        'superscript_icon' => false,
        'superscript' => false,
        'title_icon' => false,
        'col_max' => 3,
        'read_more_label' => __('Read more', 'rctd'),
    ], $args);

?>


<div class="col-12 col-md-6 col-lg-4 col-xl-4 d-flex align-items-stretch text-left rflex-clickable">
    <div class="rflex-tile rflex-content-tile">
        <?php if( isset($args['hero']['image_thumb']) ): ?>
        <div class="rflex-tile-head rflex-tile-head-img rflex-img-aspect">
            <img class="rflex-img rflex-img-center rflex-img-cover" title="<?= $args['hero']['title'] ?>" alt="<?= $args['hero']['alt'] ?>" src="<?= $args['hero']['image_thumb'] ?>">
        </div>
        <?php endif; ?>
        <div class="rflex-tile-bottom-link">
            <a href="<?= $args['link'] ?>" class="rflex-icon-prepend rflex-icon-arrow-right"><?php _e('Read more', 'rctd') ?></a>
        </div>
        <div class="rflex-inner">
            <?php if( $args['superscript'] ): ?>
            <div class="rflex-tile-superscript">
                <?php if( $args['superscript_icon'] ): ?>
                <i class="rflex-icon-prepend rflex-icon-<?= $args['superscript_icon'] ?> rflex-icon-color-primary"></i>
                <?php endif; ?>
                <small class="text-color-light"><?= $args['date'] ?></small>
            </div>
            <?php endif; ?>
            <?php if( is_home() || is_archive() ): ?>
            <h2 class="h4 h-upper"><?= $args['title'] ?></h2>
            <?php else: ?>
            <h3 class="h4 h-upper"><?= $args['title'] ?></h3>
            <?php endif; ?>
            <div class="rflex-tile-content">
                <?= $args['excerpt'] ?>
            </div>
        </div>
    </div>
</div>