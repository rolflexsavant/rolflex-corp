<?php
/**
 * If no is_wide argument is specified we let it depend on if the aside argument is empty true or false.
 * A wide content section has no sidebar and takes the whole width of the page. You can choose a non-wide
 * page layout by specifying the is_wide = false (even when there is no aside ).
 */
$args['is_wide'] = isset($args['is_wide']) ? $args['is_wide'] : empty($args['aside']);

if( !isset($args['hide_breadcumbs']) ) {
    $args['hide_breadcrumbs'] = false;
}

// Include the hereo if present.
if(isset($args['hero']) ):
    yield_part('section-hero', [
        'title' => empty($args['title']) ? get_the_title() : $args['title'],
       // 'description' => $args['description'], @deprecated... should not return and caused confusion. If it causes bugs that this is commented, fix it...
        'hero' => $args['hero']
    ]);
endif; ?>
<?php if( empty($args['hide_content']) || $args['hide_content'] == false  ): ?>
<div class="rflex-section rflex-section-content">
    <div class="container-lg">
        <div class="row <?= $args['is_wide'] ? 'justify-content-between' : 'justify-content-center'; ?>">
            <div class="col-12 <?= $args['is_wide'] ? 'col-lg-12' : 'col-lg-7'; ?>">
                <?php if( !did_yield_breadcrumbs() ): ?>
                    <div class="rflex-content-breadcrumbs">
                        <?php yield_breadcrumbs() ?>
                    </div>
                <?php endif; ?>
                <?php if(!isset($args['hero']) ): ?>
                <header class="rlfex-content-header rflex-post-header">
                    <?php if( !empty($args['superscript']) ): ?>
                        <p class="rflex-title-super"><?= $args['superscript'] ?></p>
                    <?php endif; ?>
                    <h1 class="h1"><?php the_title(); ?></h1>

                    <?php if( !empty($args['hero_inline']) ): ?>
                    <div class="rflex-hero-inline">
                        <img src="<?= $args['hero_inline']['image_inline'] ?>" alt="<?= $args['hero_inline']['alt'] ?>" title="<?= $args['hero_inline']['title'] ?>" />
                    </div>
                    <?php endif; ?>
                </header>
                <?php endif; ?>
                <div class="rflex-content-body rflex-post-body">
                    <?php
                    /**
                     * Needs to be this ugly, gutenberg blocks from the content are only processed with the_content();
                     */
                    ?>
                    <?php if( !empty($args['content']) ) { echo $args['content']; } else { the_content(); } ?>
                </div>
                <?php if( !empty($args['footer']) || is_singular(['articles', 'post']) ) : ?>
                <footer class="rflex-content-footer rflex-post-footer">
                    <?php if( is_singular(['articles', 'post']) ): ?>
                        <?php yield_part('part-post-controls'); ?>
                    <?php endif; ?>
                    <?php if( !empty($args['footer']) ): ?>
                        <?php foreach($args['footer'] as $template => $data ): ?>
                            <?php yield_content_footer($template, $data ) ?>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </footer>
                <?php endif; ?>
            </div>

            <?php if( isset($args['aside']) && count($args['aside']) > 0 ): ?>
            <aside class="col-12 col-lg-4 rflex-p-top-big">
                <?php foreach($args['aside'] as $template => $data ): ?>
                    <?php yield_part( $template, $data ) ?>
                <?php endforeach; ?>
            </aside>
            <?php endif; ?>

        </div>
    </div>
</div>
<?php endif; ?>