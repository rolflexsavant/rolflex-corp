<?php
/**
 * Not the prettiest solution. This way we ditch the standard WP query for the downloads
 * archive and perform a new one for each term. The solution for now because of the
 * lack for a better solution that doesn't modify Wordpress' standard behaviour
 * too much. With the caching plugin the code below doesn't need to be executed on
 * each request anyway.
 */

$ordered_by_term = [];
$terms_list = get_terms([
    'taxonomy' => 'download-types',
    'hide_empty' => true
]);

foreach($terms_list as $term) {
    wp_reset_query();

    if( !isset($ordered_by_term[$term->term_id]) ) {
        $ordered_by_term[$term->term_id] = [
            'term' => $term,
            'posts' => [],
        ];
    }

    $loop = new WP_Query([
        'post_type' => 'downloads',
        'posts_per_page' => get_option('downloads_per_page'),
        'tax_query' => [
            [
                'taxonomy' => 'download-types',
                'field' => 'slug',
                'terms' => $term->slug,
            ],
        ]
    ]);

    while($loop->have_posts()) { $loop->the_post();
        $download = get_download_fields( get_post() );
        $ordered_by_term[$term->term_id]['posts'][] = $download;
    }
}
?>
<?php foreach($ordered_by_term as $term): ?>
<div class="rflex-section rflex-section-odd-even">

    <div class="container-lg">
        <div class="row justify-content-center">
            <div class="col-12">
                <h3 class="rflex-icon-prepend rflex-icon-<?= $term['term']->icon ?>"><a name="<?= $term['term']->slug ?>"><?= $term['term']->name ?></a></h3>
            </div>
        </div>
        <div class="row justify-content-left">
            <?php foreach($term['posts'] as $download): ?>
                <?php yield_part('tile-post-type-full', [
                   // 'ID' => get_the_ID(),
                    'link' => $download['link'],
                    'title' => $download['title'],
                    'col_limit' => 3,
                    'content' => $download['content'],
                    'terms' => $download['terms'],
                    'new_window' => true,
                    'read_more' => __('Download'),
                ] ); ?>
            <?php endforeach; ?>

            <?php yield_part('part-archive-nav') ?>
        </div>
    </div>

</div>
<?php endforeach; ?>