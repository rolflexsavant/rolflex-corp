<div class="rflex-helpful">
    <p><?php _e('Was this article helpful?', 'rctd') ?></p>
    <ul class="rflex-list-inline">
        <li><a href="#"><i class="rflex-icon rflex-icon-thumbs-up"></i><span class="d-none">Yes</span></a></li>
        <li><a href="#"><i class="rflex-icon rflex-icon-thumbs-down"></i><span class="d-none">No</span></a></li>
    </ul>
</div>