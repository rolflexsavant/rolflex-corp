<?php $args = array_merge([
    'classes' => ''
], $args ); ?>
<div class="rflex-organization-visit <?= $args['classes'] ?>">
    <h4 class="h4"><?php _e('Visit us', 'rctd'); ?></h4>
    <p>
        <?php info('visit_us') ?>
    </p>
    <div class="rflex-p-top">
        <?php yield_part('button', [
            'link' => '#',
            'classes' => 'btn btn-cta btn-prim',
            'label' => __('Plan a visit', 'rctd' )
        ]); ?>
    </div>
</div>