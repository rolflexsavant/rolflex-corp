<?php
$suggestions = get_info('search.suggestions', []);
$suggestions_headline = get_info('search.suggestions_has_headline', false) ? get_info('search.suggestions_headline') : __('Most searched for:', 'rctd');
?>
<div id="rflex-search-console" class="rflex-search-console-wrapper">
<div class="rflex-search-console-overlay rflex-toggle-search"></div>
<div class="rflex-search-console">
    <div class="container-lg">
        <div class="row justify-content-center">
            <div class="col-12 rflex-search-console-content">
                <a class="rflex-toggle-search rflex-search-console-close d-none d-md-inline-block"  href="#"><i class="rflex-icon rflex-icon-close"></i>&nbsp;<?= __('Close search', 'rctd') ?></a>
                <h3 class="h3"><?= __('What are you looking for?','rctd') ?></h3>

                <?php yield_part('part-search-form') ?>

                <?php if( !empty($suggestions) ): ?>
                    <h4><?= $suggestions_headline ?></h4>

                    <ul class="rflex-search-list">
                        <?php foreach($suggestions as $suggestion): ?>
                        <li><a href="<?= $suggestion['link'] ?>" class="rflex-icon-prepend rflex-icon-arrow-right"><?= $suggestion['label'] ?></a></li>
                        <?php endforeach; ?>
                    </ul>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
</div>