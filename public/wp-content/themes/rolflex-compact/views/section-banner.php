<?php
    $args['section_class'] = empty($args['section_class']) ? '' : $args['section_class'];
?>
<div class="rflex-section <?= $args['section_class'] ?>">
    <div class="container-lg">
        <div class="row justify-content-center">
            <div class="col-9 rflex-banner rflex-banner-door rflex-clickable text-left">
                <div class="row">
                    <div class="col-12 col-md-8 col-lg-7">
                        <div class="rflex-banner-inner">
                            <h3 class="h2 rflex-banner-title"><?php info('banner.title') ?></h3>
                            <p class="rflex-banner-text"><?php info('banner.text') ?></p>
                            <?php yield_part('part-button', [
                                'classes' => 'btn btn-sec btn-cta rflex-banner-btn',
                                'label' => get_info('banner.button_label'),
                                'link' => get_info('banner.link'),
                                'icon' => 'rflex-icon-append rflex-icon-arrow-right'
                            ]);?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
