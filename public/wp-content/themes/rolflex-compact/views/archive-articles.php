<?php $args = get_articles_archive_data(); ?>

<div class="rflex-section">
    <div class="container-lg">
        <div class="row justify-content-center">
            <?php foreach($args['top'] as $topic): ?>
            <div class="col-12 col-sm-6 col-lg-4 d-flex align-items-stretch">
                <?php yield_part('tile-posts-list', [
                    'icon'  => $topic['icon'],
                    'title' => $topic['title'],
                    'posts' => $topic['articles'],
                ]); ?>
            </div>
            <?php endforeach; ?>
        <!-- </div>
    </div>
</div>



<div class="rflex-section">
    <div class="container-lg">
        <div class="row justify-content-center"> -->
            <?php foreach($args['bottom'] as $topic): ?>
            <div class="col-12 col-sm-6 col-lg-4 d-flex align-items-stretch">
                <?php yield_part('tile-posts-list', [
                    'icon'          => $topic['icon'],
                    'title'         => $topic['title'],
                    'posts'      => $topic['articles'],
                ]); ?>
            </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>


<?php yield_part('section-banner' , [
    'section_class' => 'rflex-section-narrow'
]) ;?>