<?php
$style_classes = '';

if( $args['hero']['style'] == 'overlay_light' ) {
    $style_classes = ' rflex-overlay-light';
}
if( $args['hero']['style'] == 'overlay_dark' ) {
    $style_classes = ' rflex-overlay-dark';
}
    did_yield_breadcrumbs(true);
?>
<header id="rflex-hero-container" class="rflex-content-header rflex-hero<?= $style_classes ?> rflex-hero-frontpage"  style="background-image: url('<?= $args['hero']['image_large'] ?>');">
    <div class="container-lg">
        <div class="row rflex-hero-body justify-content-center">
            <div class="col-12 col-md-7 col-lg-6">
                <?php if( !empty($args['super']) ): ?>
                <p class="rflex-title-super"><?= $args['super'] ?></p>
                <?php endif; ?>
                <h1 class="h1"><?= $args['title'] ?></h1>
                <?= $args['hero']['text'] ?>

                <div class="rflex-hero-cta">
                    <?php yield_part('part-button', [
                        'link' => get_info('special_pages.design_your_door', ''),
                        'classes' => 'btn btn-cta btn-prim',
                        'icon' => 'rflex-icon-append rflex-icon-design-door',
                        'label' => __('Design your own door', 'rctd')
                    ]) ?>
                </div>

            </div>
            <div class="d-none d-lg-block col-lg-6">
                <div id="rflex-animation-container">
                    <img class="rflex-animation-placeholder" src="<?= get_stylesheet_directory_uri() ?>/assets/img/animation/0001.png" alt="Rolflex Compact Door" />
                </div>
            </div>
            <div class="d-none d-md-block d-lg-none col-md-5">
                <img class="rflex-animation-placeholder" src="<?= get_stylesheet_directory_uri() ?>/assets/img/animation/0036.png" alt="Rolflex Compact Door" />
            </div>
        </div>
    </div>
</header>
<?php yield_part('part-polygon' , ['color' => 'primary', 'position' => 'bottom-left'] ); ?>
