<?php
$args = array_merge([
    'superscript_icon' => false,
    'superscript' => false,
    'title_icon' => false,
    'col_limit' => 3,
    'terms' => [],
    'read_more' => __('Read more', 'rctd'),
    'new_window' => false,
], $args);

if( !empty($args['terms']) ) {
    $args['terms'] = get_terms_fields($args['terms']);
}
?>
<div class="<?= classes_col_limit($args['col_limit']) ?> d-flex align-items-stretch text-left rflex-clickable">
    <div class="rflex-tile rflex-content-tile">
        <?php if( isset($args['hero']['image_thumb']) ): ?>
        <div class="rflex-tile-head rflex-tile-head-img rflex-img-aspect">
            <img class="rflex-img rflex-img-center rflex-img-cover" title="<?= $args['hero']['title'] ?>" alt="<?= $args['hero']['alt'] ?>" src="<?= $args['hero']['image_thumb'] ?>">
        </div>
        <?php endif; ?>
        <?php if( !empty($args['link']) ): ?>
        <div class="rflex-tile-bottom-link">
            <a href="<?= $args['link'] ?>" <?= $args['new_window'] ? 'target="_blank" ' : '' ?>class="rflex-icon-prepend rflex-icon-arrow-right"><?= $args['read_more'] ?></a>
        </div>
        <?php endif; ?>
        <div class="rflex-inner">
            <?php if( $args['superscript'] ): ?>
            <div class="rflex-tile-superscript">
                <?php if( $args['superscript_icon'] ): ?>
                <i class="rflex-icon-prepend rflex-icon-<?= $args['superscript_icon'] ?> rflex-icon-color-primary"></i>
                <?php endif; ?>
                <small class="text-color-light"><?= $args['superscript'] ?></small>
            </div>
            <?php endif; ?>
            <?php if( !empty($args['terms']) ): $taxonomy = $args['terms'][0]['taxonomy']; ?>
            <ul class="rflex-terms-list rflex-list-inline">
                <?php foreach($args['terms'] as $term ): ?>
                    <li class="rflex-terms-list-term rflex-terms-list-item-<?= $term['taxonomy'] ?> rflex-icon-prepend rflex-icon-<?=$term['icon'] ?>"><?= $term['name'] ?></li>
                <?php endforeach; ?>
            </ul>
            <?php endif; ?>
            <?php if( is_home() || is_archive() ): ?>
            <h2 class="h4 h-upper"><?= $args['title'] ?></h2>
            <?php else: ?>
            <h3 class="h4 h-upper"><?= $args['title'] ?></h3>
            <?php endif; ?>
            <div class="rflex-tile-content">
                <?= $args['content'] ?>
            </div>
        </div>
    </div>
</div>