<?php
$args['selected'] = rflex_current_site_language();

$args['options'] = rflex_get_language_options();
?>
<div class="rflex-dropdown-simple rflex-toggle">
    <a class="rflex-anchor-prevent-default rflex-dropdown-button">
        <span class="rflex-icon-prepend rflex-icon-globe">
            <?php // _e('Language select:', 'rctd'); ?>
            <?= $args['selected']['label'] ?>
        </span>&nbsp;
        <i class="rflex-icon"></i>
    </a>
    <ul class="rflex-list-group rflex-dropdown-simple-options text-left submenu">
        <?php foreach($args['options'] as $language ) : ?>
            <?php if( $language['code'] != $args['selected']['code'] ): ?>
            <li class="rflex-dropdown-options-option">
                <a href="<?= $language['url'] ?>"><?= $language['label'] ?></a>
            </li>
            <?php endif; ?>
        <?php endforeach; ?>
    </ul>
</div>