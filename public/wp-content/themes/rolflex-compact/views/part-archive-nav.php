<?php if( rflex_is_paged() ): ?>
    <div class="row">
        <div class="col-12 text-center">
            <?php yield_part('part-numeric-nav') ?>
        </div>
    </div>
<?php endif; ?>