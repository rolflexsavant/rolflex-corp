<?php
    $special_pages = [
        'terms_conditions' => __('Terms &amp; conditions','rctd'),
        'privacy_statement' => __('Privacy statement', 'rctd' ),
        'cookies' => __('Cookies', 'rctd' ),
    ];
    $special_page_urls = get_field('special_pages', 'options');
?>

<ul class="rflex-footer-text rflex-list-inline">
    <li>&copy; <?= date('Y') ?> <?php info('organization.official_name', '') ?></li>
    <?php foreach( $special_pages as $special_page => $name ) : ?>
        <?php if( isset($special_page_urls[$special_page]) ): ?>
            <li><a class="rflex-<?= $special_page ?>" href="<?= $special_page_urls[$special_page] ?>" target="_blank"><?= $name ?></a></li>
        <?php endif; ?>
    <?php endforeach ?>
</ul>
