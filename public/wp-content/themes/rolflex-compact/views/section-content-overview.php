<?php $args = array_merge([
    'title' => __('Do you want to know more?', 'rctd'),
    'shade' => false,
    'read_more' => __('Read more', 'rctd'),
    'col_limit' => 3,
], $args ); ?>
<?php if( isset($args['content']) && count($args['content']) > 0 ): ?>
    <div class="rflex-section <?= $args['shade'] ? 'rflex-section-shaded' : '' ?>">
        <div class="container-lg">
            <div class="row">
                <div class="col-12 rflex-section-head text-center">
                    <h3 class="h3"><?= $args['title']  ?></h3>
                    <?php if(!empty($args['text'])): ?>
                        <?= $args['text'] ?>
                    <?php endif; ?>
                </div>
            </div>
            <div class="row justify-content-center">
                <?php foreach($args['content'] as $content ): ?>
                    <?php yield_part('tile-post-type-full', [
                    'hero' => $content['hero'],
                    'link' => $content['link'],
                    'title' => $content['title'],
                    'col_limit' => $args['col_limit'],
                    'content' => $content['content'],
                  //  'superscript' => sprintf('%1$s | %2$s', $project['country'], $project['sectors'] ),
                    'read_more' => $args['read_more'],
                ] ); ?>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
<?php endif; ?>