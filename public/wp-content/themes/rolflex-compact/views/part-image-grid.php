<?php
// Defaults that can be overridden
$args = array_merge([
    'limit' => 4,
    'override_captions' => false,
    'grid_tiles' => true,
], $args );


if( !is_numeric($args['limit']) ) {
    $col_lg_up = 'col-lg';
} else {
    $col_lg_up = 'col-lg-'.ceil(12 / $args['limit']);
}

if( $args['override_captions'] ) {
    foreach($args['images'] as $key => $image ) {
        $args['images'][$key]['image']['caption'] = $args['images'][$key]['caption_override'];
    }
} ?>
<div class="rflex-image-grid row justify-content-center">
    <?php foreach($args['images'] as $image):  ?>
    <div class="col-12 col-sm-6 col-md-6 <?= $col_lg_up ?> d-flex align-items-stretch">

        <div class="rflex-image-grid-tile <?= $args['grid_tiles'] ? 'rflex-tile' : '' ?>">
            <div class="rflex-inner">
                <div class="rflex-aspect-container">
                    <div class="rflex-aspect-item-wrapper">
                        <img class="rflex-aspect-item" src="<?= $image['image']['sizes']['grid-thumb'] ?>" alt="<?= $image['image']['title'] ?>" title="<?= $image['image']['title'] ?>" />
                    </div>
                </div>
                <?php if( !str_is_empty($image['image']['caption']) ): ?>
                    <p><?= $image['image']['caption'] ?></p>
                <?php endif; ?>
            </div>
        </div>

    </div>
    <?php endforeach; ?>
</div>