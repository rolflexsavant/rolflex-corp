<?php
$style_classes = '';

if( $args['hero']['style'] == 'overlay_light' ) {
    $style_classes = ' rflex-overlay-light';
}
if( $args['hero']['style'] == 'overlay_dark' ) {
    $style_classes = ' rflex-overlay-dark';
}

if( is_front_page() ) { // Pretend if we yielded the breadcrumbs on the frontpage, so they wont show
    did_yield_breadcrumbs(true);
}

?>
<?php if( $args['hero']['style'] != 'no_image' ): ?>
<header class="rflex-content-header rflex-hero<?= $style_classes ?>" style="background-image: url('<?= $args['hero']['image_large'] ?>')">
<?php else: ?>
<header class="rflex-content-header rflex-hero rflex-hero-no-image">
<?php endif; ?>
    <div class="container-lg">
        <?php if( $args['hero']['style'] != 'no_image' ): ?>
        <div class="row rflex-hero-body">
            <div class="col-12 col-md-8 col-lg-6">
        <?php else: ?>
        <div class="row rflex-hero-body justify-content-center">
            <div class="col-12 col-md-6 text-center">
        <?php endif; ?>
                <?php if( !empty($args['super']) ): ?>
                <p class="rflex-title-super"><?= $args['super'] ?></p>
                <?php endif; ?>
                <h1 class="h1"><?= $args['title'] ?></h1>
                <?= $args['hero']['text'] ?>

                <?php if( isset($args['component']['view']) ): ?>
                    <?php yield_part($args['component']['view'], $args['component']['args'] ); ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <?php if( !did_yield_breadcrumbs() ): ?>
    <div class="rflex-hero-breadcrumbs">
        <div class="container-lg">
            <div class="row">
                <div class="col-12">
                    <?php yield_breadcrumbs() ;?>
                </div>
            </div>
        </div>
    </div>
    <?php endif; ?>
</header>
<?php yield_part('part-polygon' , ['color' => 'primary', 'position' => 'bottom-left'] ); ?>
