<?php
$args = array_merge([
    'platform' => 'youtube',
    'video_id' => false,
], $args );

if( !empty($args['url']) ) {
    if( $args['platform'] == 'youtube') {
        $args['video_id'] =  url_query_value('v', $args['url']);
    }
}

if( $args['video_id'] && $args['platform'] == 'youtube') {
    /**
     * For further development and tweaking the youtube player:
     *
     * @see https://developers.google.com/youtube/player_parameters
     * @see https://developers.google.com/youtube/iframe_api_reference
     */
    $options = implode('&', [
        'controls' => '1',
        'modestbranding' => '1',
        'rel' => '0',
        'enablejsapi' => '1',
    ]);

    $args['src'] = 'https://www.youtube.com/embed/'.$args['video_id'].'?'.$options;
}

?>
<div class="row justify-content-center rflex-m-top">
    <div class="col-12">
        <?php if( !empty($args['title']) ): ?>
            <h3 class="h3"><?= $args['title'] ?></h3>
        <?php endif; ?>
        <?php if( !empty($args['text']) ): ?>
            <p><?= $args['text'] ?></p>
        <?php endif; ?>
    </div>
    <div class="col-12 col-md-10 col-lg-8">
        <?php if( !empty($args['src']) ): ?>
        <div class="rflex-aspect-container rflex-aspect-container-widescreen rflex-m-top">
            <div class="rflex-aspect-item-wrapper">
                <iframe src="<?= $args['src'] ?>" class="rflex-aspect-item rflex-aspect-iframe rflex-block-video-iframe" frameborder="0"></iframe>
            </div>
        </div>
        <?php else: ?>
            <?php yield_part('part-alert', [
                'title' =>  __('Video player' , 'rctd'),
                'message' => __('No video could be loaded in this video player...', 'rctd'),
                'type' => 'warning'
            ]); ?>
        <?php endif; ?>
    </div>
</div>