<?php
// Argument 'shade' can come from the module builder
if( isset($args['shade']) && $args['shade'] == true ) {
    $args['classes'] = 'rflex-section-shaded';
}

if( isset($args['gallery']) ): ?>
    <?php if( $args['shade'] ): ?>
        <?php yield_part('part-polygon' , ['color' => 'shade', 'position' => 'top-right'] ); ?>
    <?php endif; ?>
<section class="rflex-section text-center <?= $args['classes'] ?>">
    <div class="container-lg">
        <?php yield_part('part-gallery', $args ); ?>
    </div>
</section>
<?php if( $args['shade'] ): ?>
        <?php yield_part('part-polygon' , ['color' => 'shade', 'position' => 'bottom-left'] ); ?>
    <?php endif; ?>
<?php endif; ?>