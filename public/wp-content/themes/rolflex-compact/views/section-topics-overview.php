<?php
$args = array_merge([
    'title' => __('Do you want to know more about the compact door?', 'rctd'),
    'text'  => __('<p>Read more about the following topics</p>', 'rctd' ),
    'topics' => get_all_topics(),
], $args);
?>
<section class="rflex-section">
    <div class="container-lg">
        <div class="row">
            <div class="col-12 rflex-section-head rflex-feature-overview text-center">
                <h3 class="h3"><?= $args['title'] ?></h3>
                <?= $args['text']; ?>
            </div>
        </div>
        <div class="row justify-content-center">
            <?php foreach($args['topics'] as $topic): ?>
                <div class="rflex-tile-icon col-6 col-md-4 col-lg-3 d-flex align-items-stretch">
                    <a href="<?= $topic['link'] ?>" class="rflex-tile-icon-inner">
                        <i class="rflex-icon rflex-icon-<?= $topic['icon'] ?>"></i>
                        <span class="rflex-tile-icon-label rflex-icon-append rflex-icon-arrow-right"><?= $topic['name'] ?></span>
                    </a>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>