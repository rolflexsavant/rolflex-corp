<?php $args = array_merge([
    'id' => null,
    'classes' => '',
], $args );
?>
<div <?= !is_null($args['id']) ? 'id="'.$args['id'].'"' : '' ?> class="rflex-menu-bar <?= $args['classes'] ?>">
    <div class="container-lg">
        <div class="row">
            <div class="col">
            <?php if ( has_nav_menu( 'main-desktop' ) ) : ?>
                <?php wp_nav_menu([
                    // Custom walker properties
                    'href_hashtag_add_li_classes' => 'rflex-hashtag-class',
                    'href_hashtag_remove' => true,
                    'href_hashtag_classes' => 'rflex-submenu-title',
                    // Default walker properties
                    'depth'           => 2,
                    'items_wrap'      => '<ul id="%1$s" class="rflex-list-inline  %2$s">%3$s</ul>',
                    'theme_location'  => 'main-desktop',
                    'container'       => 'nav',
                    'container_class' => 'rflex-nav-desktop',
                    'container_id'    => '',
                    'menu_class'      => 'menu',
                    'menu_id'         => '',
                    'echo'            => true,
                ]); ?>
            <?php endif; ?>
            </div>
        </div>
    </div>
</div>