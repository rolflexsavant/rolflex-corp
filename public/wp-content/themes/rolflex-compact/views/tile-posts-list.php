<?php if( !empty($args['posts']) ): ?>
<div class="rflex-container rflex-side-container">
    <div class="rflex-inner">
        <h3 class="h4 h-upper rflex-icon-prepend rflex-icon-color-primary rflex-icon-<?= $args['icon'] ?>"><?= $args['title']; ?></h3>

        <ul class="rflex-list-group rflex-list-arrow">
            <?php foreach($args['posts'] as $post ): ?>
            <li><a href="<?= $post['link'] ?>"><?= $post['title'] ?></a></li>
            <?php endforeach; ?>
        </ul>
    </div>
</div>
<?php endif; ?>