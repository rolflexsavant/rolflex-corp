<?php $args = array_merge([
    'classes' => ''
], $args ); ?>
<address class="rflex-organization-address <?= $args['classes'] ?>">
    <?php info('organization.official_name') ?><br>
    <?php info('organization.address_line') ?><br>
    <?php info('organization.postal_code') ?><br>
    <?php info('organization.city') ?><br>
    <?php info('organization.country') ?>
</address>