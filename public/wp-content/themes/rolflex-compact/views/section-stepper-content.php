<?php
//d($args);

$img_size = 'square';
$link_label = __('Learn more', 'rctd') ?>

<div class="rflex-section rflex-section-compact-door">
    <div class="container-md">
        <div class="row">
            <div class="col-12 text-center rflex-section-compact-door-title">
                <p class="rflex-title-super"><?= $args['superscript'] ?></p>
                <h2 class="h2"><?= $args['title'] ?></h2>
                <?php if(!empty($args['subtext']) ): ?>
                <p><?= $args['subtext'] ?></p>
                <?php endif; ?>
            </div>
        </div>

        <?php if( !empty($args['steps']) ): ?>
        <div class="row justify-content-center rflex-section-compact-door-steps">
            <?php foreach($args['steps'] as $step ): ?>
                <div class="col-12 col-sm-3 text-center rflex-section-compact-door-illustration">
                    <img class="rflex-img-round" alt="<?= $step['image']['alt'] ?>" src="<?= $step['image']['sizes'][$img_size] ?>" />
                </div>
                <div class="col-12 col-sm-9 rflex-section-compact-door-content">
                    <h3 class="h4"><?= $step['title'] ?></h3>
                    <?= $step['content'] ?>

                    <?php if( !empty($step['link']) ): ?>
                        <a href="<?= $step['link'] ?>" class="rflex-icon-prepend rflex-icon-arrow-right"><?= $link_label ?></a>
                    <?php endif; ?>

                    <?php if( $step['sub_steps'] ): ?>
                    <div class="row rflex-m-top">

                        <?php foreach($step['sub_steps'] as $sub_step ): ?>
                        <div class="col-4 col-sm-3">
                            <img class="rflex-img-tile" alt="<?= $sub_step['image']['alt'] ?>" src="<?= $sub_step['image']['sizes'][$img_size] ?>"  />
                        </div>
                        <div class="col-8 col-sm-9">
                            <h4 class="h5"><?= $sub_step['title'] ?></h4>
                            <?= $sub_step['content'] ?>
                            <?php if( !empty($sub_step['link']) ): ?>
                            <a href="<?= $sub_step['link'] ?>" class="rflex-icon-prepend rflex-icon-arrow-right"><?= $link_label ?></a>
                            <?php endif; ?>
                        </div>
                        <?php endforeach; // end sub steps loop ?>

                    </div>
                    <?php endif; ?>

                </div>
            <?php endforeach; // end steps loop ?>
        </div>
        <?php endif; // end if steps ?>

        <?php if( !empty($args['cta']['link'] ) ): ?>
        <div class="row">
            <div class="col-12 text-center rflex-section-compact-door-footer">
                <div class="btn-wrapper">
                <?php yield_part('part-button', [
                    'link' => $args['cta']['link'],
                    'classes' => 'btn btn-cta btn-prim',
                    'icon' => 'rflex-icon-append rflex-icon-design-door',
                    'label' => empty($args['cta']['label']) ? __('Design your own door', 'rctd') : $args['cta']['label']
                ]) ?>
                </div>
            </div>
        </div>
        <?php endif ?>


    </div>
</div>