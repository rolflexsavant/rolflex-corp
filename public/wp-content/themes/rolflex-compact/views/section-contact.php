<?php $args = array_merge([ // Override defaults with existing args
    'superscript' => __('Contact', 'rctd'),
    'title' => __('Get in touch with us'),
], $args);
?>
<?php yield_part('part-polygon' , ['color' => 'shade', 'position' => 'top-right'] ); ?>
<section class="rflex-section text-center <?= $args['section_classes'] ?>">
    <div class="container-lg">
        <div class="row">
            <div class="col-12">
                <p class="rflex-title-super"><?= $args['superscript'] ?></p>
                <h3 class="h3"><?= $args['title'] ?></h3>
            </div>
        </div>
        <div class="row justify-content-center rflex-m-top">
            <?php foreach($args['contact_points'] as $contact ): ?>
            <div class="col-12 col-md-6 col-lg-4 col-xl-4 d-flex align-items-stretch text-left">
                <?php yield_part('tile-contact', $contact ); ?>
            </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>
<?php yield_part('part-polygon' , ['color' => 'shade', 'position' => 'bottom-left'] ); ?>