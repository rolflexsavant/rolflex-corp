<div class="rflex-container rflex-side-container rflex-project-meta">
    <div class="rflex-inner">
        <?php if( !empty($args['logo']) ): ?>
            <figure class="rflex-container-image">
                <img src="<?= $args['logo']['url'] ?>" title="<?= $args['logo']['title'] ?>" alt="<?= $args['logo']['alt'] ?>" />
            </figure>
        <?php endif; ?>
        <?php if( count($args['meta']) > 0 ): ?>
        <dl class="rflex-list-group">
            <?php foreach($args['meta'] as $meta ): if( !is_null($meta['value']) ): ?>
                <dt><?= $meta['label'] ?></dt>
                <dd><?= $meta['value'] ?></dd>
            <?php endif; endforeach; ?>
        </dl>
        <?php endif; ?>
    </div>
</div>