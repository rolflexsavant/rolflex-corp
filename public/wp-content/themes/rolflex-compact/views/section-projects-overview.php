<?php $args = array_merge([
    'title' => __('Awesome projects we have done', 'rctd'),
    'supertitle' => __('Projects', 'rctd'),
    'shade' => true,
], $args ); ?>
<?php if( isset($args['projects']) && count($args['projects']) > 0 ): ?>
    <?php if( $args['shade'] ): ?>
        <?php yield_part('part-polygon' , ['color' => 'shade', 'position' => 'top-right'] ); ?>
    <?php endif; ?>

    <div class="rflex-section <?= $args['shade'] ? 'rflex-section-shaded' : '' ?>">
        <div class="container-lg">
            <div class="row">
                <div class="col-12 rflex-section-head text-center">
                    <?php if(!empty($args['supertitle'])): ?>
                    <p class="rflex-title-super"><?= $args['supertitle'] ?></p>
                    <?php endif; ?>
                    <h3 class="h3"><?= $args['title']  ?></h3>
                    <?php if(!empty($args['description'])): ?>
                        <?= $args['description'] ?>
                    <?php endif; ?>
                </div>
            </div>
            <div class="row justify-content-center">
                <?php foreach($args['projects'] as $project ): ?>
                    <?php yield_part('tile-post-type-full', [
                    'hero' => $project['hero'],
                    'link' => $project['permalink'],
                    'title' => $project['title'],
                    'col_limit' => 2,
                    'content' => $project['description'],
                    'superscript' => sprintf('%1$s | %2$s', $project['country'], $project['sectors'] ),
                    'read_more' => __('View project'),
                ] ); ?>
                <?php endforeach; ?>
            </div>
            <?php if( true /** !is_singular('projects') */ ): // When we're not on an archive page ?>
                <div class="row rflex-m-top">
                    <div class="col-12 text-center rflex-m-top">
                        <?php yield_part('part-button', [
                            'link' => get_post_type_archive_link('projects'),
                            'classes' => 'btn btn-cta btn-prim',
                            'icon' => 'rflex-icon-append rflex-icon-arrow-right',
                            'label' => __('View all projects', 'rctd')
                        ]) ?>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>

    <?php if( $args['shade'] ): ?>
        <?php yield_part('part-polygon' , ['color' => 'shade', 'position' => 'bottom-left'] ); ?>
    <?php endif; ?>
<?php endif; ?>