<?php
/**
 * Alert partial. Types correspond with the bootstrap alert types
 * @see https://getbootstrap.com/docs/4.0/components/alerts/
 */
$args = array_merge([ // Defaults
    'title' => __('Oops...', 'rctd'),
    'message' => __('Something went wrong here...', 'rctd' ),
    'type' => 'warning',
], $args );

if( empty($args['icon']) ) {
    $args['icon'] = $args['type'];
}
?>
<div class="alert alert-<?= $args['type'] ?> rflex-m-top">
    <h4 class="h4 rflex-m-top"><?= $args['title'] ?></h4>
    <p><?= $args['message'] ?></p>
</div>