<div class="row rflex-btn-row text-center">
    <?php if( isset($args['lead']) && !is_null(isset($args['lead'])) ): ?>
    <div class="col-12">
        <h3><?= $args['lead'] ?></h3>
    </div>
    <?php endif; ?>
    <div class="rflex-btn-container col-12" style="padding: 30px 0 60px 0">
        <?php if( count($args['btns']) > 1 ): ?>
            <ul class="rflex-btn-list rflex-list-inline">
            <?php foreach($args['btns'] as $btn): ?>
                <li>
                    <?php yield_part('part-button', $btn ); ?>
                </li>
            <?php endforeach; ?>
            </ul>
        <?php else: $btn = array_shift($args['btns']) ?>
            <?php yield_part('part-button', $btn ); ?>
        <?php endif; ?>
    </div>
</div>