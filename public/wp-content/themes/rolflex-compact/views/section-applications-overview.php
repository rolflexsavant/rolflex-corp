<?php
$args = array_merge([
    'supertitle' => __('Applications', 'rctd'),
    'title' => __('Fits any kind of application', 'rctd'),
    'text'  => __('<p>The compact folding door is an <strong>industrial door</strong> that is designed to fit any kind of application.</p>', 'rctd' ),
    'shade' => true,
], $args);
?>
<?php if( $args['shade'] ): ?>
<?php yield_part('part-polygon' , ['color' => 'shade', 'position' => 'top-right'] ); ?>
<?php endif; ?>
<section class="rflex-section<?= $args['shade'] ? ' rflex-section-shaded' : '' ?>">

    <div class="container-lg">
        <div class="row">
            <div class="col-12 rflex-section-head rflex-feature-overview text-center">
                <?php if(!empty($args['supertitle'])): ?>
                <p class="rflex-title-super"><?= $args['supertitle'] ?></p>
                <?php endif; ?>
                <?php if(!empty($args['title'])): ?>
                <h3 class="h3"><?= $args['title'] ?></h3>
                <?php endif; ?>
                <?php if(!empty($args['text'])): ?>
                <?= $args['text']; ?>
                <?php endif; ?>
            </div>
        </div>
        <div class="row justify-content-center">
            <?php foreach($args['applications'] as $application): //d($application['hero']['image_large']) ?>
                <div class="rflex-tile-image text-center col-6 col-md-4 col-lg-3 d-flex align-items-stretch">
                    <a href="<?= $application['link'] ?>" class="rflex-tile-icon-inner">
                        <img class="rflex-tile-image-image" alt="<?=$application['hero']['image_large']['alt']?>" src="<?= $application['hero']['image_large']['sizes']['hd-mini-cropped'] ?>">
                        <span class="rflex-tile-image-label"><?= $application['name'] ?></span>
                    </a>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>
<?php if( $args['shade'] ): ?>
<?php yield_part('part-polygon' , ['color' => 'shade', 'position' => 'bottom-left'] ); ?>
<?php endif; ?>