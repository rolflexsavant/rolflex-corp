<?php $args = array_merge([
    'classes' => ''
], $args ); ?>
<ul class="rflex-organization-business rflex-list-clean <?= $args['classes'] ?>">
    <li><strong><?php _e('KVK', 'rctd') ?></strong> <?php info('organization.cc_kvk') ?></li>
    <li><strong><?php _e('BTW', 'rctd') ?></strong> <?php info('organization.vat_btw') ?></li>
    <li><strong><?php _e('IBAN', 'rctd') ?></strong> <?php info('organization.iban') ?></li>
</ul>