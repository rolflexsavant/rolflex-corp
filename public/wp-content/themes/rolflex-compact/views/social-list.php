<?php
    $social_media = get_field('social_media', 'options');
?>
<?php if(!empty($social_media) ): ?>
<ul class="rflex-social-list rflex-list-inline">
    <?php foreach($social_media as $item ): ?>
    <li class="rflex-social-media rflex-social-<?= $item['platform'] ?>">
        <a href="<?= $item['url'] ?>" class="<?= $item['platform'] ?>" target="_blank">
            <i class="rflex-icon rflex-icon-<?= $item['platform'] ?>"></i>
            <span class="d-none"><?= $item['name'] ?></span>
        </a>
    </li>
    <?php endforeach; ?>
</ul>
<?php endif; ?>