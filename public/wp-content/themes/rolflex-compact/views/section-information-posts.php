<section class="rflex-section">
    <div class="container-lg">
        <div class="row justify-content-between">
            <?php if( $args['information_type'] == 'custom' ): ?>
            <div class="col-12 col-lg-6 rflex-editor-content">
                <?php if( !empty($args['title']) ): ?>
                    <h2 class="h2"><?= $args['title'] ?></h2>
                <?php endif; ?>
                <?php if( !empty($args['content']) ): ?>
                    <?= wpautop($args['content']) ?>
                <?php endif; ?>
            </div>
            <?php endif; ?>
            <?php yield_part('tile-latest-posts',[
                'col_classes' => 'col-12 col-lg-5',
                'posts_per_page' => $args['posts_per_page'],
                'categories' => $args['categories']
            ]); ?>
        </div>
    </div>
</section>