<?php $args = array_merge([
    'icon' => false,
    'label' => __('Included', 'rctd'),
    'col_limit' => 4,
], $args ); ?>
<div class="<?= classes_col_limit($args['col_limit']) ?> d-flex align-items-stretch <?= !empty($args['link']) ? ' rflex-clickable' : '' ?>">
    <div class="rflex-tile rflex-tile-selling">
        <div class="rflex-inner">
            <?php if($args['icon'] ): ?>
            <i class="rflex-icon rflex-icon-<?= $args['icon'] ?>"></i>
            <?php endif; ?>
            <h4 class="h4"><?= $args['title'] ?></h4>
        </div>
        <?php if( !empty($args['link']) ): ?>
        <a href="<?= $args['link'] ?>" class="rflex-tile-bottom">
            <?= $args['label']; ?>
        </a>
        <?php else: ?>
        <span class="rflex-tile-bottom">
            <?= $args['label']; ?>
        </span>
        <?php endif; ?>
    </div>
</div>