<?php
// Defaults...
$args = array_merge([
    'title' =>  __("Don't miss out on anything", 'rctd'),
    'view_all_label' => __('See what else is new', 'rctd'),
    'posts_per_page' => 5,
    'col_classes' => 'col-12 col-lg-5',
    'categories' => false,
], empty($args) ? [] : $args );

// If there are categories selected, narrow the query vars
if( is_array($args['categories']) ) {
    $query_vars['category__in'] = $args['categories'];

    // If only one category is set, link to that
    if( count($args['categories']) == 1 ) {
        $catID = array_shift($args['categories']);
        $args['archive_link'] = get_term_link($catID);
    }
}
// Multiple categories set? Link to the blog (no categories selected = all categories = multiple categories set = true )
if( !isset($args['archive_link']) ) {
    $args['archive_link'] = get_permalink( get_option( 'page_for_posts') );
}

// Get the posts
$query_vars['posts_per_page'] = $args['posts_per_page'];
$args['posts'] = rflex_get_latest_posts( $query_vars );
?>
<div class="<?= $args['col_classes'] ?>">

    <div class="rflex-tile-shaded">
        <h3><?= $args['title'] ?></h3>
        <ul class="rflex-posts-list rflex-list-clean">
            <?php foreach($args['posts'] as $post): ?>
            <li>
                <a  class="row" href="<?= $post['permalink'] ?>">
                    <span class="col-4">
                        <?php if( $post['hero'] ): ?>
                        <img src="<?= $post['hero']['image_thumb'] ?>" alt="<?= $post['hero']['alt'] ?>">
                        <?php endif; ?>
                    </span>
                    <span class="col align-self-center rflex-post-list-title">
                        <?= $post['title'] ?>
                    </span>
                </a>
            </li>
            <?php endforeach; ?>
        </ul>

        <a href="<?= $args['archive_link'] ?>" class="rflex-icon-append rflex-icon-arrow-right rflex-m-top"><?= $args['view_all_label'] ?></a>
    </div>
</div>