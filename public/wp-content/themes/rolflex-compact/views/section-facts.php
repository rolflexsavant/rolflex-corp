<?php if( !empty($args['facts']) ):  ?>
<div class="rflex-section text-center">
    <div class="container-lg rflex-facts-figures-grid">
        <div class="row">
            <div class="col-12">
                <h3 class="h3"><?php _e('Facts & Figures', 'rctd') ?></h3>
            </div>
        </div>
        <div class="row justify-content-center">
            <?php foreach($args['facts'] as $fact): ?>
            <div class="col-6 col-md-4 col-lg-3 rflex-facts-figures-tile">
                <div class="rflex-facts-figures-value">
                    <?= $fact['value'] ?>
                </div>
                <h4 class="h4"><?= $fact['label'] ?></h4>
            </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>
<?php endif; ?>