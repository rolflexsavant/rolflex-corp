<?php
if( isset($args['testimonial']) && $args['testimonial'] instanceof WP_Post ) {
    $args = array_merge(
        $args,
        get_testimonial_fields($args['testimonial'])
    );
} else {
    $args = array_merge( get_testimonial_fields(),  $args );
}

?>
<section class="rflex-section rflex-section-diapositive rflex-testimonial">
    <div class="container-lg">
        <div class="row justify-content-center d-flex align-items-center">
            <div class="col-12 col-md-4">
                <p class="rflex-testimonial-title"><?= $args['title'] ?></p>
            </div>
            <div class="col-12 col-md-8 col-lg-7">
                <div class="rflex-testimonial-body">
                    <p class="rflex-testimonial-content">
                        <?= $args['testimonial_content'] ?>
                    </p>
                    <div class="row rflex-testimonial-signature d-flex align-items-center">
                        <?php if( !is_null($args['testimonial_person']['image']['url']) ): ?>
                        <div class="col-3 col-sm-2 col-md-2 col-lg-1">
                            <img class="rflex-testimonial-picture" alt="<?= $args['testimonial_person']['image']['alt'] ?>" src="<?= $args['testimonial_person']['image']['url'] ?>" />
                        </div>
                        <?php endif; ?>
                        <div class="col-auto">
                            <span class="rflex-testimonial-person"><?= $args['testimonial_person']['name'] ?></span>
                            <span class="rflex-testimonial-job"><?= $args['testimonial_person']['job_title'] ?> </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>