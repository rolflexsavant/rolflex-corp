<?php rflex_numeric_posts_nav(
    '<ul class="rflex-nav-numeric-posts">',
    '</ul>',
    '<span class="numeric-nav-item-label"><i class="rflex-icon rflex-icon-arrow-left"></i>&nbsp;'.__('<span class="d-md-none">Previous</span>', 'rctd').'</span>',
    '<span class="numeric-nav-item-label">'.__('<span class="d-md-none">Next</span>', 'rctd').'&nbsp;<i class="rflex-icon rflex-icon-arrow-right"></i></span>'
); ?>