<?php
/**
 * Template Name: Contact Template
 */
global $post;

$fields = get_fields($post);

?>
<?php yield_header(); ?>
<section <?php post_class(); ?>>
    <?php yield_part('section-hero', [
        'title' => get_the_title(),
        'hero' => get_hero_fields($fields['hero']),
    ]); ?>
    <?php yield_part('section-knowledge-center-articles', [
        'articles' => get_fields_articles($fields['knowledge_center_articles'])
    ]) ?>
    <?php yield_part('section-contact', [
        'section_classes' => 'rflex-section-shaded',
        'contact_points'  => get_info('contact_points', []),
    ]) ?>
    <section class="rflex-section">
        <div class="container-lg">
            <div class="row justify-content-between">

                <?php if( !empty($fields['contact_image']) ): ?>
                <div class="col-12 col-sm-6 col-md-7 col-lg-7  order-sm-2 rflex-margin-bottom  text-center text-md-right">

                        <img class="rflex-rounded rflex-img-adjust" title="<?= array_get($fields, 'contact_image.title') ?>" alt="<?= array_get($fields, 'contact_image.title') ?>" src="<?= array_get($fields, 'contact_image.sizes.large') ?>">

                </div>
                <?php endif; ?>
                <div class="col-12 col-sm-6 col-md-5 col-lg-4 col-xl-4 text-left">
                    <h3 class="h3"><?php _e('Address information', 'rctd') ?></h3>
                    <?php yield_part('info-address', [
                        'classes' => 'rflex-col-content-group'
                    ]) ?>
                    <?php yield_part('info-business' , [
                            'classes' => 'rflex-col-content-group'
                    ]); ?>
                    <?php yield_part('info-visit-us' , [
                            'classes' => 'rflex-col-content-group'
                    ]); ?>
                </div>
            </div>
        </div>
    </section>
</section>
<?php yield_footer(); ?>