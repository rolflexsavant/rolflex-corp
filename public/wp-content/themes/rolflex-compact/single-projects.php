<?php

//d($post);

$args = isset($args) ? $args : [];
$args = array_merge($args, get_fields_project($post, true ) );

?>
<?php yield_header(); ?>
<section <?php post_class('rflex-content rflex-post rflex-project'); ?>>
    <?php yield_part('section-content', [
        'hero' => $args['hero'],
        'description' => $args['description'],
        'aside' => [
            'aside-meta' => [
                'logo' => $args['logo'],
                'meta' => $args['meta']
            ],
        ]
    ]) ?>

    <?php if( !empty($args['gallery']['images']) && is_array($args['gallery']['images']) ):  ?>
        <?php yield_part('section-gallery', [
            'gallery' => $args['gallery'],
            'superscript' => __('Impression', 'rctd'),
            'title' => __('Photo gallery', 'rctd'),
            'shade' => true,
        ]); ?>
    <?php endif; ?>

    <?php if( isset($args['features']) ):  ?>
        <?php yield_part('section-features-overview', [
            'features' => $args['features']
        ]); ?>
    <?php endif; ?>



    <?php if( isset($args['facts']) ):  ?>
        <?php yield_part('section-facts', [
            'facts' => $args['facts']
        ]); ?>
    <?php endif; ?>

    <?php yield_part('section-banner') ?>

    <?php if( isset($args['related_projects']) && count($args['related_projects']) > 0 ): ?>
        <?php yield_part('section-projects-overview' , [
            'title' => __('Want to see more projects?', 'rctd'),
            //'description' => 'test description',
            'projects' => $args['related_projects']
        ]); ?>
    <?php endif; ?>

</section>
<?php yield_footer(); ?>
