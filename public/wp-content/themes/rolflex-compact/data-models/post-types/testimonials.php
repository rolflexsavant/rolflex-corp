<?php return [
        'labels'              => [
            'name'               => __('Testimonials', 'cdc'),
            'singular_name'      => __('Testimonial', 'cdc'),
            'menu_name'          => __('Testimonials', 'cdc'),
            'name_admin_bar'     => __('Testimonials', 'cdc'),
            'add_new'            => __('Add New', 'cdc'),
            'add_new_item'       => __('Add New Testimonial', 'cdc'),
            'new_item'           => __('New Testimonial', 'cdc'),
            'edit_item'          => __('Edit Testimonial', 'cdc'),
            'view_item'          => __('View Testimonials', 'cdc'),
            'all_items'          => __('All Testimonials', 'cdc'),
            'search_items'       => __('Search Testimonials', 'cdc'),
            'parent_item_colon'  => __('Parent Testimonials', 'cdc'),
            'not_found'          => __('No Testimonials Found', 'cdc'),
            'not_found_in_trash' => __('No Testimonials Found in Trash', 'cdc'),
        ],

        // Change these values when we have a template
        'public'              => false,
        'exclude_from_search' => true,
        'publicly_queryable'  => false,
        // End change

        'show_ui'             => true, // (bool) Whether to generate and allow a UI for managing this post type in the admin. Default is value of $public.
        'show_in_nav_menus'   => true, // (bool) Makes this post type available for selection in navigation menus. Default is value of $public.
        'show_in_menu'        => true, // (bool|string) Where to show the post type in the admin menu. To work, $show_ui must be true. If true, the post type is shown in its own top level menu. If false, no menu is shown. If a string of an existing top level menu (eg. 'tools.php' or 'edit.php?post_type=page'), the post type will be placed as a sub-menu of that. Default is value of $show_ui.
        'show_in_admin_bar'   => true, // (bool) Makes this post type available via the admin bar. Default is value of $show_in_menu.
        'show_in_rest'          => true,
        'menu_position'       => 25, // (int) The position in the menu order the post type should appear. To work, $show_in_menu must be true. Default null (at the bottom).
        'menu_icon'           => rflex_get_dash_icon('testimonials'),
        'capability_type'     => 'post',
        'hierarchical'        => false,
        'supports'            => array( 'title', 'revisions' ),
        'has_archive'         => false,
        'rewrite'             => array('slug' => get_info('custom_post_type_rewrites.testimonials' ,'testimonials'), 'with_front' => false ),
        'query_var'           => false
];