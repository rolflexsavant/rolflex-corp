<?php return [
    'object_type' => ['projects'], // Can also be an array
    'args' => [
        'labels' => [
            'name'                       => __( 'Applications', 'rctd' ),
            'singular_name'              => __( 'Application', 'rctd' ),
            'search_items'               => __( 'Search Application', 'rctd' ),
            'popular_items'              => __( 'Populair Applications', 'rctd' ),
            'all_items'                  => __( 'All Applications', 'rctd' ),
            'parent_item'                => null,
            'parent_item_colon'          => null,
            'edit_item'                  => __( 'Edit Application', 'rctd' ),
            'update_item'                => __( 'Update Application', 'rctd' ),
            'add_new_item'               => __( 'New Application', 'rctd' ),
            'new_item_name'              => __( 'New Application Name', 'rctd' ),
            'separate_items_with_commas' => __( 'Separate Applications with commas', 'rctd' ),
            'add_or_remove_items'        => __( 'Add or Remove Applications', 'rctd' ),
            'choose_from_most_used'      => __( 'Choose from most used Applications', 'rctd' ),
            'not_found'                  => __( 'No Applications Found', 'rctd' ),
            'menu_name'                  => __( 'Applications', 'rctd' ),
        ],
        'rewrite'               => [
            'slug' => get_info('custom_post_type_rewrites.project_applications', __('applications' , 'rctd') ),
            'with_front' => false
        ],
        'hierarchical' => true,
        'query_var' => true,
        'show_in_nav_menus' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_admin_column' => true,
        'show_in_rest'          => true,
    ]
];