<?php return [
    'object_type' => ['downloads'], // Can also be an array
    'args' => [
        'labels' => [
            'name'                       => __( 'Download Categories', 'rctd' ),
            'singular_name'              => __( 'Download Category', 'rctd' ),
            'search_items'               => __( 'Search Download Category', 'rctd' ),
            'popular_items'              => __( 'Populair Download Categories', 'rctd' ),
            'all_items'                  => __( 'All Download Categories', 'rctd' ),
            'parent_item'                => null,
            'parent_item_colon'          => null,
            'edit_item'                  => __( 'Edit Download Category', 'rctd' ),
            'update_item'                => __( 'Update Download Category', 'rctd' ),
            'add_new_item'               => __( 'New Download Category', 'rctd' ),
            'new_item_name'              => __( 'New Download Category Name', 'rctd' ),
            'separate_items_with_commas' => __( 'Separate Download Categories with commas', 'rctd' ),
            'add_or_remove_items'        => __( 'Add or Remove Download Categories', 'rctd' ),
            'choose_from_most_used'      => __( 'Choose from most used Download Categories', 'rctd' ),
            'not_found'                  => __( 'No Download Categories Found', 'rctd' ),
            'menu_name'                  => __( 'Categories', 'rctd' ),
        ],
        'rewrite'               => [
            'slug' => get_info('custom_post_type_rewrites.downloads', 'type') .'/'. get_info('custom_post_type_rewrites.download_types', 'download-type')
        ],
        'hierarchical' => true,
        'query_var' => true,
        'show_in_nav_menus' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_admin_column' => true,
        'show_in_rest'          => true,
    ]
];