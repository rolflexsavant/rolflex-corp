<?php return [
    'object_type' => ['articles'], // Can also be an array
    'args' => [
        'labels' => [
            'name'                       => __( 'Topics', 'rctd' ),
            'singular_name'              => __( 'Topic', 'rctd' ),
            'search_items'               => __( 'Search Topic', 'rctd' ),
            'popular_items'              => __( 'Populair Topics', 'rctd' ),
            'all_items'                  => __( 'All Topics', 'rctd' ),
            'parent_item'                => null,
            'parent_item_colon'          => null,
            'edit_item'                  => __( 'Edit Topic', 'rctd' ),
            'update_item'                => __( 'Update Topic', 'rctd' ),
            'add_new_item'               => __( 'New Topic', 'rctd' ),
            'new_item_name'              => __( 'New Topic Name', 'rctd' ),
            'separate_items_with_commas' => __( 'Separate Topics with commas', 'rctd' ),
            'add_or_remove_items'        => __( 'Add or Remove Topics', 'rctd' ),
            'choose_from_most_used'      => __( 'Choose from most used Topics', 'rctd' ),
            'not_found'                  => __( 'No Topics Found', 'rctd' ),
            'menu_name'                  => __( 'Topics', 'rctd' ),
        ],
        'rewrite'               => [
            'slug' => get_info('custom_post_type_rewrites.knowledge_center_articles', 'articles') .'/'. get_info('custom_post_type_rewrites.knowledge_center_topics', 'topics')
        ],
        'hierarchical' => true,
        'query_var' => true,
        'show_in_nav_menus' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_admin_column' => true,
        'show_in_rest'          => true,
    ]
];