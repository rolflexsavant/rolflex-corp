
// function preventDefault(e) {
//     e.preventDefault();
// }

// function preventDefaultForScrollKeys(e) {
//     if (keys[e.keyCode]) {
//         preventDefault(e);
//         return false;
//     }
// }

// // modern Chrome requires { passive: false } when adding event
// var supportsPassive = false;
// try {
//   window.addEventListener("test", null, Object.defineProperty({}, 'passive', {
//     get: function () { supportsPassive = true; }
//   }));
// } catch(e) {}

// var wheelOpt = supportsPassive ? { passive: false } : false;
// var wheelEvent = 'onwheel' in document.createElement('div') ? 'wheel' : 'mousewheel';



/**************************************************************
 * Hero animation class
 */
class HeroAnimation {

    lgSize = 1130;

    /**
     * Modifiers
     */
    canvasId;

    headerContainerId;

    heroContainerId;

    frameCount;

    width;

    height;

    framesUrl;

    framesFileExtension;

    //documentCompensation = 1000;

    animationLength = 1000;

    factor;

    /**
     * Core elements
     */

    docHeight;

    mainPadding;

    html;

    header;

    hero;

    canvas;

    context;

    frames = [];


    constructor({
       canvasId,
       mainContainerId,
       headerContainerId,
       heroContainerId,
       width,
       height,
       framesUrl,
       framesCount,
       framesFileExtension = '.png'
    }) {
        this.canvasId = canvasId;
        this.mainContainerId = mainContainerId;
        this.headerContainerId = headerContainerId;
        this.heroContainerId = heroContainerId;
        this.width = width;
        this.height = height;
        this.framesUrl = framesUrl;
        this.framesCount = framesCount;
        this.framesFileExtension = framesFileExtension;
    }


    init() {
        this.canvasContainer = document.getElementById('rflex-animation-container');

        if( this.testCompatibility() ) {
            this.initDocument();
            this.initCanvas();
            this.initFrames();
            this.processScroll();

            window.addEventListener('scroll', () => {
                this.processScroll();
            });
        }
    }


    testCompatibility() {
        if( window.innerWidth >= this.lgSize ) {
            return true;
        }
        return false;
    }


    initDocument() {
        this.layout = document.getElementById('rflex-layout');
        this.html = document.documentElement;
        this.docHeight = this.layout.offsetHeight;

        this.header = document.getElementById(this.headerContainerId);
        this.hero = document.getElementById(this.heroContainerId);
        this.main = document.getElementById(this.mainContainerId);
        this.mainPadding = this.main.paddingTop;

        this.hero.classList.add('is-animated');
        this.main.classList.add('is-animated');
    }


    initCanvas() {
        this.canvas = document.createElement('canvas');
        this.canvas.id = "hero-animation";

        this.canvasContainer.innerHTML = '';
        this.canvasContainer.appendChild(this.canvas);

        this.context = this.canvas.getContext("2d");
        this.context.save();

        this.factor = this.height / this.width;

        let canvasWidth = this.canvas.offsetWidth;
        let canvasHeight = canvasWidth * this.factor;

        this.canvas.width = canvasWidth;
        this.canvas.height = canvasHeight;
    }


    initFrames() {
        for( let i = 0; i <= this.framesCount; i++ ) {
            const image     = new Image();
            image.src       = this.getCurrentFrameUrl(i);
            this.frames[i]  = image;
        }

        this.frames[0].onload = () => {
            this.showFrame(0);
        }
    }


    processScroll() {
        const currentScrollPosition = this.html.scrollTop;
        const animationEnd = 0 + this.animationLength;
        const scrollFraction = currentScrollPosition / animationEnd;

        this.updateFrame(scrollFraction);

        if( currentScrollPosition > this.header.offsetHeight ) {

            if( currentScrollPosition < animationEnd ) {
                this.setDocHeight(this.docHeight + this.animationEnd);
                this.main.classList.add('is-sticky');
                this.main.style.paddingTop = 0;
            }

            else {
                this.setDocHeight(this.docHeight);
                this.main.style.paddingTop = this.animationLength - this.header.offsetHeight + 'px';
                this.main.classList.remove('is-sticky');
            }
        }

        if( currentScrollPosition <= this.header.offsetHeight ) {
            this.setDocHeight(this.docHeight);
            this.main.style.paddingTop = 0;
            this.main.classList.remove('is-sticky');
        }
    }


    setDocHeight( height ) {
        this.layout.style.height = `${height}px`;
    }


    updateFrame(scrollFraction) {
        const frameIndex = Math.min(
            this.framesCount - 1,
            Math.ceil(scrollFraction * this.framesCount)
        );

        requestAnimationFrame(() => this.showFrame(frameIndex + 1))
    }


    showFrame(i) {
        this.context.clearRect(0,0, this.canvas.width, this.canvas.height );
        this.context.restore();
        this.context.drawImage(this.frames[i], 0, 0, this.canvas.width, this.canvas.height );
    }


    getCurrentFrameUrl( index ) {
        const frameNumberString = index.toString().padStart(4, '0');
        const frameUrl = this.framesUrl + frameNumberString + this.framesFileExtension;
        return frameUrl;
    }


}