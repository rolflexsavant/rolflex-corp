/**************************************************************
 * Class that holds all the theme related javascript logic
 */
class Theme {

  lgSize = 1130;

  /**
   * ids
   */

  /**
   * All the classnames of elements we use are stored here
   */
  classNames;

  /**
   * The mobile navigation buttons
   */
  mobNavButtons = [];

  /**
   * Container of mobile navigation
   */
  mobNavContainers = [];

  /**
   * Sticky ID
   */
  stickyId;

  /**
   * Header
   */
  stickyContainer;

  /**
   * StickyOffset
   */
  stickyOffset;

  /**
   *
   * @param {*} param0
   */
  stickyMenuContainer;

  /**
   *
   */
  stickySearchConsole;

  /**
   *
   * @param {*} param0
   */
  stickyMenuOffset = 400;

  /**
   * Sets up the Theme Javascript
   * @param {*} param
   */
  constructor({
    classNames,
    togglePairs,
    stickyId,
    stickyMenuId,
    imageGalleryConfigs = []
  }) { // Classes set outside the theme class are specified in the constructor
    this.classNames           = classNames;
    this.togglePairs          = togglePairs;
    this.stickyId             = stickyId;
    this.stickyMenuId         = stickyMenuId;
    this.imageGalleryConfigs  = imageGalleryConfigs;
  }

  /**
   * Image
   */
  init() {
    this.mobNavButtons        = document.getElementsByClassName(this.classNames.btnMobNav);
    this.mobNavContainers     = document.getElementsByClassName(this.classNames.containerMobNav);
    this.stickyContainer      = document.getElementById(this.stickyId);
    this.stickyMenuContainer  = document.getElementById(this.stickyMenuId);
    this.stickySearchConsole  = document.getElementById('rflex-search-console');
    this.submenuTitles        = document.getElementsByClassName(this.classNames.submenuTitles);
    this.clickableElements    = document.getElementsByClassName(this.classNames.clickableElements );
    this.toggleElements       = document.getElementsByClassName(this.classNames.toggleElements);


    for(let i = 0; i < this.toggleElements.length; i++ ){
      this.toggleElements[i].addEventListener('click', (e) => {

        if( !this.toggleElements[i].classList.contains('is-toggled') ) {
          e.stopPropagation();

          document.addEventListener('click', (e) => {
            this.toggleElements[i].classList.remove('is-toggled');
          }, {once: true});

          this.toggleElements[i].classList.add('is-toggled');

          return e.preventDefault();
        }

      })
    }


    for( let i = 0; i < this.togglePairs.length; i++ ) {
      let togglePairEventName = this.togglePairs[i].toggleEventName;
      let togglePairTriggers = document.getElementsByClassName(this.togglePairs[i].toggleTriggerClass );
      let togglePairTarget = document.getElementById(this.togglePairs[i].toggleTargetId);

      for( let i = 0; i < togglePairTriggers.length; i++ ) {
          let trigger = togglePairTriggers[i];

          //console.log({trigger: trigger, event: togglePairEventName });
          trigger.addEventListener( togglePairEventName , (e) => {
            e.preventDefault();

            if( togglePairTarget.classList.contains('is-toggled') ) {
              for(let i = 0; i < togglePairTriggers.length; i++ ) {
                togglePairTriggers[i].classList.remove('is-toggled');
              }

              togglePairTarget.classList.remove('is-toggled');
            }
            else {
              trigger.classList.add('is-toggled');
              togglePairTarget.classList.add('is-toggled');
            }
          });
      }
    }

    //console.log(this.stickyContainer.classList, stickyId);
    this.stickyOffset = this.stickyContainer.offsetTop;

    for( let i = 0; i < this.mobNavButtons.length; i++ ) {
      this.mobNavButtons[i].addEventListener('click', (e) => {
        this.toggleMobileNavigation(e, this.mobNavButtons[i] );
      });
    }


    for( let i = 0; i < this.submenuTitles.length; i++ ) {
      this.submenuTitles[i].addEventListener('click', (e) => {
        this.toggleSubmenu(e, this.submenuTitles[i] );
      });
    }

    for( let i = 0; i < this.classNames.disableClickClasses.length; i++ ) {
      let disableClickElements = document.querySelectorAll(this.classNames.disableClickClasses[i]);

      for( let i = 0; i < disableClickElements.length; i++ ) {
        disableClickElements[i].addEventListener('click', (e) => {
          e.preventDefault();
        });
      }
    }

    for( let i = 0; i < this.imageGalleryConfigs.length; i++ ) {
      let imageGalleryConfig =  this.imageGalleryConfigs[i];
      this.initGallery( imageGalleryConfig );
    }

    window.addEventListener('scroll',  () => {
      this.toggleSticky();
    });

    for(let i = 0; i < this.clickableElements.length; i++ ) {
      let clickableElement = this.clickableElements[i];
      let links = clickableElement.getElementsByTagName('a');

      if( links.length > 0 ) {
        let a = links[0];

        clickableElement.addEventListener('click', (e) => {
          this.redirect(a.href);
        });
      }
    }

  }

  redirect( url ) {
    window.location = url;
  }


  getElementsFromClassArray( classArray , htmlNode ) {
    let elements = [];

    for(let i = 0; i < classArray.length; i++ ) {
      let className  = classArray[i];
      let classElements  = htmlNode.getElementsByClassName(className);

      for(i = 0; i < classElements.length; i++) {
        elements.push(classElements[i]);
      }
    }

    return elements;
  }

  /********************************************************************
   * IMAGE GALLERY
   */
  initGallery( imageGalleryConfig ) {
    let imgGalleryContainers = document.getElementsByClassName(imageGalleryConfig.galleryClass);

    for( let i = 0; i < imgGalleryContainers.length; i++ ) {
      let galleryContainer = imgGalleryContainers[i];
      let items = galleryContainer.getElementsByClassName(imageGalleryConfig.galleryItemClass);
      let closeControls = this.getElementsFromClassArray(imageGalleryConfig.controlsClasses.close, galleryContainer )
      let nextControls = this.getElementsFromClassArray(imageGalleryConfig.controlsClasses.next, galleryContainer );
      let prevControls = this.getElementsFromClassArray(imageGalleryConfig.controlsClasses.prev, galleryContainer );
      let modal = galleryContainer.getElementsByClassName(imageGalleryConfig.galleryModalClass);

      if( modal.length > 0 ) {
        modal = modal[0];
      }

      let modalImages = modal.getElementsByClassName(imageGalleryConfig.galleryModalImageClass);

      for(let i = 0; i < closeControls.length; i++ ) {
        let control = closeControls[i];

        control.addEventListener('click', (e) => {
          this.galleryToggleModal(modal);
          return e.preventDefault();
        });
      }

      for(let i = 0; i < items.length; i++ ) {
        let item = items[i];

        item.addEventListener('click', (e) => {
          this.galleryToggleModal(modal, () => {
            this.galleryShowImage(i, modalImages );
          });
          return e.preventDefault();
        });
      }


      for(let i = 0; i < modalImages.length; i++ ) {
        let clickImage = modalImages[i];
        let showImage = i + 1;

        if( showImage == modalImages.length || showImage > modalImages.length ) {
          showImage = 0;
        }

        clickImage.addEventListener('click', (e) => {
          this.galleryShowImage(showImage, modalImages );
          return e.preventDefault();
        })
      }

      for( let i = 0; i < nextControls.length; i++ ) {
        let nextControl = nextControls[i];

        nextControl.addEventListener('click', (e) => {

          for(let i = 0; i < modalImages.length; i++) {
            if( !modalImages[i].classList.contains('is-toggled') ) {
              modalImages[i].classList.add('is-toggled');
              let nextImage = i + 1;

              if( nextImage == modalImages.length || nextImage > modalImages.length ) {
                nextImage = 0;
              }

              modalImages[nextImage].classList.remove('is-toggled');
              break;
            }
          }
          e.preventDefault();
        })
      }

      for( let i = 0; i < prevControls.length; i++ ) {
        let prevControl = prevControls[i];

        prevControl.addEventListener('click', (e) => {

          for(let i = 0; i < modalImages.length; i++) {
            if( !modalImages[i].classList.contains('is-toggled') ) {
              modalImages[i].classList.add('is-toggled');
              let prevImage = i - 1;

              if( prevImage < 0 ) {
                prevImage = modalImages.length - 1;
              }

              modalImages[prevImage].classList.remove('is-toggled');
              break;
            }
          }
          e.preventDefault();
        })
      }

    }
  }


  galleryShowImage( showImage , allImages ) {
    for( let i = 0; i < allImages.length; i ++ ) {
      if( !allImages[i].classList.contains('is-toggled') ) {
        allImages[i].classList.add('is-toggled');
      }
    }

    allImages[showImage].classList.remove('is-toggled');
  }


  galleryToggleModal( modal , afterToggleCb ) {
    if( modal.classList.contains('is-toggled') ) {
      modal.classList.remove('is-toggled');
    } else {
      modal.classList.add('is-toggled');
    }
    if( afterToggleCb !== undefined ) {
      afterToggleCb();
    }
  }


  /**
   * Toggle the mobile navigation
   *
   * @param {*} event
   * @param {*} btn
   */
  toggleMobileNavigation( event, btn ) {
    if( !btn.classList.contains('is-toggled') ) {
      btn.classList.add('is-toggled');

      for(let i = 0; i < this.mobNavContainers.length; i++ ) {
        if( !this.mobNavContainers[i].classList.contains('is-toggled') ) {
          this.mobNavContainers[i].classList.add('is-toggled');
        }
      }
    } else {
      btn.classList.remove('is-toggled');

      for(let i = 0; i < this.mobNavContainers.length; i++ ) {
        if( this.mobNavContainers[i].classList.contains('is-toggled') ) {
          this.mobNavContainers[i].classList.remove('is-toggled');
        }
      }
    }

    return event.preventDefault();
  }


  toggleSubmenu( event, btn ) {
    let parent = btn.parentElement;

    if( parent.classList.contains('is-toggled') ) {
      parent.classList.remove('is-toggled');
    } else {
      parent.classList.add('is-toggled');
    }

    return event.preventDefault();
  }


  toggleSticky() {
    let mobHead = this.stickyContainer
    let deskHead = this.stickyMenuContainer;

    if (window.pageYOffset > this.stickyOffset) {
      mobHead.classList.add("is-sticky");
    } else {
      mobHead.classList.remove("is-sticky");
    }

    // Only for screen sizes LG or bigger
    if( window.innerWidth >= this.lgSize ) {
      if (window.pageYOffset > this.stickyMenuOffset) {
        if( !deskHead.classList.contains('is-sticky') ) {
          this.stickySearchConsole.classList.remove('is-toggled');
        }
        deskHead.classList.add("is-sticky");
        this.stickySearchConsole.classList.add('is-sticky');
      } else {
        if( deskHead.classList.contains('is-sticky') ) {
          this.stickySearchConsole.classList.remove('is-toggled');
          this.stickySearchConsole.classList.remove('is-sticky');

          //if( window.pageYOffset < deskHead.offsetHeight ) {
            deskHead.classList.remove("is-sticky");
          //}

        }
      }
    }
  }

}


/**
 * Load the theme scripts when the DOM content is loaded
 */
document.addEventListener('DOMContentLoaded', () => {
  const theme = new Theme({
    classNames: {
      btnMobNav : 'rflex-toggle-mob-nav',
      containerMobNav : 'rflex-mob-nav-container',
      containerHeader : 'rflex-header',
      submenuTitles: 'rflex-submenu-title',
      toggleElements : 'rflex-toggle',
      disableClickClasses : [
        '.rflex-anchor-noclick > a',
        '.rflex-anchor-prevent-default'
      ],
      // Elements that are clickable and navigate to the first link found in the element
      clickableElements : [
        'rflex-clickable'
      ],

    },
    togglePairs : [
      {
        toggleTriggerClass: 'rflex-toggle-search',
        toggleTargetId: 'rflex-search-console',
        toggleEventName: 'click',
      }
    ],
    stickyId : 'rflex-header-container',
    stickyMenuId : 'rflex-nav-bar',
    imageGalleryConfigs: [
      {
        galleryClass: 'rflex-image-gallery',
        galleryItemClass: 'rflex-gallery-list-item',
        galleryModalClass: 'rflex-gallery-modal',
        galleryModalImageClass: 'rflex-gallery-modal-img',
        controlsClasses: {
          close: ['rflex-gallery-controls-close', 'rflex-gallery-modal'],
          next: ['rflex-gallery-controls-next'],
          prev: ['rflex-gallery-controls-prev']
        }
      }
    ]
  });

  theme.init();
});