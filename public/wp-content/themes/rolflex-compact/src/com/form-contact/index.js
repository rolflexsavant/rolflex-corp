const { render, useState } = wp.element;

const ContactForm = () => {

 // const [fullname, setFullname ] = useState('');

  const [state, setState ] = useState({});

  let form = {
    fullname : '',
    company : '',
    email : '',
    phone : '',
    city : '',
    country : '',
    question : '',
  }


  function validateEmail(email) {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  const validateForm = () => {
    const required = [
      'fullname', 'company', 'email', 'phone', 'country', 'question',
    ];


    // if( !validateEmail(form.email) ) {
    //   console.log("returned email false");
    //   return false;
    // }


      for( let field of required ) {
        if( form[field].length == 0 ) {
          console.log({form: form , msg: "returned tis false" });
          return false;
        }
      }
      return true;


    console.log({accept: form.accept , msg: "returned regular false"});
    return false;
  }


  const handleChange = ( e ) => {
    form[e.target.name] = e.target.value;
    console.log(forma);
    setForm(forma);
  };


  const handleForm = ( e ) => {

    console.log(e);
    e.preventDefault();

    console.log("Hi there");
  };


  const doFullname = (e) => {
    setFullname( e.target.value );
  };
    return (
      <div>
        <h1>{forma.fullname}</h1>
        <form onSubmit={handleForm}>
          <div class="form-group">
            <label>{ rflex.label.fullname }</label>
            <input type="text" name="fullname" onChange={handleChange} class="form-control"/>
          </div>

          <div class="form-group">
            <label>{ rflex.label.company }</label>
            <input type="text" name="company" class="form-control" />
          </div>

          <div class="form-group">
            <label>{ rflex.label.email }</label>
            <input type="text" name="email"  class="form-control" />
          </div>

          <div class="form-group">
            <label>{ rflex.label.phone }</label>
            <input type="text" name="phone"  class="form-control" />
          </div>

          <div class="form-group">
            <label>{ rflex.label.city }</label>
            <input type="text" name="city" class="form-control" />
          </div>

          <div class="form-group">
            <label>{ rflex.label.country }</label>
            <input type="text" name="country" class="form-control" />
          </div>

          <div class="form-group">
            <label>{ rflex.label.question }</label>
            <textarea name="question" class="form-control"></textarea>
          </div>

          <button class="form-btn form-btn-submit" type="submit" ><span class="btn-label rflex-icon-prepend rflex-icon-arrow-right">{ rflex.btn.submit }</span></button>
        </form>
      </div>
    );





};

render(<ContactForm />, document.getElementById(`react-component`));




