
<?php
/**
 * If the args are empty, this is the point of entry, otherwise the archive is
 * loaded through "home.php".
 */
if( empty($args) ) {
    $args['hero']   = get_hero_fields( get_archive_field('hero') );
    $args['title']  = get_archive_field('title');

    if( get_post_type() == 'articles' ) {
        $args['component'] = [
            'view' => 'part-search-form',
            'args' => [
                'post_type' => 'articles'
            ],
        ];
    }
}
yield_header(); ?>
<section <?php post_class('rflex-content rflex-post rflex-project'); ?>>
    <?php if(isset($args['hero']) ): ?>
        <?php yield_part('section-hero', $args ); ?>
    <?php endif; ?>
    <?php yield_archive( get_post_type() ) ?>


</section>
<?php yield_footer(); ?>