'use strict';

/***********************************************************************
 * SASS
 */
const gulp = require('gulp');
const livereload = require('gulp-livereload');


gulp.task('scss',  () => {
    const sass = require('gulp-sass');
    sass.compiler = require('node-sass');

    const cssnano = require('gulp-cssnano');
    const autoprefixer = require('autoprefixer');
    const sourcemaps = require('gulp-sourcemaps');
    const postcss = require('gulp-postcss');
    const rename = require('gulp-rename');
    const stripcsscomments = require('gulp-strip-css-comments');

    return gulp.src('./src/scss/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(sourcemaps.init())
        .pipe(postcss([ autoprefixer() ]))
        .pipe(stripcsscomments())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('./assets/css'))
        .pipe( cssnano() )
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('./assets/css'));
});


/***********************************************************************
 * JS / BABEL
 */
gulp.task('js', () => {
    const babel = require('gulp-babel');
    const terser = require('gulp-terser');
    const rename = require('gulp-rename');

    return gulp.src(['./src/js/*.js'])
        .pipe(babel({
            presets: ['@babel/env'],
            plugins: ["@babel/plugin-proposal-class-properties"]
        }))
        .pipe(gulp.dest('./assets/js'))
        .pipe(terser())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('./assets/js'))
});




/***********************************************************************
 * IMG
 */
gulp.task('img', () => {
    const copy = require('gulp-copy');
    const imagemin = require('gulp-imagemin');
    const webp = require('gulp-webp');

    return gulp.src('./src/img/*')
        //.pipe(copy('assets/img'))
        /*.pipe( imagemin({
            interlaced: true,
            progressive: true,
            optimizationLevel: 6,
        })) */
        //.pipe( gulp.dest('assets/img') )
        //.pipe( webp() )
        .pipe( gulp.dest('assets/img') );
});

gulp.task('animated', () => {
    const copy = require('gulp-copy');
    const imagemin = require('gulp-imagemin');
    const webp = require('gulp-webp');

    return gulp.src('./src/img/animation/*')
        //.pipe(copy('assets/img'))
        /*.pipe( imagemin({
            interlaced: true,
            progressive: true,
            optimizationLevel: 6,
        })) */
        //.pipe( gulp.dest('assets/img') )
        //.pipe( webp() )
        .pipe( gulp.dest('assets/img/animation') );
});


/***********************************************************************
 * FONTS & ICONS
 */
gulp.task('fonts-icons', () => {
    const rename = require('gulp-rename');

    return gulp.src([
            './src/ico/icomoon/fonts/**',
            './src/fonts/**'
        ])
        .pipe(rename({dirname: ''}))
        .pipe( gulp.dest('assets/fonts') );
});



/***********************************************************************
 * WATCH & LIVERELOAD
 * ---
 * Watches other processes and notifies live reload on any file change
 */
gulp.task('watch', () => {
    livereload.listen();

    gulp.watch('./src/img/**', gulp.series(['img']) );
    gulp.watch('./src/scss/**', gulp.series(['scss']) );
    gulp.watch('./src/js/**', gulp.series(['js']) );
    gulp.watch([
        './src/ico/icomoon/fonts/*',
        './src/fonts/*'
    ], gulp.series(['fonts-icons']) );
    gulp.watch('./**').on('change', livereload.changed);
});


gulp.task('default', gulp.series(['scss', 'js', 'img', 'fonts-icons', 'watch']) );

gulp.task('build', gulp.series(['scss', 'js', 'img', 'animated', 'fonts-icons']) );
