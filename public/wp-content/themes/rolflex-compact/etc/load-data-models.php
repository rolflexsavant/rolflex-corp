<?php
add_action( 'init', 'rflex_register_data_models' );


function rflex_register_data_models() {
    $data_models = rflex_get_data_models();

    if( isset($data_models['taxonomies']) ) {
        foreach($data_models['taxonomies'] as $model_name => $model ) {
            register_taxonomy( $model_name, $model['object_type'], $model['args'] );
        }
    }

    if( isset($data_models['post-types']) ) {
        foreach($data_models['post-types'] as $model_name => $model ) {
            register_post_type( $model_name, $model );
        }
    }
}



function rflex_get_data_models() {
    foreach(['post-types', 'taxonomies'] as $type_name) {
        $data_models_dir = rflex_DATA_MODELS_PATH.'/'.$type_name.'/';

        foreach( scandir($data_models_dir) as $data_model_file) {
            if( is_file($data_models_dir.$data_model_file) ) {
                $data_model_name = pathinfo($data_model_file, PATHINFO_FILENAME);
                $data_models[$type_name][$data_model_name] = include($data_models_dir.$data_model_file);
            }
        }
    }
    return $data_models;
}


/**
 * Easily adjust custom post type icons in one place...
 * @see: https://developer.wordpress.org/resource/dashicons/#products
 */
function rflex_get_dash_icon( $post_type ) {
    $dashicons = [
        'applications' => 'dashicons-hammer',
        'articles' => 'dashicons-welcome-learn-more',
        'organizations' => 'dashicons-bank',
        'projects' => 'dashicons-portfolio',
        'downloads' => 'dashicons-cloud-saved',
        'default' => 'dashicons-admin-appearance',
    ];

    return isset($dashicons[$post_type]) ? $dashicons[$post_type] : $dashicons['default'];
}

