<?php
/**
 * Filter the hero fields we want from all the generated fields..
 */
function get_hero_fields($haystack) {
    $fields = [
        'title' => array_get($haystack, 'image_large.title'),
        'alt'   => array_get($haystack, 'image_large.alt'),
        'text' => array_get($haystack, 'text' ), // @deprecated, description should not be used, check the hero file and the admin. It could be removed
        'style' => array_get($haystack, 'style'),
        'image_large' => array_get($haystack, 'image_large.sizes.hero-large'),
        'image_inline' => array_get($haystack, 'image_large.sizes.hero-inline'),
        'image_small' => array_get($haystack, 'image_small.sizes.hero-mobile'),
        'image_thumb' => array_get($haystack, 'image_large.sizes.hd-thumb-cropped'),
    ];

    return $fields;
}


/**
 * Builds the testimonial
 */
function get_testimonial_fields( $post = null ) {
    $fields = [];

    if( !is_null($post) ) {
        $fields = get_fields($post->ID);
    }

    $testimonial = [
        'title' => isset($post->post_title) ? $post->post_title : null ,
        'testimonial_person' => [
            'image' => [
                'alt' => array_get($fields, 'testimonial_person.image.alt', null),
                'url' => array_get($fields, 'testimonial_person.image.sizes.square'),
            ],
            'name' => array_get($fields, 'testimonial_person.name', null),
            'job_title'  => array_get($fields, 'testimonial_person.job_title', null),
            'organization' => array_get($fields, 'testimonial_person.organization.post_title', null),
        ],
        'testimonial_content' => array_get($fields, 'testimonial_content', null),
    ];

    return $testimonial;
}

/**
 * Banner
 */
function get_fields_banner() {
    $fields = [
        'title' => __('Discover your own perfect fit', 'rctd' ),
        'lead' => __('This is the lead text for the design your own door banner.'),
        'link' => get_info('special_pages.design_your_door' , '/'),
        'button' => 'Design your own door',
    ];

    return $fields;
}


// Short hand to get...
function get_info($info_field, $default_value = 'NOT_SET' ) {
    return rflex_ACF::getInstance()->get_info($info_field, $default_value);
}

// Short hand to print...
function info( $info_field, $default_value = 'NOT_SET' ) {
    print get_info($info_field, $default_value );
}

/**
* Only use on archive pages, adds the post type related prefix to the options field.
*/
function get_archive_field($field_name, $default_value = 'NOT_SET') {
    $field_name = sprintf('archive_%1$s_%2$s', get_post_type(), $field_name );
    return get_info($field_name, $default_value);
}


function get_the_term_title() {
    return get_queried_object()->name;
}


function get_terms_fields( $terms ) {

    foreach($terms as $k => $term ) {
        $fields = get_fields('term_'.$term->ID );
        $terms[$k] = array_merge( (array) $term , (array) $fields );
    }

    return $terms;
}


function get_download_fields( WP_Post $post ) {
    $open_in_window_mime_types = [
        'application/pdf',
    ];

    $fields = get_fields($post);
    $path = get_attached_file($fields['file']['ID']);
    $pathinfo = pathinfo($path);
    $title = get_the_title($post);
    $terms = get_the_terms( get_the_ID($post), 'download-types' );

    $download = [
        'title' => $title,
        'content' => wpautop(get_the_excerpt($post)),
        'link' => get_the_permalink($post),
        'file' => array_merge([
            'pathinfo' => array_merge([
                'path' => $path
            ], $pathinfo),
            'download_name' => str_kebab_case($title).'.'.$pathinfo['extension'],
            'download_size' => str_readable_bytes($fields['file']['filesize']),
        ], $fields['file']),
        'terms' => $terms,
        'open_in_window' => in_array($fields['file']['mime_type'], $open_in_window_mime_types),
        'raw' => [
            'post' => $post,
            'fields' => $fields,
        ]
    ];

    return $download;
}

function set_download_headers( $download ) {
// Header content type
    if( $download['open_in_window'] ) {
        header('Content-type: '.$download['file']['mime_type']);
        header('Content-Disposition: inline; filename="' . $download['file']['download_name'] . '"');
        header('Content-Transfer-Encoding: binary');
        header("Content-Length: " . filesize($download['file']['pathinfo']['path']) );
        header('Accept-Ranges: bytes');
        readfile($download['file']['pathinfo']['path']);
        //exit;
    } else {
        header( "refresh:3;url=".$download['file']['link'] );
    }

}
