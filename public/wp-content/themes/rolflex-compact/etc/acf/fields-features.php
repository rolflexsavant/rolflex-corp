<?php


function get_fields_features( $features_array ) {
    $features = array_map(function($feature_raw){

        if( $feature_raw['feature'] instanceof WP_Post ) {
            $feature['feature'] = get_fields_feature($feature_raw['feature']);
            $feature = array_merge( $feature_raw, $feature );

            return $feature;
        }

        return $feature_raw;

    }, $features_array );

    return $features;
}


/**
 *
 */
function get_fields_feature( WP_Post $post ) {
    $fields = get_fields($post->ID);

    $feature = [
        'title' => $post->post_title,
        'icon' => array_get($fields, 'icon'),
        'link' => get_post_permalink($post),
        'raw' => [
            'post' => $post,
            'fields' => $fields,
        ]
    ];

    return $feature;
}


