<?php
function get_all_applications() {
    $applications     = [];
    $raw_applications = get_terms('applications', ['hide_empty' => !WP_DEBUG ]);


    foreach($raw_applications as $k => $application) {
        $fields = get_fields('term_'.$application->term_id);

        // Defaults
        $application = (array) array_merge( [
            'icon' => 'application-default',
            'link' => get_term_link($application),
            'hero' => isset($fields['hero']) ? get_hero_fields($fields['hero']) : null,
        ], (array) $application);

        if( is_array($fields) ) {
            $application = (array) array_merge( (array) $fields, (array) $application );
        }

        $applications[$k] = $application;
    }

    return $applications;
}


function get_fields_projects( array $projects_array ) {
    $projects = array_map(function($project_object) {

        $project = get_fields_project($project_object);

        return $project;

    } ,$projects_array);

    return $projects;
}

/**
 * Create the project field array, filter out what we want.
 */
function get_fields_project( WP_Post $post , $include_related = false ) {


    $fields     = get_fields($post->ID);

    $fields['features']   = !empty($fields['features']) ? $fields['features'] : [];
    $fields['related_projects']   = is_array($fields['related_projects']) ? $fields['related_projects'] : [];

    if( isset($fields['meta_data']['organization']->ID) ) {
        $organization_fields = get_fields($fields['meta_data']['organization']->ID);
    } else {
        $organization_fields = [];
    }

    $project = [
        'ID' => $post->ID,
        'title' => $post->post_title,
        'image' => array_get($fields, 'hero.hero.image_large.sizes.projects-thumb'),
        'organization_name' => array_get($fields, 'meta_data.organization.post_title', null, true ),
        'description' => array_get($fields, 'description'), // @deprecated, description should not be used, check the hero file and the admin. It could be removed
        'city'  => array_get($fields, 'meta_data.city'),
        'country' => array_get($fields, 'meta_data.country'),
        'permalink' => get_post_permalink($post), // @TODO: needs to give the right permalink
        'sectors' => 'sectors',
        'hero' => get_hero_fields( array_get($fields, 'hero' ) ),
        'thumb' => array_get($fields, 'hero.image_large.sizes.projects-thumb'),
        'logo' => [
            'title' => array_get($organization_fields, 'logo.title'),
            'alt' => array_get($organization_fields, 'logo.alt'),
            'url' => array_get($organization_fields, 'logo.sizes.projects-logo'),
        ],
        'meta' => [
            [
               'label'  => __('Client', 'rctd'),
               'value'  => array_get($fields, 'meta_data.organization.post_title', null, true ),
            ],
            [
                'label' => __('Year', 'rctd'),
                'value' => array_get($fields, 'meta_data.year'),
            ],
            [
                'label' => __('City', 'rctd'),
                'value' => array_get($fields, 'meta_data.city'),
            ],
            [
                'label' => __('Country', 'rctd'),
                'value' => array_get($fields, 'meta_data.country'),
            ]
        ],
        'facts' => array_get($fields, 'facts', []),
        'features' => get_fields_features($fields['features']),
        'gallery' => array_get($fields,'gallery', []),
        'raw' => [
            'post' => $post,
            'fields' => $fields,
        ]
    ];

    if( $include_related == true ) {
        $project['related_projects'] = get_fields_projects($fields['related_projects']);
    }

    return $project;
}
