<?php

/**
 * For spee
 */
function get_articles_archive_data() {

    $topics = array_filter( get_terms('topics'), function($topic) {
        return true; // Return everything
        //return ( $topic->parent == 0 );
    });

    $topics = array_map( function($topic){
        return get_fields_topic($topic);
    }, $topics );


    $items_per_row  = 3;
    $number_of_rows = ceil( count($topics) / $items_per_row );
    $rows           = array_chunk($topics, $items_per_row, true );
    $middle_of_rows = ceil( count($rows) / 2 );


    for( $i = 0; $i < count($rows); $i++ ) {
        $section_name = ( $i < $middle_of_rows ) ? 'top' : 'bottom';

        if( !isset($section[$section_name]) ) {
            $section[$section_name] = [];
        }

        $section[$section_name] = array_merge($section[$section_name], $rows[$i] );
    }

    return [
        'top' => $section['top'],
        'bottom' => $section['bottom']
    ];
}


function get_fields_articles( array $posts, $include_related = true ) {
    $articles = [];

    foreach($posts as $post ) {
        $articles[] = get_fields_article($post, $include_related );
    }
    return $articles;
}

function get_fields_article( WP_Post $post, $include_related = true ) {
    $fields = get_fields($post);
    unset($post->post_content);

    return [
        'title' => get_the_title($post),
        'link' => get_the_permalink($post),
        'raw' => $post
    ];
}



function get_fields_topic( WP_Term $term , $number_of_articles = 10 ) {
    $fields     = get_fields('term_'.$term->term_id);
    $articles   =  get_topic_articles($term, $number_of_articles, false );

    return [
        'title' => $term->name,
        'link' => get_term_link($term),
        'icon' => array_get($fields, 'icon'),
        'articles' => $articles,
        'fields' => $fields,
        'raw' => $term,
    ];
}



function get_topic_articles( WP_Term $term , $number_of_articles = 10 , $include_children = true ) {
    $query = new WP_Query([
        'post_type' => 'articles',
        'posts_per_page' => $number_of_articles,
        'tax_query' => [
            [
                'taxonomy' => 'topics',
                'field' => 'term_id',
                'terms' => [$term->term_id],
                'include_children' => $include_children,
            ]
        ]
    ]);

    $articles = [];

    while( $query->have_posts() ) { $query->the_post();
        $article = get_post();
        $articles[] = get_fields_article($article);
    }; wp_reset_postdata();

    return $articles;
}



function get_all_topics() {
    $topics     = [];
    $raw_topics = get_terms('topics');

    foreach($raw_topics as $k => $topic) {
        $fields = get_fields('term_'.$topic->term_id);

        // Defaults
        $topic = (array) array_merge( [
            'icon' => 'topic-default',
            'link' => get_term_link($topic),
        ],(array) $topic);

        if( is_array($fields) ) {
            $topic = (array) array_merge( (array) $fields, (array) $topic );
        }

        $topics[$k] = $topic;
    }

    return $topics;
}





/**
 * Gets all the topics and related fields from ACF merges them into one nice object.
 */
// function get_topics() {
//     $topics     = [];
//     $raw_topics = get_terms('topics');

//     foreach($raw_topics as $k => $topic) {
//         $fields = get_fields('term_'.$topic->term_id);

//         if( is_array($fields) ) {
//             $topic = (object) array_merge( (array) $fields, (array) $topic );
//         }

//         $topics[$k] = $topic;
//     }

//     return $topics;
// }