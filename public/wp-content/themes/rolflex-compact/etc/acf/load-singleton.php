<?php
/**
 * This is where we find our save point.
 */
define('rflex_ACF_JSON', rflex_DATA_MODELS_PATH.'/custom-fields-json' );

class rflex_ACF {

protected static $_instance = null;

protected $_info = [];


static public function getInstance() {
    if( is_null(self::$_instance) ) {
        self::$_instance = new self();
    }
    return self::$_instance;
}

public function init() {
    add_filter('acf/settings/save_json', [$this, 'json_save_point'] );
    add_filter('acf/settings/load_json', [$this, 'json_load_point']);
    add_action('acf/init', [$this, 'op_init']);
    add_action('acf/init', [$this, 'init_block_types']);
    /**
     * Adds custom fields to the Terms
     */
    add_filter('get_terms', [$this, 'filter_add_term_fields'], 0 );
    add_filter('get_the_terms', [$this, 'filter_add_term_fields'], 0 );
}


public function json_load_point( $paths ) {
    // remove original path (optional)
    if( isset($paths[0]) ) {
        unset($paths[0]);
    }
    // append path
    $paths[] = rflex_ACF_JSON;
    // return
    return $paths;
}

public function json_save_point( $path ) {
    return rflex_ACF_JSON;
}


public function op_init() {
    // Check function exists.
    if( function_exists('acf_add_options_sub_page') ) {
        // Add parent.
        $general = acf_add_options_page(array(
            'page_title'  => __('Rolflex settings'),
            'menu_title'  => __('Rolflex'),
            'redirect'    => false,
        ));

        // Add sub page.
        $child_contact = acf_add_options_sub_page(array(
            'page_title'  => __('Site settings'),
            'menu_title'  => __('Site settings'),
            'parent_slug' => $general['menu_slug'],
        ));


        acf_add_options_page(array(
            'page_title' 	=> __('Projects archive', 'rctd'),
            'menu_title' 	=> __('Archive page', 'rctd'),
            'menu_slug' 	=> 'options_projects',
            'capability' 	=> 'edit_posts',
            'parent_slug'	=> 'edit.php?post_type=projects',
            'position'	=> false,
            'icon_url' 	=> 'dashicons-images-alt2',
            'redirect'	=> false,
        ));

        acf_add_options_page(array(
            'page_title' 	=> __('Knowledge center archive', 'rctd'),
            'menu_title' 	=> __('Archive page', 'rctd'),
            'menu_slug' 	=> 'options_articles',
            'capability' 	=> 'edit_posts',
            'parent_slug'	=> 'edit.php?post_type=articles',
            'position'	=> false,
            'icon_url' 	=> 'dashicons-images-alt2',
            'redirect'	=> false,
        ));

        acf_add_options_page(array(
            'page_title' 	=> __('Blog archive', 'rctd'),
            'menu_title' 	=> __('Archive page', 'rctd'),
            'menu_slug' 	=> 'options_blog',
            'capability' 	=> 'edit_posts',
            'parent_slug'	=> 'edit.php?post_type=post',
            'position'	=> false,
            'icon_url' 	=> 'dashicons-images-alt2',
            'redirect'	=> false,
        ));

        acf_add_options_page(array(
            'page_title' 	=> __('Downloads archive', 'rctd'),
            'menu_title' 	=> __('Archive page', 'rctd'),
            'menu_slug' 	=> 'options_downloads',
            'capability' 	=> 'edit_posts',
            'parent_slug'	=> 'edit.php?post_type=downloads',
            'position'	=> false,
            'icon_url' 	=> 'dashicons-images-alt2',
            'redirect'	=> false,
        ));

    }
}



function filter_add_term_fields($terms) {
    $terms = array_map(function($term){
        if( !$term instanceof WP_Term ) {
            return $term; // Not a WP_Term? Skip this stuff...
        }

        $fields = get_fields('term_'.$term->term_id);

        if( is_array($fields) ) {
            foreach($fields as $field_name => $field_value) {
                $term->{$field_name} = $field_value;
            }
        }

        return $term;

    }, $terms );

    return $terms;
}


/**
 * Load custom blocks
 */
public function init_block_types() {
    // // register a testimonial block.
    // acf_register_block_type([
    //     'name'              => 'applications',
    //     'title'             => __('Applications'),
    //     'description'       => __('Applications block.'),
    //     'render_template'   => 'blocks/applications/applications.php',
    //     'category'          => 'formatting',
    //     'icon'              => 'admin-comments',
    //     'keywords'          => array( 'application', 'applications' ),
    // ]);

    // // register a testimonial block.
    acf_register_block_type([
        'name'              => 'image-gallery',
        'title'             => __('Image Gallery', 'rctd'),
        'description'       => __('Image gallery block.', 'rctd'),
        'render_template'   => 'blocks/image-gallery/image-gallery.php',
        'category'          => 'content',
        'icon'              => 'admin-comments',
        'keywords'          => array( 'image', 'gallery', 'image gallery' ),
    ]);

    // // register a testimonial block.
    acf_register_block_type([
        'name'              => 'buttons',
        'title'             => __('Button container'),
        'description'       => __('Block containing buttons.'),
        'render_template'   => 'blocks/buttons/buttons.php',
        'category'          => 'content',
        'icon'              => 'admin-comments',
        'keywords'          => array( 'button', 'button' ),
    ]);

    acf_register_block_type([
        'name'              => 'image-grid',
        'title'             => __('Image grid'),
        'description'       => __('Shows a collection of image nicely in a grid.'),
        'render_template'   => 'blocks/image-grid/image-grid.php',
        'category'          => 'content',
        'icon'              => 'admin-comments',
        'keywords'          => array( 'image', 'grid', 'image-grid' ),
    ]);

    acf_register_block_type([
        'name'              => 'video',
        'title'             => __('Embed video'),
        'description'       => __('Allows you to embed a video'),
        'render_template'   => 'blocks/video/video.php',
        'category'          => 'content',
        'icon'              => 'admin-comments',
        'keywords'          => array( 'video', 'embed', 'youtube', 'vimeo', 'player' ),
    ]);

}


/*
 * Not used yet, just there because it might come in handy
 */
public function post_id() {
    if ( is_admin() && function_exists( 'acf_maybe_get_POST' ) ) :
        return intval( acf_maybe_get_POST( 'post_id' ) );
    else :
        global $post;
        return $post->ID;
    endif;
}


/**
 * Simple API function to handle get_field( $fieldname, 'option') calls easier
 * and more cleanly. Allows to get deeper values in an array with the . notation.
 * Inspired by Laravels array helper
 */
public function get_info($info_field, $default_value = 'NOT_SET' ) {
    $original_key   = $info_field;
    $fields         = [];

    // Return it if we find it right away.
    if( isset($this->_info[$original_key]) ) {
        return $this->_info[$original_key];
    }

    if( strpos($info_field, '.') ) {
        $fields     = explode('.', $info_field);
        $info_field = array_shift($fields);
    }

    $info = get_field($info_field, 'option');

    foreach($fields as $field ) {
        if( is_array($info) && isset($info[$field]) ) {
            $info = $info[$field];
        } else {
            $info = null;
            break;
        }
    }

    if( (is_null($info) || $info == '') && $default_value !== 'NOT_SET' ) {
        $info = $default_value;
    }

    // Store it and prevent us from doing this again.
    $this->_info[$original_key] = $info;

    return $info;
}

}

// Load the instance and initialize it
rflex_ACF::getInstance()->init();


function filter_add_term_fields(array $terms) {
    return rflex_ACF::getInstance()->filter_add_term_fields($terms);
}