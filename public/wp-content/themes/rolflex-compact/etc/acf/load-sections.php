<?php
/**
 * This file contains all the Rolflex section related logic. It's pretty straight forward. There are parts of the
 * project where (almost) the same things are happening as you'll find here. Mostly in top-level template files and
 * functions called from there. The plan is to move everything here. With some minor tweaks this should easily be
 * possible and make things much more simple. We're almost there already, but dealines deadlines deadlines... :-)
 */
define('rflex_ACF_section_CB_PREFIX', 'section_load_' );

define('rflex_ACF_section_TPL_PREFIX' , 'section-');


/******************************************************************************************************
 * section/ section helpers
 ******************************************************************************************************
 */
function load_sections( $sections = [] ) {
    if( !is_array($sections) ) {
        return;
    }

    foreach($sections as $section_fields ) {
        $section_name = $section_fields['acf_fc_layout'];
        unset($section_fields['acf_fc_layout']);

        section_load($section_name, $section_fields);
    }
}


function section_load( $section_name , $fields ) {
    $section_callback = rflex_ACF_section_CB_PREFIX.$section_name;

    // Do we have a section callback override? We can weak this more so we can run with less code....
    if( function_exists( $section_callback ) ) {
        call_user_func_array($section_callback, [$fields]);
    } else {
        $section_name    = section_get_name($section_callback);
        $template       = section_get_template( $section_name );

        if( $template ) {
            yield_part($template, $fields);
        } else {
            trigger_error("Could not find a template for section named <code>$section_name</code>" , E_USER_NOTICE );
        }
    }
}

function section_get_name( $__FUNCTION__ ) {
    return str_replace(rflex_ACF_section_CB_PREFIX,  '', $__FUNCTION__ );
}


function section_get_template( $section_name ) {
    $template = rflex_ACF_section_TPL_PREFIX.$section_name;
    $template = str_kebab_case($template);
    return $template;
}



/******************************************************************************************************
 * Functions that actually load the sections/sections
 ******************************************************************************************************
 */


function section_load_features_overview( $fields ) {
    $section_name    = section_get_name(__FUNCTION__);
    $template       = section_get_template( $section_name );

    $args['features'] = get_fields_features($fields['features']);

    yield_part($template, $args );
}


function section_load_knowledge_center_articles($fields) {
    $section_name   = section_get_name(__FUNCTION__);
    $template       = section_get_template( $section_name );

    $title    = !empty($fields['articles_title']) ? $fields['articles_title'] : __('Populair in our knowledge center');
    $articles = get_fields_articles($fields['knowledge_center_articles']);

    yield_part($template, [
        'title' => $title,
        'articles' => $articles,
    ]);
}


function section_load_applications_overview($fields = []) {
    $section_name   = section_get_name(__FUNCTION__);
    $template       = section_get_template( $section_name );

    $args = array_merge($fields, [
        'applications' =>  get_all_applications()
    ]);

    yield_part($template, $args);
}


function section_load_content_overview( $fields  ) {
    $section_name    = section_get_name(__FUNCTION__);
    $template        = section_get_template( $section_name );
    $content         = [];

    foreach( $fields['content_overview'] as $post ) {
        $post_fields = get_fields($post);
        $hero_fields = isset($post_fields['hero']) ? get_hero_fields($post_fields['hero']) : null;

        $tile_content = isset($hero_fields['text']) ? $hero_fields['text'] : get_the_excerpt($post);

        $content[] = [
            'title' => $post->post_title,
            'link'  => get_the_permalink($post),
            'hero'  => $hero_fields,
            'content'   => $tile_content,
        ];
    }

    $args = [
        'content' => $content,
        'shade' => $fields['shade'],
    ];

    if( !empty($fields['title']) ) {
        $args['title'] = $fields['title'];
    }

    if( !empty($fields['text']) ) {
        $args['text'] = $fields['text'];
    }

    yield_part($template, $args);
}


function section_load_projects_overview( $fields ) {
    $section_name    = section_get_name(__FUNCTION__);
    $template       = section_get_template( $section_name );

    $args['projects'] = get_fields_projects($fields['related_projects']);

    yield_part($template, $args );
}



// function section_load_gallery( $fields ) {
//     $section_name    = section_get_name(__FUNCTION__);
//     $template       = section_get_template( $section_name );

//     yield_part($template, $fields );
// }

// function section_load_banner( $fields ) {
//     $section_name    = section_get_name(__FUNCTION__);
//     $template       = section_get_template( $section_name );

//     yield_part($template, $fields);
// }


// function section_load_facts_figures( $fields  ) {
//     $section_name    = section_get_name(__FUNCTION__);
//     $template       = section_get_template( $section_name );

//     //d($fields);
//     yield_part($template, $fields);
// }