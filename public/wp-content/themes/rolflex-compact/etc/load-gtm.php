<?php

add_action('init', 'rflex_gtm_init');
function rflex_gtm_init() {

    if( !is_admin() && rflex_gtm_get_id() !== false ) {
        ds_datalayer(); // Make sure it is initialized in time, before the wp-hook
        add_action('wp', 'rflex_gtm_init_datalayer');
        add_action('wp_head','rflex_gtm_head', -999999);
        // Yes, the noscript is loaded in the bottom instead of right after the <body>
        // but let's be honest. When is this needed? Worth creating a new hook for?
        add_action('wp_footer', 'rflex_gtm_body', -999999);
    }
}



/**
 * If a constant is defined it will return the value of the constant. If no constant is defined
 * it tries to get it from a ACF field.
 */
function rflex_gtm_get_id() {
    if( defined('GOOGLE_TAG_MANAGER_ID') ) {
        return GOOGLE_TAG_MANAGER_ID;
    }
    return get_option('rflex_gtm_id', false);
}


/**
 * Initialize the datalayer for Google Tag Manager
 */
function rflex_gtm_init_datalayer() {
    $dl         = ds_datalayer();
    $language   = rflex_current_site_language();

    $dl->push('language', $language['code'] );
}


/**
 * Simple function that prints the <head> part of the google tag manager script.
 *
 * @param void
 */
function rflex_gtm_head() { ?>
<script>
var dataLayer = [<?= ds_datalayer()->toJson() ?>];

(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','<?= rflex_gtm_get_id() ?>');</script>
<?php }


/**
 * Simple function that prints the <body> part of the google tag manager script
 */
function rflex_gtm_body() { ?>
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=<?= rflex_gtm_get_id() ?>"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<?php }
