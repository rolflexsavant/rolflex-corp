<?php
/**
 * Main init.
 */

add_action('init', function() {
    /*
    * Let WordPress manage the document title.
    * By adding theme support, we declare that this theme does not use a
    * hard-coded <title> tag in the document head, and expect WordPress to
    * provide it for us.
    */
    add_theme_support( 'title-tag' );

    add_theme_support( 'editor-styles' ); // if you don't add this line, your stylesheet won't be added
    add_editor_style( 'assets/css/'. rflex_asset_maybe_add_suffix( rflex_CSS_EDITOR ) );

    // Show mini versions of images in the admin...
    add_image_size( 'admin-mini', '512', '288' , true );

    add_image_size( 'projects-logo', '400', '100', false );
    add_image_size( 'projects-thumb', '1024', '768' , false );

    // add_image_size( 'projects-large', '1920', '1080' , false );
    // add_image_size( 'projects-small', '1280', '720' , false );
    // add_image_size( 'projects-mobile', '1024', '768' , false );
    add_image_size( 'hd-large', '1920', '1080' , false );
    add_image_size( 'hd-small', '1280', '720' , false );
    add_image_size( 'hd-thumb', '960', '540' , false );
    add_image_size( 'hd-large-cropped', '1920', '1080' , true );
    add_image_size( 'hd-small-cropped', '1280', '720' , true );
    add_image_size( 'hd-thumb-cropped', '960', '540' , true );
    add_image_size( 'hd-mini-cropped', '480', '270' , true );
    add_image_size( 'square', '400', '400' , true );


    add_image_size( 'grid-thumb', '960', '540' , false );

    add_image_size( 'hero-large', '1920', '1080' , false );
    add_image_size( 'hero-small', '1280', '720' , false );
    add_image_size( 'hero-inline', '960', '540' , false );
    add_image_size( 'hero-thumb', '960', '540' , false );
    add_image_size( 'hero-mobile', '1024', '768' , false );

    add_image_size( 'gallery-full', '1920', '1080' , false );
    add_image_size( 'gallery-highlight', '1280', '720' , true );
    add_image_size( 'gallery-highlight-mobile', '960', '540' , true );
    add_image_size( 'gallery-thumb', '512', '288' , true );


    add_filter( 'allowed_block_types', 'rflex_allowed_block_types', 10, 2 );
    add_filter( 'gutenberg_can_edit_post_type', 'rflex_disable_gutenberg', 10, 2 );
    add_filter( 'use_block_editor_for_post_type', 'rflex_disable_gutenberg', 10, 2 );

    add_action( 'wp_enqueue_scripts', 'rflex_register_styles' );
    add_action( 'wp_enqueue_scripts', 'rflex_register_scripts' );
});






function rflex_allowed_block_types( $allowed_blocks, $post ) {
    $allowed_blocks = array_keys(WP_Block_Type_Registry::get_instance()->get_all_registered());

	$disable_blocks = [
        'core/archives',
        'core/audio',
        'core/button',
        'core/buttons',
        'core/block',
        'core/calendar',
        'core/categories',
        'core/code',
        // 'core/column',
        // 'core/columns',
        'core/cover-image',
        'core/embed',
        'core/file',
        'core/freeform',
        'core/gallery',
        'core/group',
        // 'core/heading',
        // 'core/html',
        // 'core/image',
        'core/latest-comments',
        'core/latest-posts',
        // 'core/list',
        'core/media-text',
        'core/missing',
        'core/more',
        'core/nextpage',
        // 'core/paragraph',
        'core/preformatted',
        'core/pullquote',
        'core/quote',
        'core/reusable-block',
        'core/rss',
        'core/search',
        'core/separator',
        'core/shortcode',
        'core/social-link',
        'core/social-links',
        'core/spacer',
        'core/subhead',
        // 'core/table',
        'core/tag-cloud',
        // 'core/text-columns',
        'core/verse',
        'core/video',
    ];

    // Needs array_values --> For some reason it doesn't work if the array keys arent reset
    $allowed_blocks = array_values(array_diff($allowed_blocks, $disable_blocks));

    //xdb($allowed_blocks);
    return $allowed_blocks;
}

/**
 * Templates and Page IDs without editor
 *
 */
function rflex_disable_editor( $id = false ) {

	$excluded_templates = array(
		//'modular.php',
		'contact.php'
	);

	$excluded_ids = array(
		// get_option( 'page_on_front' )
	);

	if( empty( $id ) )
		return false;

	$id = intval( $id );
	$template = get_page_template_slug( $id );

	return in_array( $id, $excluded_ids ) || in_array( $template, $excluded_templates );
}

/**
 * Disable Gutenberg by template
 *
 */
function rflex_disable_gutenberg( $can_edit, $post_type ) {

    if( in_array($post_type, ['downloads']) )
        return false;

	if( ! ( is_admin() && !empty( $_GET['post'] ) ) )
		return $can_edit;

	if( rflex_disable_editor( $_GET['post'] ) )
        $can_edit = false;


	return $can_edit;

}






/***************************************************************************************************
 * ASSETS
 */


function rflex_assets_version( $asset_type = null ) {
    static $build_version = null;

    if( is_null($build_version) && function_exists('ds_version') ) {
        $version = ds_version();
        $build_version = isset($version->build) ? $version->build : null;
    }
    return $build_version;
}


/**
 * Maybe add a suffix to the file extension
 *
 * @param string The path to the asset
 */
function rflex_asset_maybe_add_suffix( $path ) {
    if( !rflex_MINIFY_ASSETS ) {
        return $path;
    }
    $extension = '.'.pathinfo($path, PATHINFO_EXTENSION );

    return preg_replace('/'.$extension.'$/', '.min'.$extension, $path );
}




function rflex_register_styles() {
    wp_enqueue_style( 'rolflex-compact-style', get_template_directory_uri().rflex_asset_maybe_add_suffix(rflex_DIR_CSS.'/'.rflex_CSS_THEME ), array(), rflex_assets_version() );
}

// Enqueue Theme JS with React Dependency
function rflex_register_scripts() {
    wp_dequeue_style( 'wp-block-library' );
    wp_dequeue_style( 'wp-block-library-theme' );

    $dependencies = [
        //'wp-element'
    ];


    wp_enqueue_script(
        'rolflex-compact-script',
        get_stylesheet_directory_uri() . rflex_DIR_JS . rflex_asset_maybe_add_suffix('/theme.js'),
        $dependencies,
        rflex_assets_version(), // Change this to null for production
        true
    );


    if(  is_front_page() ) {
        //) {
        wp_enqueue_script(
            'rolflex-compact-hero',
            get_stylesheet_directory_uri() . rflex_DIR_JS . rflex_asset_maybe_add_suffix('/hero.js'),
            $dependencies,
            rflex_assets_version(), // Change this to null for production
            true
        );

        $inline_js = "(function(){
            document.addEventListener('DOMContentLoaded', () => {
                const hero = new HeroAnimation({
                    canvasId : 'hero-animation',
                    mainContainerId : 'rflex-main',
                    headerContainerId : 'rflex-header-container',
                    heroContainerId : 'rflex-hero-container',
                    framesUrl : '". get_stylesheet_directory_uri().'/assets/img/animation/'."',
                    height : 864,
                    width : 800,
                    framesCount : 84,
                    framesFileExtension : '.png'
                });
                hero.init();
            });
        })();";

        wp_add_inline_script('rolflex-compact-hero', $inline_js );
    }


    wp_localize_script('rolflex-component-form-contact', 'rflex', [
        'label' => [
            'fullname'      => __('Full name', 'rctd'),
            'company'       => __('Company name', 'rctd'),
            'email'         => __('E-mail address', 'rctd'),
            'phone'         => __('Telephone number', 'rctd'),
            'streetline1'   => __('Streetline 1', 'rctd'),
            'streetline2'   => __('Streetline 2', 'rctd'),
            'city'          => __('City', 'rctd'),
            'country'       => __('Country', 'rctd'),
            'question'      => __('Question / message', 'rctd'),
            'accept'        => sprintf( __('I accept the <a href="%1s">%2s</a>'), 'http://www.google.com' , 'Terms & Conditions' )
        ],
        'btn' => [
           'submit' => __('Submit form', 'rctd')
        ],
        'alert' => [
            'success'   => __('Your form has been sent submitted successfully.', 'rctd'),
            'email'     => __('The specified e-mail address is incorrect.', 'rctd'),
            'error'     => __('The following field contains errors: %1s', 'rctd'),
            'fatal_error' => __('We could not process the form. Something went wrong.', 'rctd')
        ]
    ]);
}

/**
 * Sofia Pro doesn't support a ringel-s so for the german website we have a little javascript fix
 */
//add_action('wp_footer', 'rflex_ringel_s_fix');
function rflex_ringel_s_fix() {
    $language = rflex_current_site_language();

    if( $language['code'] == 'de' ) {
    ?><script>
        (function(){
            document.addEventListener('DOMContentLoaded', function(){
                var els = document.querySelectorAll('h1,h2,h3,h4,h5,h6,p,a');
                for(var i = 0; i < els.length; i++ ) {
                var replaceHTML = els[i].innerHTML.replace(/ß/g, '<span class="rflex-ringel-s">ß</span>');
                els[i].innerHTML = replaceHTML;
                }
            });
        })();
    </script><?php
    }
}


// // Enqueue Theme JS w React Dependency
// add_action( 'wp_enqueue_scripts', 'my_enqueue_theme_js' );
// function my_enqueue_theme_js() {
//   wp_enqueue_script(
//     'my-theme-frontend',
//     get_stylesheet_directory_uri() . '/build/index.js',
//     ['wp-element'],
//     time(), // Change this to null for production
//     true
//   );
// }



/**
 * REMOVE COMMENTS
 */

// Removes from admin menu
add_action( 'admin_menu', 'rflex_remove_admin_menus' );
function rflex_remove_admin_menus() {
    remove_menu_page( 'edit-comments.php' );
}

// Removes from post and pages
add_action('init', 'rflex_remove_comment_support', 100);
function rflex_remove_comment_support() {
    remove_post_type_support( 'post', 'comments' );
    remove_post_type_support( 'page', 'comments' );
}
// Removes from admin bar
function rflex_admin_bar_render() {
    global $wp_admin_bar;
    $wp_admin_bar->remove_menu('comments');
}
add_action( 'wp_before_admin_bar_render', 'rflex_admin_bar_render' );
