<?php
/****************************************************************************************
 * Completely Remove All Parts - CRAP
 * -
 * Wordpress has a lot of annoying things that are loaded into a webpage. These things
 * are disabled in this page.
 */


add_action( 'customize_register', 'crap_customize_register' );
function crap_customize_register( $wp_customize ) {
	$wp_customize->remove_control( 'custom_css' );
}


add_filter( 'post_thumbnail_html', 'remove_wps_width_attribute', 10 );
add_filter( 'image_send_to_editor', 'remove_wps_width_attribute', 10 );


function remove_wps_width_attribute( $html ) {
	$html = preg_replace( '/(width|height)=\"\d*\"\s/', "", $html );
    return $html;
}


function crap_is_wplogin(){
	if( function_exists('helper_is_login') ) {
		return helper_is_login();
	}
    $ABSPATH_MY = str_replace(array('\\','/'), DIRECTORY_SEPARATOR, ABSPATH);
    return ((in_array($ABSPATH_MY.'wp-login.php', get_included_files()) || in_array($ABSPATH_MY.'wp-register.php', get_included_files()) ) || (isset($_GLOBALS['pagenow']) && $GLOBALS['pagenow'] === 'wp-login.php') || $_SERVER['PHP_SELF']== '/wp-login.php');
}

/**
 * Completely Remove jQuery From WordPress
 */
add_action('init', 'crap_init');
function crap_init() {
    crap_disable_emojis();

    // Drop some customizer actions
    remove_action( 'plugins_loaded', '_wp_customize_include', 10);
    remove_action( 'admin_enqueue_scripts', '_wp_customize_loader_settings', 11);
	remove_action('wp_head', 'wp_generator');
	remove_action('wp_head', 'wp_resource_hints', 2);
	remove_action('wp_head', 'rest_output_link_wp_head', 10);

    if (!is_admin() && !crap_is_wplogin() ) {
        wp_deregister_script('jquery');
        wp_register_script('jquery', false);
    }
}


add_action( 'init', 'crap_oembed', PHP_INT_MAX - 1 );
function crap_oembed() {
	// Remove the REST API endpoint.
	remove_action('rest_api_init', 'wp_oembed_register_route');

	// Turn off oEmbed auto discovery.
	// Don't filter oEmbed results.
	remove_filter('oembed_dataparse', 'wp_filter_oembed_result', 10);

	// Remove oEmbed discovery links.
	remove_action('wp_head', 'wp_oembed_add_discovery_links');

	// Remove oEmbed-specific JavaScript from the front-end and back-end.
	remove_action('wp_head', 'wp_oembed_add_host_js');
}


/**
 * Disable the emoji's
 */
function crap_disable_emojis() {
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_action( 'admin_print_styles', 'print_emoji_styles' );
	remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
	remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
	remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );

	// Remove from TinyMCE
	add_filter( 'tiny_mce_plugins', 'crap_disable_emojis_tinymce' );
}


/**
 * Filter out the tinymce emoji plugin.
 */
function crap_disable_emojis_tinymce( $plugins ) {
	if ( is_array( $plugins ) ) {
		return array_diff( $plugins, array( 'wpemoji' ) );
	} else {
		return array();
	}
}

/**
 * This is a modification of image_downsize() function in wp-includes/media.php
 * we will remove all the width and height references, therefore the img tag
 * will not add width and height attributes to the image sent to the editor.
 *
 * @param bool false No height and width references.
 * @param int $id Attachment ID for image.
 * @param array|string $size Optional, default is 'medium'. Size of image, either array or string.
 * @return bool|array False on failure, array on success.
 */
function crap_image_downsize( $value = false, $id, $size ) {
	if ( !wp_attachment_is_image($id) )
		return false;

	$img_url = wp_get_attachment_url($id);
	$is_intermediate = false;
	$img_url_basename = wp_basename($img_url);

	// try for a new style intermediate size
	if ( $intermediate = image_get_intermediate_size($id, $size) ) {
		$img_url = str_replace($img_url_basename, $intermediate['file'], $img_url);
		$is_intermediate = true;
	}
	elseif ( $size == 'thumbnail' ) {
		// Fall back to the old thumbnail
		if ( ($thumb_file = wp_get_attachment_thumb_file($id)) && $info = getimagesize($thumb_file) ) {
			$img_url = str_replace($img_url_basename, wp_basename($thumb_file), $img_url);
			$is_intermediate = true;
		}
	}

	// We have the actual image size, but might need to further constrain it if content_width is narrower
	if ( $img_url) {
		return array( $img_url, 0, 0, $is_intermediate );
	}
	return false;
 }
 /* Remove the height and width refernces from the image_downsize function.
 * We have added a new param, so the priority is 1, as always, and the new
 * params are 3.
 */
//add_filter( 'image_downsize', 'crap_image_downsize', 1, 3 );


//add_filter( 'post_thumbnail_html', 'remove_width_attribute', 10 );
//add_filter( 'image_send_to_editor', 'remove_width_attribute', 10 );

function remove_width_attribute( $html ) {
   $html = preg_replace( '/(width|height)="\d*"\s/', "", $html );
   return $html;
}