<?php
// Always sets the language to the current language the user visits...
// Function is found in the mu-plugin: rflex-languages
add_action('init', 'rflex_set_current_site_language' );

/**
 * Remove the YOAST WPSEO og:locale tag. It only allows language_COUNTRY values
 * and this theme is only multilingual and not multiregional.
 */
add_action( 'wpseo_frontend_presenters', 'rflex_remove_wpseo_locale_presenter' );
function rflex_remove_wpseo_locale_presenter( $presenters ) {
    return array_map( function( $presenter ) {
        if ( ! $presenter instanceof Yoast\WP\SEO\Presenters\Open_Graph\Locale_Presenter ) {
            return $presenter;
        }
    }, $presenters );
}