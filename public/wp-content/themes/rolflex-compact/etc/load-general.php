<?php
/**
 * Some things we don't want to hook up right away. These hooks can be added here.
 */
add_action('init', function(){
    add_filter( 'wp_nav_menu_'.rflex_menu('slug', 'main-desktop').'_items', 'rflex_menu_add_search' );
});


add_filter('pre_get_posts', 'number_of_posts_per_content_type');
function number_of_posts_per_content_type($query) {
    if (!is_admin() ) {
        if( $query->is_search() && $query->is_main_query() ) {
            $query->set('posts_per_page', get_option('searchresults_per_page', 20) );
        }

        if( $query->is_post_type_archive('projects') && $query->is_main_query() ) {
            $query->set('posts_per_page', get_option('projects_per_page', 12) );
        }

        if( $query->is_post_type_archive('downloads') && $query->is_main_query()) {
            $query->set('posts_per_page', get_option('downloads_per_page', 12) );
        }

    }
    return $query;
}


// Filter wp_nav_menu() to add additional links and other output
function rflex_menu_add_search($items) {
    $searchItem= '<li class="rflex-toggle-search"><a href="#" class="rflex-icon-prepend rflex-icon-search">' . __('Search', 'rctd') . '</a></li>';
    $doorItem = '<li class="rflex-door-cta"><a href="'.get_info('special_pages.design_your_door', '').'"><i class="rflex-icon rflex-icon-design-door"></i>&nbsp;'.__('Design your own door', 'rctd').'</a></li>';
    // add the home link to the end of the menu
    $items = $items .  $searchItem . $doorItem;
    return $items;
}


/**
 * Change the page parameter in URL to a language agnostic one
 */
//add_action( 'init', 'rflex_set_pagination_base' );
function rflex_set_pagination_base () {
    global $wp_rewrite;
    $wp_rewrite->pagination_base = 'platina';
}


/**
 * Returns a nicely formatted terms string
 */
function rflex_terms_list( $terms, $separator = ', ') {
    return implode($separator, array_map( function($category){
        return $category->name;
    }, $terms ) );
}


/**
 * Get latest posts
 */
function rflex_get_latest_posts($args = [] ) {
    $args = array_merge([
        'posts_per_page' => 5,
    ], $args );

    $query = new WP_Query($args);
    $posts = [];

    if( $query->have_posts() ) {
        while($query->have_posts() ) {
            $query->the_post();

            $post   = get_post();
            $fields = get_fields($post);
            $hero   = empty($fields['hero']['image_large']) ? false : get_hero_fields($fields['hero']);

            $posts[] = [
                'ID' => get_the_ID(),
                'title' => get_the_title(),
                'permalink' => get_the_permalink(),
                'hero' => $hero,
            ];
        }

        wp_reset_postdata();
    }

    return $posts;
}


function rflex_do_404() {
    status_header( 404 );
    nocache_headers();
    require_once get_query_template( '404' );
}

/**
 * Redirect wrapper
 */
function rflex_redirect($url, $code = 302, $x_redirect_by = "Rolflex") {
    wp_redirect($url, $code, $x_redirect_by );
    exit;
}

/**
 *
 */
add_action('init', 'rflex_flush_rewrite');
function rflex_flush_rewrite() {
    flush_rewrite_rules( true );
}

/**
 * Register navigation menus uses wp_nav_menu in five places.
 */
add_action( 'after_setup_theme', 'rflex_register_menus' );
function rflex_register_menus() {
    $locations = [
        'main-desktop' => __( 'Desktop Main Navigation' , 'rctd'),
        'top-desktop' => __( 'Desktop Top Navigation' , 'rctd'),
        'main-mobile' => __( 'Mobile Main Navigation' , 'rctd'),
        'footer'      => __('Footer Menu', 'rctd'),
    ];

    /**
     * Footer menu locations are dynamic
     */
  //  $locations = array_merge($locations, rflex_get_footer_menus(true) );

	register_nav_menus($locations);
}


// /**
//  * Returns the footer menu locations
//  *
//  * @param boolean If set to true it will return location => menu name value pairs
//  * @return array Returns an array with footer menu locations (and optionally their names)
//  */
// function rflex_get_footer_menus( $include_names = false ) {
//     $footer_menu_count = get_field('footer_menu_count', 'option');
//     $footer_menu_count =  $footer_menu_count > 0 ?  $footer_menu_count : 4;

//     $footer_menus = [];

//     for( $i = 1; $i <= $footer_menu_count; $i++ ) {
//         $footer_menus[rflex_FOOTER_MENU_PREFIX.$i] = "Footer Menu $i";
//     }

//     if( $include_names ) {
//         return $footer_menus;
//     }

//     return array_keys($footer_menus);
// }


function rflex_menu($property, $location ) {
    return get_term(get_nav_menu_locations()[$location], 'nav_menu')->{$property};
}

function rflex_footer_menu_options() {
    $menu           = get_term(get_nav_menu_locations()['footer'], 'nav_menu');
    $layout_options = get_field('menu_layout', $menu);

    if( is_null($layout_options) ) {
        $layout_options = [
            'cols' => 'auto', // String, because this is what ACF returns too..
            'align' => 'center',
        ];
    }

    $layout_options['col_class'] = 'col-lg';

    if( $layout_options['cols'] !== 'auto' ) {
        $layout_options['col_class'] = $layout_options['col_class']. '-'. floor((12 / (int) $layout_options['cols'] ));
    }


    return $layout_options;
}


/**
 * Allow to add classes to all li elements in the navigation
 */
function rflex_register_menu_li_classes($classes, $item, $args) {
    if(isset($args->add_li_classes)) {
        $classes[] = $args->add_li_classes;
    }
    return $classes;
}
add_filter('nav_menu_css_class', 'rflex_register_menu_li_classes', 1, 3);


/**
 * Allow to add classes to all li elements with a depth of 1
 */
function rflex_register_menu_top_li_classes($classes, $item, $args) {
    if( $item->menu_item_parent == 0 && isset($args->add_top_li_classes) ) {
        $classes[] = $args->add_top_li_classes;
    }
    return $classes;
}
add_filter('nav_menu_css_class', 'rflex_register_menu_top_li_classes', 1, 3);


/**
 * Allow to add classes to all li elements with a depth of 1
 */
function rflex_register_menu_href_hashtag_add_li_classes($classes, $item, $args) {
    if( $item->url == '#' && isset($args->href_hashtag_add_li_classes) ) {
        $classes[] = $args->href_hashtag_add_li_classes;
    }
    return $classes;
}
add_filter('nav_menu_css_class', 'rflex_register_menu_href_hashtag_add_li_classes', 1, 3);


/**
 * Manipulate the HTML output of wp_nav_menu
 */
add_filter( 'wp_nav_menu_items', 'rflex_process_hashtag_items', 10, 2 );
function rflex_process_hashtag_items( $items, $args ) {
    $hashtag_subject = isset($args->href_hashtag_classes) ? ' class="'.$args->href_hashtag_classes.'"' : '';
    // If we need to remove the href hashtag, we will replace it with the classes
    if( !isset($args->href_hashtag_remove) || $args->href_hashtag_remove == false ) {
        $hashtag_subject .= ' href="#"';
    }

    $items = str_replace(' href="#"', $hashtag_subject, $items );


    return $items;
}


/**
 * Let's add the language menu to the mobile menu
 */
add_filter( 'wp_nav_menu_items', 'rflex_menu_language_html', 2, 2 );
function rflex_menu_language_html( $original_items, $arguments ) {
    if( isset($arguments->include_language) && $arguments->include_language == true ) {
        $language_selected = rflex_current_site_language();
        $language_options = rflex_get_language_options();
        $items = [];

        foreach( $language_options as $option ) {
            if( $option['code'] !== $language_selected['code'] ) {
                $items[] = sprintf('<li><a href="%1s">%2s</a></li>', $option['url'] , $option['label'] );
            }
        }

        $lang_menu_items = sprintf(
            '<li class="rflex-menu-language-container"><a class="rflex-submenu-title rflex-icon-prepend rflex-icon-globe">%1s</a><ul class="sub-menu">%2s</ul></li>',
            $language_selected['label'],
            implode("\n", $items )
        );

        $original_items = implode("\n", [$original_items, $lang_menu_items ]);
    }

    return $original_items;
}



/**
 * Little helper HTML to show in which breakpoint we are
 * Only displays when rflex_SHOW_BREAKPOINTS is defined true
 */
function rflex_show_breakpoints() {
    echo '<div style="position: fixed; bottom: 0px; left: 0; width:100%; background: #000; color: #fff">
        <div class="d-block d-sm-none">XS</div>
        <div class="d-none d-sm-block d-md-none">SM</div>
        <div class="d-none d-md-block d-lg-none">MD</div>
        <div class="d-none d-lg-block d-xl-none">LG</div>
        <div class="d-none d-xl-block">XL</div>
    </div>';
}
