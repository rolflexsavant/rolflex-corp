<?php
/**
 * Wordpress has added an arguments parameter since WP 5.5.0 to the get_sidebar, get_footer,
 * get_header and get_template_part functions. It is annoying that it is the last
 * parameter that gets the arguments while the second argument is rarely used.
 * These functions wrap around them with a nicer order of arguments.
 */

/* Shorthand for get_sidebar(); */
function yield_sidebar( array $args = [], $name = null ) {
    return get_sidebar($name, $args );
}

/* Shorthand for get_footer(); */
function yield_footer( array $args = [], $name = null ) {
    return get_footer($name, $args );
}

/* Shorthand for get_header() */
function yield_header( array $args = [], $name = null ) {
    return get_header($name, $args );
}

/* Shorthand for get_template_part with different parameter order. */
function yield_template( $slug, array $args = [], $name = null ) {
    return get_template_part($slug, $name, $args );
}

function yield_part($slug, array $args = [], $name = null ) {
    return yield_template('views/'.$slug, $args, $name  );
}

// function yield_aside( $slug , array $args = [], $name = null )  {
//     return yield_part('aside-'.$slug, $args, $name );
// }

function yield_content_footer( $slug, array $args = [], $name = null ) {
    return yield_part('footer-'.$slug, $args, $name );
}

function yield_archive( $post_type , $args = [] ) {
    return yield_part('archive-'.$post_type , $args );
}


function yield_breadcrumbs( $args = [] ) {
    global $did_breadcrumbs;

    if( $did_breadcrumbs ) {
        return;
    }

    yield_part('part-breadcrumbs', $args );
    $did_breadcrumbs = true;
}


function did_yield_breadcrumbs( $set_value = null ) {
    global $did_breadcrumbs;

    if( !is_null($set_value) ) {
        $did_breadcrumbs = $set_value;
    }

    if( !isset($did_breadcrumbs) ) {
        return false;
    }
    return $did_breadcrumbs;
}


function classes_col_limit($limit) {
    $col_width = ceil(12 / $limit);
    return 'col-12 col-md-6 col-lg-'.$col_width;
}

function get_meta_description( $post = null ) {
    if( is_null($post) ) {
        $post = get_post();
        $ID = get_the_ID($post);
    } else {
        $ID = get_the_ID();
    }
    return get_post_meta( $ID, '_yoast_wpseo_metadesc', true );
}

function get_the_result_description( $post = null ) {
    $description = get_meta_description($post);

    if( empty($description) ) {
        $description = get_the_excerpt($post);
    }

    $description = str_wordcut_char($description, 165); // 165 is the meta description character length

    return $description;
}

function the_result_description( $post = null ) {
    print get_the_result_description($post);
}



function rflex_is_paged() {
    global $wp_query;

    if( $wp_query->max_num_pages <= 1 )
        return false;

    return true;
}


function rflex_paged_text( $paged_string = null ) {
    if( is_null($paged_string) ) {
        $paged_string = __('Page %1s of %2s');
    }

    global $wp_query;
    $total_pages = (string) absint($wp_query->max_num_pages);
    $current_page = (string) get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;

    return sprintf( $paged_string , $current_page, $total_pages );
}


function rflex_numeric_posts_nav($before = '<ul class="rflex-nav-numeric-posts">', $after = '</ul>', $label_prev = null, $label_next = null) {

    if( is_singular() )
        return;

    global $wp_query;

    /** Stop execution if there's only 1 page */
    if( $wp_query->max_num_pages <= 1 )
        return;

    $paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
    $max   = intval( $wp_query->max_num_pages );

    /** Add current page to the array */
    if ( $paged >= 1 )
        $links[] = $paged;

    /** Add the pages around the current page to the array */
    if ( $paged >= 3 ) {
        $links[] = $paged - 1;
      //  $links[] = $paged - 2;
    }

    if ( ( $paged + 2 ) <= $max ) {
        $links[] = $paged + 2;
        $links[] = $paged + 1;
    }

    echo $before . "\n";

    /** Previous Post Link */
    if ( get_previous_posts_link() )
        printf( '<li class="numeric-nav-item numeric-nav-item-prev">%s</li>' . "\n", get_previous_posts_link($label_prev) );

    /** Link to first page, plus ellipses if necessary */
    if ( ! in_array( 1, $links ) ) {
        $class = 1 == $paged ? ' numeric-nav-item-active' : '';

        printf( '<li class="numeric-nav-item numeric-nav-item-number%s"><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );

        if ( ! in_array( 2, $links ) )
            echo '<li class="numeric-nav-item numeric-nav-item-empty">…</li>';
    }

    /** Link to current page, plus 2 pages in either direction if necessary */
    sort( $links );
    foreach ( (array) $links as $link ) {
        $class = $paged == $link ? ' numeric-nav-item-active' : '';
        printf( '<li class="numeric-nav-item numeric-nav-item-number%s"><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
    }

    /** Link to last page, plus ellipses if necessary */
    if ( ! in_array( $max, $links ) ) {
        if ( ! in_array( $max - 1, $links ) )
            echo '<li class="numeric-nav-item numeric-nav-item-empty">…</li>' . "\n";

        $class = $paged == $max ? ' numeric-nav-item-active' : '';
        printf( '<li class="numeric-nav-item numeric-nav-item-number%s"><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
    }

    /** Next Post Link */
    if ( get_next_posts_link() )
        printf( '<li class="numeric-nav-item numeric-nav-item-next">%s</li>' . "\n", get_next_posts_link($label_next) );

    echo $after . "\n";

}