<?php
/**
 * Init the admin
 */
add_action('init', function() {
    if( is_user_logged_in() ) {
        helper_admin_color_schema_force();
        admin_color_scheme_init();
    }
});


/**
 * Init actions that adjust the front-end admin bar color to the selected color
 * in the admin.
 */
function admin_color_scheme_init() {
    add_action( 'wp_before_admin_bar_render', 'admin_bar_save_wp_admin_color_schemes_list' );
    add_action( 'wp_enqueue_scripts', 'admin_bar_enqueue_admin_bar_color' );
}

/**
 * Save the color schemes list into wp_options table
 */
function admin_bar_save_wp_admin_color_schemes_list() {
    global $_wp_admin_css_colors;

    if ( @count( $_wp_admin_css_colors ) > 1 && has_action( 'admin_color_scheme_picker' ) ) {
        update_option( 'wp_admin_color_schemes', $_wp_admin_css_colors );
    }
}

/**
 * Enqueue the registered color schemes on the front end
 */
function admin_bar_enqueue_admin_bar_color() {
    if ( ! is_admin_bar_showing() ) {
        return;
    }

    $user_color = get_user_option( 'admin_color' );

    if ( isset( $user_color ) && isset($wp_admin_color_schemes[$user_color]->url) ) {
        $wp_admin_color_schemes = get_option( 'wp_admin_color_schemes' );
        wp_enqueue_style( $user_color, $wp_admin_color_schemes[$user_color]->url );
    }
}