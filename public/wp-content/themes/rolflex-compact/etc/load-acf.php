<?php
/**
 * Tools to help deal with ACF en its options
 */
require_once dirname(__FILE__).'/acf/fields-common.php'; // We need some of the stuff here right away
require_once dirname(__FILE__).'/acf/load-singleton.php';

/**
 * Functions to handle fields...
 */
require_once dirname(__FILE__).'/acf/fields-features.php';
require_once dirname(__FILE__).'/acf/fields-faq.php';
require_once dirname(__FILE__).'/acf/fields-projects.php';

/**
 * Sections
 */
require_once  dirname(__FILE__).'/acf/load-sections.php';