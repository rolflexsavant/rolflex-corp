<?php
/**
 * The text domain of the theme for translations.
 */
load_theme_textdomain( 'rctd', dirname(__FILE__).'/lang' );
/**
 * Hard coded contsants
 */
// Stylesheet directory
define('rflex_DIR_CSS', '/assets/css');
// Javascript directory
define('rflex_DIR_JS', '/assets/js');
// Fonts directory
define('rflex_DIR_FONTS', '/assets/fonts');
// Component directory
define('RFLEX_DIR_COM', '/assets/com');


// Stylesheet names
define('rflex_CSS_THEME', 'theme-bundle.css');
define('rflex_CSS_EDITOR', 'editor-bundle.css');

/* Now some constants that we define conditionally */
define('rflex_THEME_VERSION' , '1.0'); // Will be conditionally later

// Don't minify assets when WP debug is on.
define('rflex_MINIFY_ASSETS', defined('WP_DEBUG') ? !WP_DEBUG : true );

define('rflex_SHOW_BREAKPOINTS', defined('WP_DEBUG') ? WP_DEBUG : false );

if( !defined('rflex_SHOW_BREAKPOINTS') ) {
    define('rflex_SHOW_BREAKPOINTS', false );
}

define('rflex_DATA_MODELS_PATH' , dirname(__FILE__).'/data-models');

// The directory in we put our "stuff"
define('rflex_ETC_PATH', dirname(__FILE__).'/etc' );


// Alphabetical
require_once rflex_ETC_PATH.'/load-acf.php';
require_once rflex_ETC_PATH.'/load-admin.php';
require_once rflex_ETC_PATH.'/load-assets-support.php';
require_once rflex_ETC_PATH.'/load-data-models.php';
require_once rflex_ETC_PATH.'/load-disable-crap.php';
require_once rflex_ETC_PATH.'/load-general.php';
require_once rflex_ETC_PATH.'/load-gtm.php';
require_once rflex_ETC_PATH.'/load-language.php';
require_once rflex_ETC_PATH.'/load-template.php';