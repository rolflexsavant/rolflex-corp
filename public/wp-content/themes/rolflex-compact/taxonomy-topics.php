<?php
/**
 * The topics taxonomy page is something we will redirect. We will show the first
 * article in the topic. On the right the article will display other articles
 * in the same topic. This is a short cut to go live because this page
 * wasn't designed. We place the redirect logic here we have no other logical place
 * for it yet.
 */

$term = get_queried_object();

/**
 * If we have a term , lets try to get the first article and redirect
 * to this first article.
 */
if( $term instanceof WP_Term ) {
    $loop = new WP_Query([
        'post_type' => 'articles',
        'tax_query' => [
            [
                'taxonomy' => 'topics',
                'field' => 'slug',
                'terms' => $term->slug,
            ],
        ]
    ]);

    // Loop the loop :-)
    while($loop->have_posts()) { $loop->the_post();
        $article = get_post();
        $permalink = get_the_permalink($article);

        rflex_redirect($permalink);
    }
}


/**
 * If there hasn't been a loop, we handle it as a 404
 */
rflex_do_404();

