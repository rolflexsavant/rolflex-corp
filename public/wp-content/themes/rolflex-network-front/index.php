<?php

$langauge = null;
$user_lang = rflex_current_user_language();

if( !is_null($user_lang) ) {
    $language = rflex_get_available_language($user_lang);
}

if( is_null($language) ) {
    $language = rflex_get_available_language('en');
}

wp_redirect($language['url']);
exit;
