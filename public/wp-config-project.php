<?php
/************************************************************************************************************
 * There is quite some configuration (for wordpress) that is specific to this
 * project and doesn't contain sensitive information. That's configuration
 * we'll in this file, it's not environment specific or sensitive enough
 * to not put it under version control.
 */

 /*****************************************************
 * Environment specific configuration
 *
 * There are different configurations under version contol
 * for the different environments.
 */

// Different configurations that we have under version control
$configs = [
    'development' => 'wp-config-dev.php',
    'production' => 'wp-config-prod.php',
];

if( !defined('RFLEX_ENV') ) {
    define('RFLEX_ENV' , 'production');
}


/**
 * Load environment specific configuration
 */
if( isset($configs[RFLEX_ENV]) ) {
    require_once $configs[RFLEX_ENV];
}


/**
 * The following values are set depending on which and if certain
 * values have been set.
 */
if( !defined('WP_DEBUG') ) {
    define('WP_DEBUG' , false );
}

if( defined('RFLEX_SSL') && constant('RFLEX_SSL')) {
    if( !defined('FORCE_SSL_ADMIN') ) {
        define( 'FORCE_SSL_ADMIN', true );
    }
    $_SERVER['HTTPS'] = 'on';
}

/*****************************************************
 * Database
 */

 /** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/*****************************************************
 * Multisite
 */
define('WP_ALLOW_MULTISITE', true);
define('MULTISITE', true);
define('SUBDOMAIN_INSTALL', false);
define('PATH_CURRENT_SITE', '/');
define('SITE_ID_CURRENT_SITE', 1);
define('BLOG_ID_CURRENT_SITE', 1);


/*****************************************************
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'rolflex-e93667c1a2e9edc17f24b03301ca67a1' );
define( 'SECURE_AUTH_KEY',  'rolflex-e93667c1a2e9edc17f24b03301ca67a2' );
define( 'LOGGED_IN_KEY',    'rolflex-e93667c1a2e9edc17f24b03301ca67a3' );
define( 'NONCE_KEY',        'rolflex-e93667c1a2e9edc17f24b03301ca67a4' );
define( 'AUTH_SALT',        'rolflex-e93667c1a2e9edc17f24b03301ca67a5' );
define( 'SECURE_AUTH_SALT', 'rolflex-e93667c1a2e9edc17f24b03301ca67a6' );
define( 'LOGGED_IN_SALT',   'rolflex-e93667c1a2e9edc17f24b03301ca67a7' );
define( 'NONCE_SALT',       'rolflex-e93667c1a2e9edc17f24b03301ca67a8' );


/*****************************************************
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'rflex_wp_';



