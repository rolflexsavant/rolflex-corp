<?php
/** Multisite */
if( !defined('DOMAIN_CURRENT_SITE') ) {
    define('DOMAIN_CURRENT_SITE', 'rflex-corp.test' );
}


define( 'WP_DEBUG', true ); // Debugging mode...
define('WP_DEBUG_LOG', true);
define('WP_DEBUG_DISPLAY', false);


// Force to load debug functions
define( 'DS_LOAD_DEBUG' , true );
define('DS_FORCE_OPTIONS_ALWAYS', true );
define('GOOGLE_TAG_MANAGER_ID', 'GTM-525CLTP');