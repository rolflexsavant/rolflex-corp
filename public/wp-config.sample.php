<?php
define('WP_CACHE', true); // WP-Optimize Cache

/** The name of the database for WordPress */
define( 'DB_NAME', '' );
/** MySQL database username */
define( 'DB_USER', '' );
/** MySQL database password */
define( 'DB_PASSWORD', '' );
/** MySQL hostname */
define( 'DB_HOST', 'localhost' );


/**
 * Makes sure which environment configuration will be loaded later on.
 * NEVER use the development configuration in production. It is not
 * secure and can use sensitive information on screen.
 */
define('RFLEX_ENV' , 'development' ); // development | production.

/**
 * SSL true or false? Defaults to false. Is set to true it will load
 * additional settings for SSL
 */
define('RFLEX_SSL', false );


/**
 * Optional for your development machine, if you don't set it, it will
 * follow the environment configuration in wp-config-dev or
 * wp-config-prod. Is commented out by default.
 */
//define('DOMAIN_CURRENT_SITE', 'rflex-corp.test' );


/**
 * Project configuration. Loads configuration specifically for
 * this project and environments.
 */
require_once dirname(__FILE__).'/wp-config-project.php';


/**
 * That's all, stop editing! Happy publishing.
 */
/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}
/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';