<?php
/** Multisite */
if( !defined('DOMAIN_CURRENT_SITE') ) {
    define('DOMAIN_CURRENT_SITE', 'rolflex-web-01-prod.rolflex.com' );
}

define( 'WP_DEBUG', false ); // Debugging mode...
define( 'WP_DEBUG_LOG', false );
define( 'WP_DEBUG_DISPLAY', false);

// Force to load debug functions
define( 'DS_LOAD_DEBUG' , false );
define( 'DS_FORCE_OPTIONS_ALWAYS', false );
define( 'GOOGLE_TAG_MANAGER_ID', 'GTM-525CLTP');