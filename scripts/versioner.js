const fs = require('fs');
const projectPath = fs.realpathSync(`${__dirname}/..`);
const versionFile = `${projectPath}/version.json`;

//console.log(process.argv);

function readVersionFile() {
    if( !fs.existsSync(versionFile) ) {
        writeVersionFile({});
    }
    return JSON.parse(fs.readFileSync(versionFile));
}

function writeVersionFile( version ) {
    fs.writeFileSync(versionFile, JSON.stringify(version));
}


function version() {
    let version     = readVersionFile();

    if( typeof version.build === 'undefined' ) {
        version.build = 0;
    }

    version.build = version.build + 1;

    writeVersionFile(version);

    console.log("\n\nVersion:");
    console.table(version);
    console.log("\n\n");
}

version();