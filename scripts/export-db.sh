#!/bin/bash
# Bash script: Search/replace production to development url (multisite compatible)

if $(wp --url=http://compact-door.test core is-installed --network --path=../public); then
    wp search-replace --path=../public --url=http://compact-door.test 'http://compact-door.test' 'http://compact-door.emiel.cc' --recurse-objects --network --skip-columns=guid --skip-tables=wp_users
else
    wp search-replace --path=../public 'http://compact-door.test' 'http://compact-door.emiel.cc' --recurse-objects --skip-columns=guid --skip-tables=wp_users
fi

wp search-replace --path=../public --url=http://compact-door.emiel.cc 'http://compact-door.emiel.cc' 'http://compact-door.test' --recurse-objects --network --skip-columns=guid --skip-tables=wp_users