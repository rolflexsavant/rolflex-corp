const { spawn } = require('child_process');

/**
 * De build script to run all build scripts. For now it allows us to run just the gulp file from the specified
 * wordpress theme. But it can easily be extended to run gulp processes in multiple directories.
 */
async function build() {
    let arguments = process.argv.slice(2);

    if( arguments.length > 0 ) {

        const command = arguments.shift();

        if( command == 'theme' ) {
            const currentTheme = './public/wp-content/themes/rolflex-compact';
            arguments.unshift(`--gulpfile`, `${currentTheme}/gulpfile.js`);
            spawn(`gulp`, arguments, {stdio:'inherit'} ); // stdio:'inherit' pipes the output directly to the console including the code colors etc.
        }

    } else {
        console.error("No command specified.");
    }
}

build();