// Modules
const fs                = require('fs');
const fsExtra           = require('fs-extra')
const util              = require("util");
const path              = require('path');
const copydir           = require('copy-dir');
const makedir           = require('make-dir');
const backupId          = new Date().getTime();
const { spawn }         = require("child_process");

// Paths
const projectPath = fs.realpathSync(`${__dirname}/..`);
const wpPath = `${projectPath}/public`;
const wpContentPath = `${wpPath}/wp-content/uploads`;
const backupPath = `${projectPath}/tmp/backups`;



async function main() {
    const currentBackupPath = `${backupPath}/archive/backup-${backupId}`;
    const currentBackupContentPath = `${currentBackupPath}/wp-content/uploads`;
    const currentBackupDbFile  = `${currentBackupPath}/database.sql`;
    const latestBackupPath = `${backupPath}/latest`;

    for(let path of [ backupPath, currentBackupPath, currentBackupContentPath, latestBackupPath ] ) {
        if( !fs.existsSync(path) ) {
            console.log(`Created directory ${path}`);
            makedir.sync(path);
        }
    }

    console.log(`Copying files from ${wpContentPath} to ${currentBackupContentPath}`);
    copydir.sync( wpContentPath, currentBackupContentPath );

    const wpcli  = spawn("wp", [`db`, `export`, currentBackupDbFile] , {
        cwd: wpPath
    });

    wpcli.stdout.on("data", data => {
        console.log(`stdout: ${data}`);
    });

    wpcli.stderr.on("data", data => {
        console.log(`stderr: ${data}`);
    });


    wpcli.on("close", code => {
        console.log("Empty latest backup directory..")
        fsExtra.emptyDirSync(latestBackupPath);

        console.log("Copy this backup to the latest backup path...");
        copydir.sync( currentBackupPath, latestBackupPath );
    });


    // wpcli.on('error', (error) => {
    //     reject(error.message);
    // });

    // wpcli.on("close", code => {
    //     resolve(code);
    // });


}


main();